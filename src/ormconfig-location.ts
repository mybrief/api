import {ConnectionOptions} from 'typeorm';

const ormConfigLocation: ConnectionOptions = {
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: '',
  database: 'mybrief_location',
  entities: [__dirname + '/entities/**/*{.ts,.js}'],

  synchronize: false,

  migrationsRun: false,
  logging: true,
  logger: 'file',

  migrationsTableName: "migrations",

  migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
  cli: {
    migrationsDir: 'src/migrations',
    entitiesDir: "src/entities"
  },
};

export = ormConfigLocation;