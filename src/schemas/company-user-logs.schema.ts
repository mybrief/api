
import * as mongoose from 'mongoose';

export const CompanyUserLogSchema = new mongoose.Schema({
    companyUserId: Number,
    createdAt: { type: Date, default: Date.now },
    actionType: { type: String, default: null },
    title: String,
    description: {type: String, default: null },
    userAgent: {type: String, default: null },
    ip: { type: String, default: null }
});