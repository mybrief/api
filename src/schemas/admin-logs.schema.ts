
import * as mongoose from 'mongoose';

export const AdminLogSchema = new mongoose.Schema({
    adminId: Number,
    createdAt: { type: Date, default: Date.now },
    actionType: { type: String, default: null },
    title: String,
    description: {type: String, default: null },
    userAgent: {type: String, default: null },
    ip: { type: String, default: null }
});