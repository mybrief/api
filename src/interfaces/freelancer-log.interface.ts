
import { Document } from 'mongoose';

export interface FreelancerLog extends Document {
  readonly createdAt: Date;
  readonly freelancerId: number;
  readonly actionType: string;
  readonly description: string;
  readonly ip: string;
}