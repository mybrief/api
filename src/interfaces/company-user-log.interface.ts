
import { Document } from 'mongoose';

export interface CompanyUserLog extends Document {
  readonly createdAt: Date;
  readonly freelancerId: number;
  readonly actionType: string;
  readonly description: string;
  readonly ip: string;
}