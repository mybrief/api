
import { Document } from 'mongoose';

export interface AdminLog extends Document {
  readonly createdAt: Date;
  readonly adminId: number;
  readonly actionType: string;
  readonly description: string;
  readonly ip: string;
}