export interface GetCompanyRequestsPercentage {
    doing: number,
    done: number,
    to_approval: number,
    to_change: number,
    to_do: number,
    done_percentage: number,
    doing_percentage: number,
    to_approval_percentage: number,
    to_change_percentage: number,
    to_do_percentage: number,
    total: number
}