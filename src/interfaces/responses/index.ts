export { CreateCompanyResponse } from './create-company.response';

export { BaseResponse } from './base-controller.response';
export { BaseControllerResponse } from './base-controller.response';

export { GetCompanyRequestsPercentage } from './get-company-requests-percentage.response';
export { GetCompanyAvailableCoinsAndConsumptionData } from './get-company-available-coin-and-consumption-data.response';