export interface GetCompanyAvailableCoinsAndConsumptionData {
    development: number
    development_percentage: number
    package: number
    package_percentage: number
    social_media: number
    social_media_percentage: number
    strategy: number
    strategy_percentage: number
    available_coins: number
}