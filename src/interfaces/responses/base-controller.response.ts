export interface BaseControllerResponse {
    error: boolean;
    message?: string;
}

export interface BaseResponse extends BaseControllerResponse {
    data?: any;
}