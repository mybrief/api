export interface CreateCompanyResponse {
    company_id: number;
    company_user_id: number;
}