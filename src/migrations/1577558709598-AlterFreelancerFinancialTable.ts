import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterFreelancerFinancialTable1577558709598 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('freelancer_financial', new TableColumn({
            type: 'double',
            name: 'current_hourly_value',
            precision: 10,
            scale: 2,
            isNullable: false,
            default: 0
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('freelancer_financial', 'current_hourly_value');
    }

}
