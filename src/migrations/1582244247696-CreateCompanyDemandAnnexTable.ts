import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateCompanyDemandAnnexTable1582244247696 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_demand_annex',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_demand_id',
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'annex_type',
                    enum: ['file', 'link'],
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'path',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }), true);

        await queryRunner.createForeignKey("company_demand_annex", new TableForeignKey({
            columnNames: ['company_demand_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company_demand',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_demand_annex");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_demand_id") !== -1);
        await queryRunner.dropForeignKey("company_demand_annex", foreignKey);
        await queryRunner.dropTable('company_demand_annex');
    }

}
