import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class AlterPackageProductTable1574098351817 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('package_product', new TableColumn({
            name: 'package_id',
            type: 'int',
            isNullable: false
        }));

        await queryRunner.createForeignKey("package_product", new TableForeignKey({
            columnNames: ['package_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'package',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("package_product");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("package_id") !== -1);
        await queryRunner.dropForeignKey("package_product", foreignKey);
        await queryRunner.dropColumn('package_product', 'package_id');
    }

}
