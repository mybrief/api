import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterAdminTable1573149700480 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('admin', new TableColumn({
            type: 'varchar',
            length: '255',
            name: 'password',
            isNullable: false
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('admin', 'password');
    }

}
