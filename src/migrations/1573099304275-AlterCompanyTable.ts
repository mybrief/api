import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterCompanyTable1573099304275 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('company', 'slug', new TableColumn({
            type: 'varchar',
            name: 'slug',
            length: '32',
            isUnique: true,
            isNullable: false
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('company', 'slug', new TableColumn({
            type: 'varchar',
            name: 'slug',
            length: '16',
            isUnique: true,
            isNullable: false
        }))
    }

}
