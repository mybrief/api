import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class AlterJobTable1576432557396 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("product_id") !== -1);

        await queryRunner.dropForeignKey("job", foreignKey);

        await queryRunner.dropColumn('job', 'product_id');

        await queryRunner.addColumn('job', new TableColumn({
            name: 'company_product_id',
            type: 'int',
            isNullable: true,
            default: null
        }));

        await queryRunner.createForeignKey("job", new TableForeignKey({
            columnNames: ['company_product_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company_product',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_product_id") !== -1);
        await queryRunner.dropForeignKey("job", foreignKey);

        await queryRunner.changeColumn('job', 'company_product_id', new TableColumn({
            name: 'product_id',
            type: 'int',
            isNullable: false
        }));

        await queryRunner.createForeignKey("job", new TableForeignKey({
            columnNames: ['product_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'product',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

}
