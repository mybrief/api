import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterFreelancerAvailableTimeTable1576174461807 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('freelancer_time_available', new TableColumn({
            type: 'tinyint',
            name: 'can_update_time_available',
            default: 0
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('freelancer_time_available', 'can_update_time_available');
    }

}
