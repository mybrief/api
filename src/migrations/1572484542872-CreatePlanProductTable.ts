import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreatePlanProductTable1572484542872 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'plan_product',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'item_id',
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'item_type',
                    enum: ['product', 'package'],
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'quantity',
                    isNullable: false,
                    default: 1
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('plan_product');
    }

}
