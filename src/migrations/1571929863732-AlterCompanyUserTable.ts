import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterCompanyUserTable1571929863732 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('company_user', new TableColumn({
            type: 'varchar',
            name: 'email',
            isNullable: false
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('company_user', 'email');
    }

}
