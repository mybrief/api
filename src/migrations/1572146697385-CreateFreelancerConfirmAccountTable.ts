import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateFreelancerConfirmAccountTable1572146697385 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'freelancer_confirm_account',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'freelancer_id'
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'token',
                    isNullable: false
                },
                {
                    type: 'tinyint',
                    name: 'consumed',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'timestamp',
                    name: 'expires_at',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }), true);

        await queryRunner.createForeignKey("freelancer_confirm_account", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("freelancer_confirm_account");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("freelancer_confirm_account", foreignKey);
        await queryRunner.dropTable('freelancer_confirm_account');
    }

}
