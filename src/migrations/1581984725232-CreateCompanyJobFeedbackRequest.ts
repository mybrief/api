import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class CreateCompanyJobFeedbackRequest1581984725232 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_job_feedback_request',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'job_id',
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'notes',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }), true);

        await queryRunner.createForeignKey("company_job_feedback_request", new TableForeignKey({
            columnNames: ['job_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'job',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_job_feedback_request");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("job_id") !== -1);
        await queryRunner.dropForeignKey("company_job_feedback_request", foreignKey);
        await queryRunner.dropTable('company_job_feedback_request');
    }

}
