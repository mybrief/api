import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterCompanyInvoiceTable1582385234279 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('company_invoice', 'payment_type');
        await queryRunner.dropColumn('company_invoice', 'item_type');
        await queryRunner.dropColumn('company_invoice', 'item_id');
        await queryRunner.dropColumn('company_invoice', 'quantity');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('company_invoice', new TableColumn({
            type: 'enum',
            name: 'payment_type',
            enum: ['bank_slip','credit_card','coin'],
            isNullable: false
        }));

        await queryRunner.addColumn('company_invoice', new TableColumn({
            type: 'enum',
            name: 'item_type',
            enum: ['product','package','plan','coin'],
            isNullable: false
        }));

        await queryRunner.addColumn('company_invoice', new TableColumn({
            type: 'int',
            name: 'item_id',
            isNullable: false
        }));

        await queryRunner.addColumn('company_invoice', new TableColumn({
            type: 'int',
            name: 'quantity',
            isNullable: false,
            default: 1
        }));
    }

}
