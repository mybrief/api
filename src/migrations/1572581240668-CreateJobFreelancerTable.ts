import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateJobFreelancerTable1572581240668 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'job_freelancer',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'job_id'
                },
                {
                    type: 'int',
                    name: 'freelancer_id'
                },
                {
                    type: 'text',
                    name: 'notes',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'tinyint',
                    name: 'paused',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'timestamp',
                    name: 'start_date',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'finish_date',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'started_at',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'finished_at',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("job_freelancer", new TableForeignKey({
            columnNames: ['job_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'job',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("job_freelancer", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job_freelancer");
        const foreignKeyJob = table.foreignKeys.find(fk => fk.columnNames.indexOf("job_id") !== -1);
        const foreignKeyFreelancer = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("job_freelancer", foreignKeyJob);
        await queryRunner.dropForeignKey("job_freelancer", foreignKeyFreelancer);
        await queryRunner.dropTable('job_freelancer');
    }

}
