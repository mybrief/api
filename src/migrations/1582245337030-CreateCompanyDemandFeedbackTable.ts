import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateCompanyDemandFeedbackTable1582245337030 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_demand_feedback',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_demand_id',
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'status',
                    enum: ['approval', 'disapproval'],
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'feedback',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }), true);

        await queryRunner.createForeignKey("company_demand_feedback", new TableForeignKey({
            columnNames: ['company_demand_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company_demand',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_demand_feedback");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_demand_id") !== -1);
        await queryRunner.dropForeignKey("company_demand_feedback", foreignKey);
        await queryRunner.dropTable('company_demand_feedback');
    }

}
