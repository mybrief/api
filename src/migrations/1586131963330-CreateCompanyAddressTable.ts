import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class CreateCompanyAddressTable1586131963330 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_billing_address',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_id',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'full_name',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'cellphone',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'zip_code',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'street',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'number',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'complement',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'neighborhood',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'city',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'state',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }));
        
        await queryRunner.createForeignKey("company_billing_address", new TableForeignKey({
            columnNames: ['company_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_billing_address");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_id") !== -1);
        await queryRunner.dropForeignKey("company_billing_address", foreignKey);
        await queryRunner.dropTable('company_billing_address');
    }

}
