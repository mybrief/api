import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateCompanyDemandTable1582243131299 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_demand',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_id',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_product_id',
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'notes',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("company_demand", new TableForeignKey({
            columnNames: ['company_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("company_demand", new TableForeignKey({
            columnNames: ['company_product_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company_product',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_demand");
        const companyForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_id") !== -1);
        const companyProductForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_product_id") !== -1);
        await queryRunner.dropForeignKey("company_demand", companyForeignKey);
        await queryRunner.dropForeignKey("company_demand", companyProductForeignKey);
        await queryRunner.dropTable('company_demand');
    }

}
