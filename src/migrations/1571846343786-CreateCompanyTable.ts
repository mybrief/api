import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateCompanyTable1571846343786 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'name',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'cellphone',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    name: 'telephone',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    name: 'segment',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'site',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    name: 'logo',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    name: 'localization',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    name: 'cnpj',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'tinyint',
                    name: 'valid',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('company', true);
    }

}
