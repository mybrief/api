import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterProductAndPackageTable1580871965009 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('product', new TableColumn({
            type: 'enum',
            name: 'allowed_payment_type',
            enum: ['bank_slip', 'credit_card', 'all']
        }));

        await queryRunner.addColumn('package', new TableColumn({
            type: 'enum',
            name: 'allowed_payment_type',
            enum: ['bank_slip', 'credit_card', 'all']
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('product', 'allowed_payment_type');
        await queryRunner.dropColumn('package', 'allowed_payment_type');
    }

}
