import {MigrationInterface, QueryRunner} from "typeorm";

export class DropTableJobFreelancer1574359450131 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job_freelancer");
        const foreignKeyFreelancer = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        const foreignKeyJob = table.foreignKeys.find(fk => fk.columnNames.indexOf("job_id") !== -1);
        await queryRunner.dropForeignKey("job_freelancer", foreignKeyFreelancer);
        await queryRunner.dropForeignKey("job_freelancer", foreignKeyJob);
        await queryRunner.dropTable('job_freelancer', true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
