import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterJobFreelancerTable1574356991712 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('job_freelancer', new TableColumn({
            type: 'int',
            name: 'reproved_number',
            isNullable: false,
            default: 0
        }));

        await queryRunner.addColumn('job_freelancer', new TableColumn({
            type: 'timestamp',
            name: 'disapproved_at',
            default: null,
            isNullable: true
        }));

        await queryRunner.addColumn('job_freelancer', new TableColumn({
            type: 'timestamp',
            name: 'approved_at',
            default: null,
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job_freelancer', 'reproved_number');
        await queryRunner.dropColumn('job_freelancer', 'disapproved_at');
        await queryRunner.dropColumn('job_freelancer', 'approved_at');
    }

}
