import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateSystemRequestsTable1576261317242 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'system_request',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'request_type_id',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'freelancer_id',
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'description',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'notified_at',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'resolved_at',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }));

        await queryRunner.createForeignKey("system_request", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("system_request", new TableForeignKey({
            columnNames: ['request_type_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'request_type',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("system_request");
        const foreignKeyFreelancer = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        const foreignKeySystemRequest = table.foreignKeys.find(fk => fk.columnNames.indexOf("request_type_id") !== -1);
        await queryRunner.dropForeignKey("system_request", foreignKeyFreelancer);
        await queryRunner.dropForeignKey("system_request", foreignKeySystemRequest);
        await queryRunner.dropTable('system_request');
    }

}
