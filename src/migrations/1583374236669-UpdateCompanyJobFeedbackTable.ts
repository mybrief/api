import {MigrationInterface, QueryRunner,TableColumn, TableForeignKey} from "typeorm";

export class UpdateCompanyJobFeedbackTable1583374236669 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('company_job_feedback', new TableColumn({
            type: 'int',
            name: 'company_job_feedback_request_id'
        }));

        await queryRunner.createForeignKey("company_job_feedback", new TableForeignKey({
            columnNames: ['company_job_feedback_request_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company_job_feedback_request',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_job_feedback");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_job_feedback_request_id") !== -1);
        await queryRunner.dropForeignKey("company_job_feedback", foreignKey);
        await queryRunner.dropColumn('company_job_feedback', 'company_job_feedback_request_id');
    }

}
