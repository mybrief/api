import {MigrationInterface, QueryRunner, Table, TableIndex, TableForeignKey} from "typeorm";

export class companyInvoice1572545469784 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_invoice',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_id'
                },
                {
                    type: 'enum',
                    name: 'payment_type',
                    enum: ['bank_slip','credit_card','coin'],
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'payment_status',
                    enum: ['processing', 'authorized', 'paid', 'refunded', 'waiting_payment', 'pending_refund', 'refused'],
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'item_type',
                    enum: ['product', 'package', 'plan'],
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'item_id',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'quantity',
                    isNullable: false,
                    default: 1
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("company_invoice", new TableForeignKey({
            columnNames: ['company_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_invoice");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_id") !== -1);
        await queryRunner.dropForeignKey("company_invoice", foreignKey);
        await queryRunner.dropTable('company_invoice');
    }

}
