import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterUserTable1571979686520 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('freelancer', 'slug', new TableColumn({
            type: 'varchar',
            name: 'slug',
            length: '32',
            isUnique: true,
            isNullable: false
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('freelancer', 'slug', new TableColumn({
            type: 'varchar',
            name: 'slug',
            length: '16',
            isUnique: true,
            isNullable: false
        }));
    }

}
