import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class AlterCompanyJobFeedbackRequestTable1581987358930 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('company_job_feedback_request', new TableColumn({
            type: 'int',
            name: 'company_id'
        }));

        await queryRunner.createForeignKey("company_job_feedback_request", new TableForeignKey({
            columnNames: ['company_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_job_feedback_request");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_id") !== -1);
        await queryRunner.dropForeignKey("company_job_feedback_request", foreignKey);
        await queryRunner.dropColumn('company_job_feedback_request', 'company_id');
    }

}
