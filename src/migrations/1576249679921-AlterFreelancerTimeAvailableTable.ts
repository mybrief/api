import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterFreelancerTimeAvailableTable1576249679921 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('freelancer_time_available', new TableColumn({
            type: 'timestamp',
            name: 'requested_analysis_at',
            default: null,
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('freelancer_time_available', 'requested_analysis_at');
    }

}
