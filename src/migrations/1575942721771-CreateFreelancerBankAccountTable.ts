import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateFreelancerBankAccountTable1575942721771 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'freelancer_bank_account',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'freelancer_id'
                },
                {
                    type: 'varchar',
                    name: 'bank',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'agency',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'account',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'full_name',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'cpf',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'account_type',
                    isNullable: false,
                    enum: ['savings', 'checking']
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("freelancer_bank_account", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("freelancer_bank_account");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("freelancer_bank_account", foreignKey);
        await queryRunner.dropTable('freelancer_bank_account');
    }

}
