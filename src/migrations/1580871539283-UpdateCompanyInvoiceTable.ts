import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class UpdateCompanyInvoiceTable1580871539283 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('company_invoice', new TableColumn({
            type: 'double',
            name: 'price',
            precision: 10,
            scale: 2,
            isNullable: true,
            default: null
        }));

        await queryRunner.addColumn('company_invoice', new TableColumn({
            type: 'double',
            name: 'discount',
            precision: 10,
            scale: 2,
            isNullable: true,
            default: null
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('company_invoice', 'price');
        await queryRunner.dropColumn('company_invoice', 'discount');
    }

}
