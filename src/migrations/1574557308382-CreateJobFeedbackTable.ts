import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateJobFeedbackTable1574557308382 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'job_feedback',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'job_id',
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'status',
                    enum: ['approval', 'disapproval'],
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'feedback',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("job_feedback", new TableForeignKey({
            columnNames: ['job_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'job',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job_feedback");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("job_id") !== -1);
        await queryRunner.dropForeignKey("job_feedback", foreignKey);
        await queryRunner.dropTable('job_feedback');
    }

}
