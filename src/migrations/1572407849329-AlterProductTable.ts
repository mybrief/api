import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterProductTable1572407849329 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('product', 'category', new TableColumn({
            type: 'enum',
            name: 'category',
            enum: ['social_media', 'strategy', 'development'],
            isNullable: false
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('product', 'category', new TableColumn({
            type: 'set',
            name: 'category',
            enum: ['social_media', 'strategy', 'development'],
            isNullable: false
        }))
    }

}
