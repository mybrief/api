import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AddSlugToCompanyTable1573093735126 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('company', new TableColumn({
            type: 'varchar',
            name: 'slug',
            length: '16',
            isUnique: true,
            isNullable: false
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('company', 'slug');
    }

}
