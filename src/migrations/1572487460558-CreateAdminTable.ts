import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateAdminTable1572487460558 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'admin',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'avatar',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'name',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'last_name',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    isUnique: true,
                    name: 'email',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('admin');
    }

}
