import {MigrationInterface, QueryRunner, TableForeignKey} from "typeorm";

export class AddForeignKeyToCompanyProductTable1576434504339 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createForeignKey('company_product', new TableForeignKey({
            columnNames: ['product_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'product',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_product");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("product_id") !== -1);
        await queryRunner.dropForeignKey("company_product", foreignKey);
    }

}
