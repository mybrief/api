import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateFreelancerProfileTable1571841418281 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'freelancer_profile',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'freelancer_id'
                },
                {
                    type: 'varchar',
                    name: 'name',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'last_name',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'avatar',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'date',
                    name: 'birthday',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'job',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'set',
                    name: 'area',
                    enum: ['designer', 'developer', 'editor'],
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'cellphone',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    name: 'location',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                }
            ]
        }), true);

        await queryRunner.createForeignKey("freelancer_profile", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("freelancer_profile");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("freelancer_profile", foreignKey);
        await queryRunner.dropTable('freelancer_profile');
    }

}
