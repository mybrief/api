import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterJobTable1574359664504 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('job', 'freelancer_id', new TableColumn({
            type: 'int',
            name: 'freelancer_id',
            isNullable: true,
            default: null
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('job', 'freelancer_id', new TableColumn({
            type: 'int',
            name: 'freelancer_id',
            isNullable: false
        }));
    }

}
