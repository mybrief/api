import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class UpdateCompanyInvoiceTable1580871037734 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('company_invoice', 'item_type', new TableColumn(
            {
                type: 'enum',
                name: 'item_type',
                enum: ['product', 'package', 'plan', 'coin'],
                isNullable: false
            }
        ));
        await queryRunner.changeColumn('company_invoice', 'item_id', new TableColumn(
            {
                type: 'int',
                name: 'item_id',
                isNullable: true,
                default: null
            },
        ));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('company_invoice', 'item_type', new TableColumn(
            {
                type: 'enum',
                name: 'item_type',
                enum: ['product', 'package', 'plan'],
                isNullable: false
            }
        ));
        await queryRunner.changeColumn('mybrief', 'item_id', new TableColumn(
            {
                type: 'int',
                name: 'item_id',
                isNullable: false,
            },
        ));
    }

}
