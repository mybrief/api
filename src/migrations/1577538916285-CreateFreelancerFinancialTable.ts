import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateFreelancerFinancialTable1577538916285 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'freelancer_financial',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'freelancer_id',
                    isNullable: false
                },
                {
                    type: 'double',
                    name: 'month_earning',
                    precision: 10,
                    scale: 2,
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'int',
                    name: 'hour_value_progress',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'date',
                    name: 'next_hourly_value_analysis',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                }
            ]
        }), true);

        await queryRunner.createForeignKey("freelancer_financial", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("freelancer_financial");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("freelancer_financial", foreignKey);
        await queryRunner.dropTable('freelancer_financial');
    }

}
