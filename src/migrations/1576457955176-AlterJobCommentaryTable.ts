import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class AlterJobCommentaryTable1576457955176 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('job_commentary', new TableColumn({
            name: 'job_id',
            type: 'int',
            isNullable: false
        }));

        await queryRunner.createForeignKey("job_commentary", new TableForeignKey({
            columnNames: ['job_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'job',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job_commentary");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("job_id") !== -1);
        await queryRunner.dropForeignKey("job_commentary", foreignKey);
        await queryRunner.dropColumn('job_commentary', 'job_id');
    }

}
