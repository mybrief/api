import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreatePackageTable1572407063376 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'package',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'name',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'description',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    name: 'image',
                    length: '255',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'int',
                    name: 'price_coin',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'double',
                    name: 'price',
                    precision: 10,
                    scale: 2,
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('package', true);
    }

}
