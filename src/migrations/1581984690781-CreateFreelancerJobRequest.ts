import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class CreateFreelancerJobRequest1581984690781 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'freelancer_job_request',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'freelancer_id',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'job_id',
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'notes',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'accepted_at',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'declined_at',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }), true);

        await queryRunner.createForeignKey("freelancer_job_request", new TableForeignKey({
            columnNames: ['job_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'job',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("freelancer_job_request", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("freelancer_job_request");
        const jobIdForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("job_id") !== -1);
        const freelancerIdForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("freelancer_job_request", jobIdForeignKey);
        await queryRunner.dropForeignKey("freelancer_job_request", freelancerIdForeignKey);
        await queryRunner.dropTable('freelancer_job_request');
    }

}
