import {MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class RemoveCoverFromJobTable1581959717783 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job', 'cover');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('job', new TableColumn({
            name: 'cover',
            type: 'varchar',
            length: '255',
            isNullable: true,
            default: null
        }));
    }

}
