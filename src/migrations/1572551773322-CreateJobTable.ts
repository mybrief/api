import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateJobTable1572551773322 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'job',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_id'
                },
                {
                    type: 'text',
                    name: 'notes',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'tinyint',
                    name: 'paused',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'timestamp',
                    name: 'start_date',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'finish_date',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'started_at',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'finished_at',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("job", new TableForeignKey({
            columnNames: ['company_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_id") !== -1);
        await queryRunner.dropForeignKey("job", foreignKey);
        await queryRunner.dropTable('job');
    }

}
