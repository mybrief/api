import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class AlterJobTable1574359337323 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('job', new TableColumn({
            type: 'int',
            name: 'reproved_number',
            isNullable: false,
            default: 0
        }));

        await queryRunner.addColumn('job', new TableColumn({
            type: 'timestamp',
            name: 'disapproved_at',
            default: null,
            isNullable: true
        }));

        await queryRunner.addColumn('job', new TableColumn({
            type: 'timestamp',
            name: 'approved_at',
            default: null,
            isNullable: true
        }));

        await queryRunner.addColumn('job', new TableColumn({
            type: 'int',
            name: 'freelancer_id',
            isNullable: false
        }));

        await queryRunner.createForeignKey("job", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job', 'reproved_number');
        await queryRunner.dropColumn('job', 'disapproved_at');
        await queryRunner.dropColumn('job', 'approved_at');

        const table = await queryRunner.getTable("job");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("job", foreignKey);
        await queryRunner.dropColumn('job', 'freelancer_id');
    }

}
