import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateJobCommentaryTable1576456329401 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'job_commentary',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    isNullable: false,
                    name: 'sender_id'
                },
                {
                    type: 'enum',
                    isNullable: false,
                    name: 'sender_type',
                    enum: ['admin', 'freelancer', 'company']
                },
                {
                    type: 'text',
                    isNullable: false,
                    name: 'commentary'
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createIndex('job_commentary', new TableIndex({
            columnNames: ['sender_id']
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job_commentary");
        const indexKey = table.indices.find(fk => fk.columnNames.indexOf("sender_id") !== -1);
        await queryRunner.dropIndex("job_commentary", indexKey);
        await queryRunner.dropTable('job_commentary');
    }

}
