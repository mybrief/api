import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterJobAttachmentTable1576711841690 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('job_attachment', new TableColumn({
            name: 'mime_type',
            type: 'varchar',
            length: '255',
            isNullable: false
        }));

        await queryRunner.addColumn('job_attachment', new TableColumn({
            name: 'bytes',
            type: 'int',
            isNullable: false
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job_attachment', 'mime_type');
        await queryRunner.dropColumn('job_attachment', 'bytes');
    }

}
