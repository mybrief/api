import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class AlterPlanProductTable1574102278007 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('plan_product', new TableColumn({
            name: 'plan_id',
            type: 'int',
            isNullable: false
        }));

        await queryRunner.createForeignKey("plan_product", new TableForeignKey({
            columnNames: ['plan_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'plan',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("plan_product");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("plan_id") !== -1);
        await queryRunner.dropForeignKey("plan_product", foreignKey);
        await queryRunner.dropColumn('plan_product', 'plan_id');
    }

}
