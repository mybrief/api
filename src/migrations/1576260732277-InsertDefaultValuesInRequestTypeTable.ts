import {MigrationInterface, QueryRunner} from "typeorm";

export class InsertDefaultValuesInRequestTypeTable1576260732277 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("INSERT INTO `request_type` (id, name) VALUES (1, 'CHANGE_TIME_AVAILABLE')");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DELETE FROM `request_type` WHERE id = 1");
    }

}
