import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class CreateCompanyJobFeedback1581958770754 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_job_feedback',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'job_id',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_user_id',
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'status',
                    enum: ['approval', 'disapproval'],
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'feedback',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }), true);

        await queryRunner.createForeignKey("company_job_feedback", new TableForeignKey({
            columnNames: ['job_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'job',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("company_job_feedback", new TableForeignKey({
            columnNames: ['company_user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company_user',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_job_feedback");
        const jobForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("job_id") !== -1);
        const companyUserForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_user_id") !== -1);
        await queryRunner.dropForeignKey("company_job_feedback", jobForeignKey);
        await queryRunner.dropForeignKey("company_job_feedback", companyUserForeignKey);
        await queryRunner.dropTable('company_job_feedback');
    }

}
