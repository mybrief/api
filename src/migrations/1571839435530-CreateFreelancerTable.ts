import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateFreelancerTable1571839435530 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'freelancer',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'username',
                    length: '16',
                    isUnique: true,
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'email',
                    length: '255',
                    isUnique: true,
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'slug',
                    length: '16',
                    isUnique: true,
                    isNullable: false
                },
                {
                    type: 'varchar',
                    name: 'password',
                    length: '255',
                    isNullable: false
                },
                {
                    type: 'tinyint',
                    name: 'valid',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('freelancer', true);
    }

}
