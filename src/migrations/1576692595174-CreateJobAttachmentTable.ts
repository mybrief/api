import {MigrationInterface, QueryRunner, Table, TableIndex, TableForeignKey} from "typeorm";

export class CreateJobAttachmentTable1576692595174 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'job_attachment',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'job_id',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'sender_id',
                    isNullable: false
                },
                {
                    type: 'enum',
                    isNullable: false,
                    name: 'sender_type',
                    enum: ['admin', 'freelancer', 'company']
                },
                {
                    type: "varchar",
                    length: '255',
                    name: 'name',
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'url',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }), true);

        await queryRunner.createForeignKey("job_attachment", new TableForeignKey({
            columnNames: ['job_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'job',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));

        await queryRunner.createIndex('job_attachment', new TableIndex({
            columnNames: ['sender_id']
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("job_attachment");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("job_id") !== -1);
        const indexKey = table.indices.find(fk => fk.columnNames.indexOf("sender_id") !== -1);
        await queryRunner.dropIndex("job_attachment", indexKey);
        await queryRunner.dropForeignKey("job_attachment", foreignKey);
        await queryRunner.dropColumn('job_attachment', 'job_id');
    }

}
