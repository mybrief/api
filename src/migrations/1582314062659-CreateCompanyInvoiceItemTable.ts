import {MigrationInterface, QueryRunner, TableForeignKey, Table} from "typeorm";

export class CreateCompanyInvoiceItemTable1582314062659 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_invoice_item',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_invoice_id',
                    isNullable: false
                },
                {
                    type: 'enum',
                    name: 'item_type',
                    enum: ['product', 'package', 'plan'],
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'item_id',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'quantity',
                    isNullable: false,
                    default: 1
                },
                {
                    type: 'enum',
                    name: 'payment_type',
                    enum: ['bank_slip','credit_card','coin'],
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                }
            ]
        }), true);

        await queryRunner.createForeignKey("company_invoice_item", new TableForeignKey({
            columnNames: ['company_invoice_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company_invoice',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_invoice_item");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_invoice_id") !== -1);
        await queryRunner.dropForeignKey("company_invoice_item", foreignKey);
        await queryRunner.dropTable('company_invoice_item');
    }

}
