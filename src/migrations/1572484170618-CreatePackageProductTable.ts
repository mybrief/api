import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreatePackageProductTable1572484170618 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'package_product',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'product_id',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'quantity',
                    isNullable: false,
                    default: 1
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("package_product", new TableForeignKey({
            columnNames: ['product_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'product',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("package_product");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("product_id") !== -1);
        await queryRunner.dropForeignKey("package_product", foreignKey);
        await queryRunner.dropTable('package_product');
    }

}
