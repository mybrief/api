import {MigrationInterface, QueryRunner, TableForeignKey, Table} from "typeorm";

export class CreateFreelancerTimeAvailableTable1575942742019 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'freelancer_time_available',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'freelancer_id'
                },
                {
                    type: 'tinyint',
                    name: 'monday',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'tinyint',
                    name: 'tuesday',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'tinyint',
                    name: 'wednesday',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'tinyint',
                    name: 'thursday',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'tinyint',
                    name: 'friday',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'tinyint',
                    name: 'saturday',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'tinyint',
                    name: 'sunday',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                }
            ]
        }), true);

        await queryRunner.createForeignKey("freelancer_time_available", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("freelancer_time_available");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("freelancer_time_available", foreignKey);
        await queryRunner.dropTable('freelancer_time_available');
    }

}
