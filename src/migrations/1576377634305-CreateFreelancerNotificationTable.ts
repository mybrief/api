import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateFreelancerNotificationTable1576377634305 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'freelancer_notification',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'freelancer_id',
                    isNullable: false
                },
                {
                    type: 'tinyint',
                    name: 'viewed',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'text',
                    name: 'title',
                    isNullable: false
                },
                {
                    type: 'text',
                    name: 'description',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'text',
                    name: 'go_to',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'notified_at',
                    default: null,
                    isNullable: true
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("freelancer_notification", new TableForeignKey({
            columnNames: ['freelancer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'freelancer',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("freelancer_notification");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("freelancer_id") !== -1);
        await queryRunner.dropForeignKey("freelancer_notification", foreignKey);
        await queryRunner.dropTable('freelancer_notification');
    }

}
