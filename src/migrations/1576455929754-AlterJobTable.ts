import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterJobTable1576455929754 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('job', 'company_id', new TableColumn({
            name: 'company_id',
            type: 'int',
            isNullable: true,
            default: null
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('job', 'company_id', new TableColumn({
            name: 'company_id',
            type: 'int',
            isNullable: false
        }));
    }

}
