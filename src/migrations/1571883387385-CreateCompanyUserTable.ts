import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreateCompanyUserTable1571883387385 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_user',
            columns: [
                {
                    type: 'int',
                    name: 'id',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isNullable: false
                },
                {
                    type: 'int',
                    name: 'company_id'
                },
                {
                    type: 'tinyint',
                    name: 'owner',
                    isNullable: false,
                    default: 0
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'name',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'last_name',
                    isNullable: false
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'avatar',
                    isNullable: true,
                    default: null
                },
                {
                    type: 'varchar',
                    length: '255',
                    name: 'password',
                    isNullable: false
                },
                {
                    type: 'set',
                    name: 'permission',
                    enum: ['purchase', 'request_job', 'approve_delivery', 'reprove_delivery'],
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'created_at',
                    default: 'current_timestamp()',
                    isNullable: false
                },
                {
                    type: 'timestamp',
                    name: 'updated_at',
                    default: null,
                    isNullable: true,
                    onUpdate: 'current_timestamp()'
                },
                {
                    type: 'timestamp',
                    name: 'deleted_at',
                    default: null,
                    isNullable: true
                }
            ]
        }), true);

        await queryRunner.createForeignKey("company_user", new TableForeignKey({
            columnNames: ['company_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'company',
            onUpdate: "CASCADE",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("company_user");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("company_id") !== -1);
        await queryRunner.dropForeignKey("company_user", foreignKey);
        await queryRunner.dropTable('company_user');
    }

}
