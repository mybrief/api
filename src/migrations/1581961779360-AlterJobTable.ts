import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class AlterJobTable1581961779360 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job', 'reproved_number');
        await queryRunner.dropColumn('job', 'disapproved_at');
        await queryRunner.dropColumn('job', 'approved_at');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('job', new TableColumn({
            type: 'int',
            name: 'reproved_number',
            isNullable: false,
            default: 0
        }));

        await queryRunner.addColumn('job', new TableColumn({
            type: 'timestamp',
            name: 'disapproved_at',
            default: null,
            isNullable: true
        }));

        await queryRunner.addColumn('job', new TableColumn({
            type: 'timestamp',
            name: 'approved_at',
            default: null,
            isNullable: true
        }));
    }

}
