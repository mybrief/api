import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AlterJobTable1576460858633 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('job', [
            new TableColumn({
                name: 'title',
                type: 'text',
                isNullable: false
            }),
            new TableColumn({
                name: 'cover',
                type: 'varchar',
                length: '255',
                isNullable: true,
                default: null
            }),
            new TableColumn({
                name: 'status',
                type: 'enum',
                isNullable: false,
                enum: ['to_do', 'doing', 'to_approval', 'to_change', 'done']
            })
        ])
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job', 'title');
        await queryRunner.dropColumn('job', 'cover');
        await queryRunner.dropColumn('job', 'status');
    }

}
