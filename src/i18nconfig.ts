import { MailerOptions } from "@nest-modules/mailer";
import { I18nOptions } from "nestjs-i18n";
import * as path from 'path';

const config: I18nOptions = {
  path: path.join(process.cwd(), '/i18n'),
  filePattern: '*.json',
  fallbackLanguage: 'pt',
  saveMissing: true
}

export = config;