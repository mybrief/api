
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from '../../modules/auth/constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt:freelancer') {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: true,
            secretOrKey: jwtConstants.secret,
        });
    }

    async validate(payload: any) {
        return {
            freelancer_id: payload.sub,
            username: payload.username
        };
    }
}