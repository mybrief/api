
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from '../../modules/auth/constants';

@Injectable()
export class JwtStrategyCompany extends PassportStrategy(Strategy, 'jwt:company') {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: true,
            secretOrKey: jwtConstants.secret,
        });
    }

    async validate(payload: any) {
        return {
            user_id: payload.sub,
            username: payload.username,
            company_id: payload.company_id
        };
    }
}