import { Module } from '@nestjs/common';
import { CommonService } from './common.service';
import { commonProviders, jobProviders } from '../providers';
import { DatabaseModule } from '../database/database.module';
import { CommonController } from './common.controller';

@Module({
  imports: [
    DatabaseModule
  ],
  providers: [
    ...commonProviders,
    ...jobProviders,
    CommonService
  ],
  exports: [
    CommonService
  ],
  controllers: [CommonController]
})
export class CommonModule {}