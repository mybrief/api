import { Controller, Get, UseGuards, Res, Req, Param } from '@nestjs/common';
import { CommonService } from './common.service';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Response, Request } from 'express';

@Controller('common')
export class CommonController {
    constructor(
        private commonService: CommonService
    ) {
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Get('banks')
    async list() {
        return await this.commonService.findAllBanks();
    }

    // @ApiBearerAuth()
    // @UseGuards(AuthGuard('jwt:freelancer'), AuthGuard('jwt:admin'))
    @Get('download/attachment/:id')
    async download(
        @Res() res: Response,
        @Req() req: Request,
        @Param('id') id: number
    ) {
        let path = process.cwd() + '/public' + await this.commonService.getAttachmentUrl(id);

        res.download(path);
    }
}
