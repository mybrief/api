import { Injectable, Inject } from '@nestjs/common';
import { Bank, City, JobAttachment } from '../../entities';
import { Repository, Like } from 'typeorm';

import * as fs from 'fs';

@Injectable()
export class CommonService {
    constructor(
        @Inject('BANK_REPOSITORY')
        private readonly bankRepository: Repository<Bank>,
        @Inject('CITY_REPOSITORY')
        private readonly cityRepository: Repository<City>,
        @Inject('JOB_ATTACHMENT_REPOSITORY')
        private readonly jobAttachmentRepository: Repository<JobAttachment>,
    ) {}

    async findBanksByName(name: string) {
        return await this.bankRepository.find({
            name: Like(`%${name.toUpperCase()}%`)
        })
    }

    async findAllBanks() {
        return await this.bankRepository.find();
    }

    async findCitiesByName(name: string) {
        return await this.cityRepository.find({
            name: Like(`%${name.toUpperCase()}%`)
        })
    }

    async getAttachmentUrl(id: number): Promise<string|null> {
        return new Promise<string|null>(async (resolve, reject) => {
            let attachment: JobAttachment = await this.jobAttachmentRepository.findOne({
                where: {
                    id: id
                },
                select: ['id', 'url']
            });

            resolve(attachment != null ? attachment.url : null)
        });
    }

    async deleteFileFromServer(path: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            try {
                fs.unlinkSync(process.cwd() +  '/public' + path);

                resolve();
            } catch(err) {
                reject(err);
            }
        });
    }
}
