/**
 * Capitalize any number of words in string
 *
 * @param str String to be replaced
 */
export function capitalLetter(str: string): string {
    let str2: string[] = str.split(" ");

    for (let i = 0, x = str2.length; i < x; i++) {
        str2[i] = str2[i][0].toUpperCase() + str2[i].substr(1);
    }

    return str2.join(" ");
}

/**
 * Removes all characters from string that are not digit
 *
 * @param str String to be replaced
 */
export function onlyNumbers(str: string): string {
    return str.replace(/\D/g,'');
}

export function replaceAccents(str: string): string {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

/**
 * Removes all extra spaces from string allowing only one space between them
 * @param str String to be replaced
 */
export function onlyOneSpaceBetweenWords(str: string): string {
    return str.trim().replace(/\s\s+/g, ' ');
}