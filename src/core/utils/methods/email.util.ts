/**
 * Check if email is valid
 *
 * @param email Email to be validated
 */
export function isValidEmail(email: string): boolean {
	return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
}