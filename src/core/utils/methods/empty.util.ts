export function isStringEmpty(str: string) {
    return str == null || str.trim() == '';
}

export function isArrayEmpty(arr: any[]) {
    return arr == null || arr.length == 0;
}

export function isObjectEmpty(obj: object) {
    return obj == null || Object.keys(obj).length === 0;
}

export function isEmpty(data: any)
{
    if(typeof(data) == 'number' || typeof(data) == 'boolean')
    {
        return false;
    } else if(typeof(data) == 'undefined' || data === null)
    {
        return true;
    } else if(typeof(data.length) != 'undefined')
    {
        return data.length == 0;
    }

    var count = 0;
    for(var i in data)
    {
        if(data.hasOwnProperty(i))
        {
            count ++;
        }
    }

    return count == 0;
}