// Company
export { isValidCNPJ } from "./methods/company.util";

// String
export { capitalLetter } from "./methods/string.util";
export { onlyNumbers } from "./methods/string.util";
export { replaceAccents } from "./methods/string.util";
export { onlyOneSpaceBetweenWords } from "./methods/string.util";

// Email
export { isValidEmail } from "./methods/email.util";

// Empty
export { isStringEmpty, isArrayEmpty, isObjectEmpty, isEmpty } from './methods/empty.util';