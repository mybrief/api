import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateFreelancerLogDto, CreateCompanyUserLogDto, CreateAdminLogDto } from '../../dtos';
import { AdminLog, CompanyUserLog, FreelancerLog } from '../../interfaces';

@Injectable()
export class LogService {
    constructor(
        @Inject('ADMIN_LOG_MODEL')
        private readonly adminLogModel: Model<AdminLog>,
        @Inject('FREELANCER_LOG_MODEL')
        private readonly freelancerLogModel: Model<FreelancerLog>,
        @Inject('COMPANY_USER_LOG_MODEL')
        private readonly companyUserLogModel: Model<CompanyUserLog>
    ) {}

    async createAdminLog(createAdminLogDto: CreateAdminLogDto): Promise<AdminLog> {
        return await new this.adminLogModel(createAdminLogDto).save();
    }

    async createFreelancerLog(createFreelancerLogDto: CreateFreelancerLogDto): Promise<FreelancerLog> {
        return await new this.freelancerLogModel(createFreelancerLogDto).save();
    }

    async createCompanyUserLog(createCompanyUserLog: CreateCompanyUserLogDto): Promise<CompanyUserLog> {
        return await new this.companyUserLogModel(createCompanyUserLog).save();
    }

    async freelancerLogs(): Promise<FreelancerLog[]> {
        return await this.freelancerLogModel.find().exec();
    }

    async adminLogs(): Promise<AdminLog[]> {
        return await this.adminLogModel.find().exec();
    }

    async companyUserLogs(): Promise<CompanyUserLog[]> {
        return await this.companyUserLogModel.find().exec();
    }
}
