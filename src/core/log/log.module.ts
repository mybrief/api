import { Module } from '@nestjs/common';
import { LogService } from './log.service';
import { DatabaseModule } from '../database/database.module';
import { logProviders } from '../providers/log.providers';

@Module({
  imports: [
    DatabaseModule
  ],
  providers: [
    LogService,
    ...logProviders
  ],
  exports: [
    LogService
  ]
})
export class LogModule {}
