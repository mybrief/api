import {
    MessageBody,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
    WsResponse,
    OnGatewayConnection,
    OnGatewayDisconnect,
  } from '@nestjs/websockets';
  import { Server, Socket } from 'socket.io';

  @WebSocketGateway()
  export class RealtimeGateway implements OnGatewayConnection, OnGatewayDisconnect {
    usersOnline: number = 0;

    async handleConnection(){
      // A client has connected
      this.usersOnline++;
    }

    async handleDisconnect(){
      // A client has disconnected
      this.usersOnline--;
    }

    @SubscribeMessage('connectFreelancer')
    async onFreelancerConnecting(client: Socket, data: {freelancerId: number, username: string}){
      // client.broadcast.emit('data', data);
      console.log('New freelancer connection', data);
    }
  }