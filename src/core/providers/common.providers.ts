
import { Connection, Repository } from 'typeorm';
import { Bank, City, Neighborhood, Street } from '../../entities';

export const commonProviders = [
  {
    provide: 'BANK_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Bank),
    inject: ['DATABASE_CONNECTION_BANKS'],
  },
  {
    provide: 'CITY_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(City),
    inject: ['DATABASE_CONNECTION_LOCATION'],
  },
  {
    provide: 'STREET_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Street),
    inject: ['DATABASE_CONNECTION_LOCATION'],
  },
  {
    provide: 'NEIGHBORHOOD_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Neighborhood),
    inject: ['DATABASE_CONNECTION_LOCATION'],
  }
];