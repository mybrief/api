
import { Connection, Repository } from 'typeorm';
import { Plan, PlanProduct } from '../../entities';

export const planProviders = [
    {
        provide: 'PLAN_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(Plan),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'PLAN_PRODUCT_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(PlanProduct),
        inject: ['DATABASE_CONNECTION'],
    },
]