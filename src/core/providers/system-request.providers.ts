
import { Connection, Repository } from 'typeorm';
import { SystemRequest, RequestType } from '../../entities';

export const systemRequestProviders = [
    {
        provide: 'SYSTEM_REQUEST_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(SystemRequest),
        inject: ['DATABASE_CONNECTION']
    },
    {
        provide: 'REQUEST_TYPE_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(RequestType),
        inject: ['DATABASE_CONNECTION']
    }
]