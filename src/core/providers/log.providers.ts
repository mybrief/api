
import { Connection } from 'mongoose';
import { CompanyUserLogSchema, FreelancerLogSchema, AdminLogSchema } from '../../schemas';

export const logProviders = [
  {
    provide: 'ADMIN_LOG_MODEL',
    useFactory: (connection: Connection) => connection.model('AdminLog', AdminLogSchema),
    inject: ['DATABASE_MONGOOSE_CONNECTION'],
  },
  {
    provide: 'COMPANY_USER_LOG_MODEL',
    useFactory: (connection: Connection) => connection.model('CompanyUserLog', CompanyUserLogSchema),
    inject: ['DATABASE_MONGOOSE_CONNECTION'],
  },
  {
    provide: 'FREELANCER_LOG_MODEL',
    useFactory: (connection: Connection) => connection.model('FreelancerLog', FreelancerLogSchema),
    inject: ['DATABASE_MONGOOSE_CONNECTION'],
  }
];