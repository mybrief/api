
import { Connection, Repository } from 'typeorm';
import { CompanyUser } from '../../entities';

export const companyUserProviders = [
  {
    provide: 'COMPANY_USER_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(CompanyUser),
    inject: ['DATABASE_CONNECTION'],
  },
];