import { CompanyInvoiceItem } from './../../entities/company-invoice-item.entity';

import { Connection, Repository } from 'typeorm';
import {
    CompanyUser,
    Company,
    CompanyInvoice,
    CompanyJobFeedback,
    CompanyJobFeedbackRequest,
    CompanyDemand,
    CompanyDemandAnnex,
    CompanyDemandFeedback
} from '../../entities';

export const companyProviders = [
    {
        provide: 'COMPANY_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(Company),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'COMPANY_USER_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(CompanyUser),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'COMPANY_INVOICE_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(CompanyInvoice),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'COMPANY_INVOICE_ITEM_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(CompanyInvoiceItem),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'COMPANY_JOB_FEEDBACK_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(CompanyJobFeedback),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'COMPANY_JOB_FEEDBACK_REQUEST_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(CompanyJobFeedbackRequest),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'COMPANY_DEMAND_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(CompanyDemand),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'COMPANY_DEMAND_ANNEX_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(CompanyDemandAnnex),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'COMPANY_DEMAND_FEEDBACK_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(CompanyDemandFeedback),
        inject: ['DATABASE_CONNECTION'],
    }
]