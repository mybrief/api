
import { createConnection } from 'typeorm';

import * as ormConfig from '../../ormconfig';
import * as ormConfigBanks from '../../ormconfig-bank';
import * as ormConfigLocation from '../../ormconfig-location';
import * as mongoose from 'mongoose';

export const databaseMongooseProviders = [
  {
    provide: 'DATABASE_MONGOOSE_CONNECTION',
    useFactory: (): Promise<typeof mongoose> =>
      mongoose.connect('mongodb://localhost/mybrief_logs', {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }),
  },
];

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => await createConnection(ormConfig),
    name: 'mybrief'
  },
  {
    provide: 'DATABASE_CONNECTION_BANKS',
    useFactory: async () => await createConnection(ormConfigBanks),
    name: 'mybrief_banks'
  },
  {
    provide: 'DATABASE_CONNECTION_LOCATION',
    useFactory: async () => await createConnection(ormConfigLocation),
    name: 'mybrief_location'
  },
];