
import { Connection, Repository } from 'typeorm';
import {
    Job,
    JobFeedback,
    JobCommentary,
    JobAttachment
} from '../../entities';

export const jobProviders = [
    {
        provide: 'JOB_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(Job),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'JOB_FEEDBACK_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(JobFeedback),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'JOB_COMMENTARY_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(JobCommentary),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'JOB_ATTACHMENT_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(JobAttachment),
        inject: ['DATABASE_CONNECTION'],
    }
]