
import { Connection, Repository } from 'typeorm';
import { Package, PackageProduct } from '../../entities';

export const packageProviders = [
    {
        provide: 'PACKAGE_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(Package),
        inject: ['DATABASE_CONNECTION'],
    },
    {
        provide: 'PACKAGE_PRODUCT_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(PackageProduct),
        inject: ['DATABASE_CONNECTION'],
    }
]