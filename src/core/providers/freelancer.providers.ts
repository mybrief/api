
import { Connection, Repository } from 'typeorm';
import {
  Freelancer,
  FreelancerProfile,
  FreelancerConfirmAccount,
  FreelancerTimeAvailable,
  FreelancerBankAccount,
  FreelancerNotification,
  FreelancerFinancial,
  FreelancerJobRequest
} from '../../entities';

export const freelancerProviders = [
  {
    provide: 'FREELANCER_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Freelancer),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'FREELANCER_PROFILE_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(FreelancerProfile),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'FREELANCER_CONFIRM_ACCOUNT_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(FreelancerConfirmAccount),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'FREELANCER_TIME_AVAILABLE_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(FreelancerTimeAvailable),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'FREELANCER_BANK_ACCOUNT_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(FreelancerBankAccount),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'FREELANCER_NOTIFICATION_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(FreelancerNotification),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'FREELANCER_FINANCIAL_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(FreelancerFinancial),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'FREELANCER_JOB_REQUEST_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(FreelancerJobRequest),
    inject: ['DATABASE_CONNECTION'],
  }
];