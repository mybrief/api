
import { Connection, Repository } from 'typeorm';
import { Product } from '../../entities';

export const productProviders = [
    {
        provide: 'PRODUCT_REPOSITORY',
        useFactory: (connection: Connection) => connection.getRepository(Product),
        inject: ['DATABASE_CONNECTION'],
    }
]