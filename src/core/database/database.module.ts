
import { Module } from '@nestjs/common';
import { databaseProviders, databaseMongooseProviders } from '../providers/database.providers';

@Module({
  providers: [...databaseProviders, ...databaseMongooseProviders],
  exports: [...databaseProviders, ...databaseMongooseProviders]
})
export class DatabaseModule {}