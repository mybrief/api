import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Joi from '@hapi/joi';

export interface EnvConfig {
    [key: string]: string;
  }

export class ConfigService {
    private readonly envConfig: EnvConfig;

    constructor(private filePath: string) {
        const config = dotenv.parse(fs.readFileSync(filePath));
        this.envConfig = this.validateInput(config);
    }

    /**
     * ### Method that validates env file with all required keys
     *
     * To create a new valid environment, you need to start node with NODE_ENV=environmentName
     *
     * The name you choose  must have an .env file with the same name, and must have all required keys
     *
     * ##### Required keys are: `DATABASE_HOST`, `DATABASE_PORT`, `DATABASE_NAME`, `DATABASE_USER`, `DATABASE_PASSWORD`, `PAGARME_KEY`
     *
     * If you don't set `NODE_ENV` development will be use as default
     *
     * @param {EnvConfig} envConfig the Turrent environment file
     */
    private validateInput(envConfig: EnvConfig): EnvConfig {
        const envVarsSchema: Joi.ObjectSchema = Joi.object({
            NODE_ENV: Joi.string()
            .valid(['development', 'production', 'test'])
            .default('development'),
            PORT: Joi.number().default(3000),
            DATABASE_HOST: Joi.string().required(),
            DATABASE_PORT: Joi.number().required(),
            DATABASE_NAME: Joi.string().required(),
            DATABASE_USER: Joi.string().required(),
            DATABASE_PASSWORD: Joi.empty(),
            BASE_PAGARME_API_URL: Joi.string().required(),
            BASE_API_URL: Joi.string().required()
        });

        const { error, value: validatedEnvConfig } = Joi.validate(
            envConfig,
            envVarsSchema,
        );
        if (error) {
            throw new Error(`NODE_ENV = ${process.env.NODE_ENV.trim()} - Config validation error (${this.filePath}): ${error.message}`);
        }
        return validatedEnvConfig;
    }

    /**
     * ### Method that gets a environments variable
     *
     * It returns a variable by key, and as optional param, you can set a
     * default value for the key in case current environment doesn't have it
     *
     * @param {string} key Key which has the value you want
     * @param {any} defaultValue The default value of the key in case current environment doesn't
     *
     * @returns {string|boolean|null} Returns the value of the key. Can be string, boolean or null
     */
    get(key: string, defaultValue: any = null): string {
        return this.envConfig[key] != undefined ? this.envConfig[key] : defaultValue;
    }

    get isProduction(): boolean {
        return process.env.NODE_ENV != null && process.env.NODE_ENV.trim() == 'production';
    }

    /**
     |
     | KEYS to database
     |
     */

    get databaseHost(): string {
        return this.envConfig.DATABASE_HOST;
    }

    get databasePort(): string {
        return this.envConfig.DATABASE_PORT;
    }

    get databaseName(): string {
        return this.envConfig.DATABASE_NAME;
    }

    get databaseUser(): string {
        return this.envConfig.DATABASE_USER;
    }

    get databasePassword(): string {
        return this.envConfig.DATABASE_PASSWORD;
    }

    /**
     |
     | KEYS to pagarme
     |
     */

    get pagarmeBaseApiUrl(): string {
        return this.envConfig.BASE_PAGARME_API_URL;
    }

    /**
     |
     | KEYS to api
     |
     */
    get baseApiUrl(): string {
        return this.envConfig.BASE_API_URL;
    }
}
