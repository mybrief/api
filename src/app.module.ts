import { Module } from '@nestjs/common';

import { I18nModule } from 'nestjs-i18n';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PagarmeModule } from './pagarme/pagarme.module';
import { ConfigModule } from './core/config/config.module';
import { DatabaseModule } from './core/database/database.module';
import { FreelancerModule } from './modules/freelancer/freelancer.module';
import { AuthModule } from './modules/auth/auth.module';
import { CompanyModule } from './modules/company/company.module';
import { CompanyUserModule } from './modules/company/company-user/company-user.module';
import { LogModule } from './core/log/log.module';

import { MailerModule } from '@nest-modules/mailer';
import { RealtimeModule } from './core/realtime/realtime.module';
import { AdminModule } from './modules/admin/admin.module';
import { CommonModule } from './core/common/common.module';

import * as mailerConfig from './mailerconfig';
import * as i18nconfig from './i18nconfig';

@Module({
  imports: [
    I18nModule.forRoot(i18nconfig),
    PagarmeModule,
    ConfigModule,
    DatabaseModule,
    FreelancerModule,
    AuthModule,
    CompanyModule,
    CompanyUserModule,
    LogModule,
    MailerModule.forRoot(mailerConfig),
    RealtimeModule,
    AdminModule,
    CommonModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
