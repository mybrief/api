import { CompanyDemand } from './company-demand.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';

@Entity()
export class CompanyDemandAnnex {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_demand_id: number;

    @Column({
        type: 'enum',
        enum: ['file','link'],
        nullable: false
    })
    notes: 'file'|'link';

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => CompanyDemand, companyDemand => companyDemand.id)
    @JoinColumn({ name: "company_demand_id" })
    company_demand: CompanyDemand;
}