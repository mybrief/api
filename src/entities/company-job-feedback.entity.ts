import { CompanyJobFeedbackRequest } from './company-job-feedback-request.entity';
import { Job } from './job.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToOne } from 'typeorm';
import { CompanyUser } from './company-user.entity';

@Entity()
export class CompanyJobFeedback {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    job_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_user_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_job_feedback_request_id: number;

    @Column({
        type: 'text',
        nullable: false
    })
    feedback: string;

    @Column({
        type: 'enum',
        enum: ['approval','disapproval'],
        nullable: false
    })
    status: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => CompanyUser, user => user.id)
    @JoinColumn({ name: "company_user_id" })
    companyUser: CompanyUser;

    @ManyToOne(type => Job, job => job.id)
    @JoinColumn({ name: "job_id" })
    job: Job;

    @OneToOne(type => CompanyJobFeedbackRequest, companyJobFeedbackRequest => companyJobFeedbackRequest.id)
    @JoinColumn({ name: 'company_job_feedback_request_id' })
    feedback_request: CompanyJobFeedbackRequest;
}