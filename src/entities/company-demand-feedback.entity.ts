import { CompanyDemand } from './company-demand.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';

@Entity()
export class CompanyDemandFeedback {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_demand_id: number;

    @Column({
        type: 'enum',
        enum: ['approval','disapproval'],
        nullable: false
    })
    status: 'approval'|'disapproval';

    @Column({
        type: 'text',
        nullable: false
    })
    feedback: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => CompanyDemand, companyDemand => companyDemand.id)
    @JoinColumn({ name: "company_demand_id" })
    company_demand: CompanyDemand;
}