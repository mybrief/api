import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Freelancer } from './freelancer.entity';

@Entity()
export class FreelancerBankAccount {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    freelancer_id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    bank: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    agency: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    account: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    full_name: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    cpf: string;

    @Column({
        type: 'enum',
        enum: ['savings','checking'],
        nullable: false
    })
    account_type: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;
}
