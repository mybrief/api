import { CompanyInvoiceItem } from './company-invoice-item.entity';
import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Company } from './company.entity';

@Entity()
export class CompanyInvoice {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_id: number;

    @Column({
        type: 'enum',
        enum: ['processing', 'authorized', 'paid', 'refunded', 'waiting_payment', 'pending_refund', 'refused'],
        nullable: false
    })
    payment_status: 'processing'|'authorized'|'paid'|'refunded'|'waiting_payment'|'pending_refund'|'refused';

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    @Column({
        type: 'double',
        nullable: true,
        default: null,
        precision: 10,
        scale: 2
    })
    price: number;

    @Column({
        type: 'double',
        nullable: true,
        default: null,
        precision: 10,
        scale: 2
    })
    discount: number;

    /**
     * Relations
     */

    @ManyToOne(type => Company, company => company.id)
    @JoinColumn({ name: "company_id" })
    company: Company;

    @OneToMany(type => CompanyInvoiceItem, companyInvoiceItem => companyInvoiceItem.invoice)
    invoice_item: CompanyInvoiceItem[];
}