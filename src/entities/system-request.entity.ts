import { Entity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany, JoinColumn } from 'typeorm';
import { Freelancer } from './freelancer.entity';
import { RequestType } from './request-type.entity';

@Entity()
export class SystemRequest {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    request_type_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    freelancer_id: number;

    @Column({
        type: 'varchar',
        nullable: true,
        default: () => null
    })
    description: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    notified_at: Date;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    resolved_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    created_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;

    @OneToOne(type => RequestType, requestType => requestType.id)
    @JoinColumn({ name: "request_type_id" })
    request_type: RequestType;
}