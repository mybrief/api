import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Company } from './company.entity';

@Entity()
export class CompanyUser {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_id: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: 0
    })
    owner: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    name: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    email: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    last_name: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    avatar: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    password: string;

    @Column({
        type: 'set',
        enum: ['purchase', 'request_job', 'approve_delivery', 'reprove_delivery'],
        nullable: false
    })
    permission: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => Company, user => user.id)
    @JoinColumn({ name: "company_id" })
    company: Company;
}