import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany } from 'typeorm';
import { Product } from './product.entity';

@Entity()
export class CompanyProduct {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    product_id: number;

    @Column({
        type: 'tinyint',
        nullable: false
    })
    consumed: number;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Product, product => product.id)
    @JoinColumn({ name: "product_id" })
    product: Product;
}