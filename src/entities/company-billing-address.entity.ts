import { Company } from './company.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';

@Entity()
export class CompanyBillingAddress {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    full_name: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    cellphone: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    zip_code: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    street: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    number: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    complement: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    neighborhood: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    city: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    state: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => Company, company => company.id)
    @JoinColumn({ name: "company_id" })
    company: Company;
}