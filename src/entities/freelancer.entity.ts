import { Entity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany } from 'typeorm';
import { FreelancerProfile } from './freelancer-profile.entity';
import { FreelancerConfirmAccount } from './freelancer-confirm-account.entity';
import { FreelancerBankAccount } from './freelancer-bank-account.entity';
import { FreelancerTimeAvailable } from './freelancer-time-available.entity';
import { SystemRequest } from './system-request.entity';
import { FreelancerFinancial } from './freelancer-financial.entity';

@Entity()
export class Freelancer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: 16,
        unique: true,
        nullable: false
    })
    username: string;

    @Column({
        type: 'varchar',
        length: 255,
        unique: true,
        nullable: false
    })
    email: string;

    @Column({
        type: 'varchar',
        length: 16,
        unique: true,
        nullable: false
    })
    slug: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    password: string;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: 0
    })
    valid: number;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => FreelancerProfile, freelancerProfile => freelancerProfile.freelancer)
    profile: FreelancerProfile;

    @OneToOne(type => FreelancerFinancial, freelancerFinancial => freelancerFinancial.freelancer)
    financial: FreelancerFinancial;

    @OneToOne(type => FreelancerBankAccount, freelancerBankAccount => freelancerBankAccount.freelancer)
    bank_account: FreelancerBankAccount;

    @OneToOne(type => FreelancerTimeAvailable, freelancerTimeAvailable => freelancerTimeAvailable.freelancer)
    time_available: FreelancerTimeAvailable;

    @OneToMany(type => FreelancerConfirmAccount, freelancerConfirmAccount => freelancerConfirmAccount.freelancer)
    confirm_account: FreelancerConfirmAccount;

    @OneToMany(type => SystemRequest, systemRequest => systemRequest.freelancer)
    requests: SystemRequest;
}
