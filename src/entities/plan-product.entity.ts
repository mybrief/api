import { Entity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { Plan } from './plan.entity';
import { Product } from './product.entity';
import { Package } from './package.entity';

@Entity()
export class PlanProduct {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    item_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    plan_id: number;

    @Column({
        type: 'enum',
        enum: ['product', 'package'],
        nullable: false
    })
    item_type: 'product'|'package';

    @Column({
        type: 'int',
        nullable: false,
        default: 1
    })
    quantity: number;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => Plan, plan => plan.id)
    @JoinColumn({ name: "plan_id" })
    plan: Plan;

    @OneToOne(type => Product, product => product.id)
    @JoinColumn({ name: "item_id" })
    product: Product;

    @OneToOne(type => Package, _package => _package.id)
    @JoinColumn({ name: "item_id" })
    package: Package;
}