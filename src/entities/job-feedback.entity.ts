import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Job } from './job.entity';

@Entity()
export class JobFeedback {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    job_id: number;

    @Column({
        type: 'enum',
        enum: ['approval', 'disapproval'],
        nullable: false
    })
    status: string;

    @Column({
        type: 'text',
        nullable: true,
        default: null
    })
    feedback: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => Job, job => job.feedback)
    @JoinColumn({ name: "job_id" })
    job: Job;
}