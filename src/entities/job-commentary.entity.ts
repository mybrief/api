import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Job } from './job.entity';
import { Freelancer } from './freelancer.entity';
import { Admin } from './admin.entity';
import { CompanyUser } from './company-user.entity';

@Entity()
export class JobCommentary {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    job_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    sender_id: number;

    @Column({
        type: 'enum',
        enum: ['admin', 'freelancer', 'company'],
        nullable: false
    })
    sender_type: string;

    @Column({
        type: 'text',
        nullable: false
    })
    commentary: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => Job, job => job.feedback)
    @JoinColumn({ name: "job_id" })
    job: Job;

    @ManyToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "sender_id" })
    freelancer: Freelancer;

    @ManyToOne(type => Admin, admin => admin.id)
    @JoinColumn({ name: "sender_id" })
    admin: Admin;

    @ManyToOne(type => CompanyUser, companyUser => companyUser.id)
    @JoinColumn({ name: "sender_id" })
    company_user: CompanyUser;
}