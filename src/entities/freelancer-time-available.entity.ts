import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Freelancer } from './freelancer.entity';

@Entity()
export class FreelancerTimeAvailable {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    freelancer_id: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    monday: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    tuesday: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    wednesday: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    thursday: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    friday: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    saturday: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    sunday: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    can_update_time_available: number;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    requested_analysis_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;
}
