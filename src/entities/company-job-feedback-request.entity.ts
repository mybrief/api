import { CompanyJobFeedback } from './company-job-feedback.entity';
import { Company } from './company.entity';
import { Job } from './job.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToOne } from 'typeorm';

@Entity()
export class CompanyJobFeedbackRequest {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    job_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_id: number;

    @Column({
        type: 'text',
        nullable: true,
        default: () => null
    })
    notes: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => Job, job => job.id)
    @JoinColumn({ name: "job_id" })
    job: Job;

    @ManyToOne(type => Company, company => company.id)
    @JoinColumn({ name: "company_id" })
    company: Company;

    @OneToOne(type => CompanyJobFeedback, companyJobFeedback => companyJobFeedback.feedback_request)
    feedback: CompanyJobFeedback;
}