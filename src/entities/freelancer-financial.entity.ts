import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Freelancer } from './freelancer.entity';

@Entity()
export class FreelancerFinancial {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    freelancer_id: number;

    @Column({
        type: 'double',
        nullable: false,
        default: 0,
        precision: 10,
        scale: 2
    })
    month_earning: number;

    @Column({
        type: 'double',
        nullable: false,
        default: 0,
        precision: 10,
        scale: 2
    })
    current_hourly_value: number;

    @Column({
        type: 'int',
        nullable: false,
        default: 0
    })
    hour_value_progress: number;

    @Column({
        type: 'date',
        nullable: true,
        default: null
    })
    next_hourly_value_analysis: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;
}
