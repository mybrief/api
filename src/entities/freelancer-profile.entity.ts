import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Freelancer } from './freelancer.entity';

@Entity()
export class FreelancerProfile {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    freelancer_id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    name: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    last_name: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    avatar: string;

    @Column({
        type: 'date',
        nullable: false
    })
    birthday: Date;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    job: string;

    @Column({
        type: 'set',
        enum: ['designer', 'developer', 'editor'],
        nullable: false
    })
    area: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    cellphone: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    location: string;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;
}
