import { Entity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany } from 'typeorm';
import { PlanProduct } from './plan-product.entity';

@Entity()
export class Plan {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    name: string;

    @Column({
        type: 'text',
        nullable: true,
        default: null
    })
    description: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: null
    })
    image: string;

    @Column({
        type: 'double',
        nullable: true,
        default: null,
        precision: 10,
        scale: 2
    })
    price: number;

    @Column({
        type: 'enum',
        enum: ['bank_slip', 'credit_card', 'all'],
        nullable: false
    })
    allowed_payment_type: string;

    @Column({
        type: 'int',
        nullable: false
    })
    charges: number;

    @Column({
        type: 'int',
        nullable: true,
        default: null
    })
    pagarme_plan_id: number;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    @OneToMany(type => PlanProduct, planProduct => planProduct.plan)
    plan_product: PlanProduct[];
}