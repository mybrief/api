import { CompanyJobFeedbackRequest } from './company-job-feedback-request.entity';
import { CompanyJobFeedback } from './company-job-feedback.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } from 'typeorm';
import { Freelancer } from './freelancer.entity';
import { Product } from './product.entity';
import { JobFeedback } from './job-feedback.entity';
import { CompanyProduct } from './company-product.entity';
import { JobCommentary } from './job-commentary.entity';
import { Company } from './company.entity';
import { JobAttachment } from './job-attachment.entity';

@Entity()
export class Job {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: true,
        default: () => null
    })
    company_id: number;

    @Column({
        type: 'int',
        nullable: true,
        default: null
    })
    freelancer_id: number;

    @Column({
        type: 'int',
        nullable: true,
        default: () => null
    })
    company_product_id: number;

    @Column({
        type: 'text',
        nullable: true,
        default: null
    })
    notes: string;

    @Column({
        type: 'text',
        nullable: false
    })
    title: string;

    @Column({
        type: 'enum',
        name: 'status',
        nullable: false,
        enum: ['to_do', 'doing', 'to_approval', 'to_change', 'done']
    })
    status: string;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: 0
    })
    paused: number;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    start_date: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    finish_date: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    started_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    finished_at: Date;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @OneToMany(type => JobFeedback, feedback => feedback.job)
    feedback: JobFeedback[];

    @OneToMany(type => JobCommentary, jobCommentary => jobCommentary.job)
    commentary: JobCommentary[];

    @OneToMany(type => JobAttachment, jobAttachment => jobAttachment.job)
    attachment: JobAttachment[];

    @ManyToOne(type => Company, company => company.job)
    @JoinColumn({ name: "company_id" })
    company: Company;

    @OneToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;

    @OneToOne(type => CompanyProduct, companyProduct => companyProduct.id)
    @JoinColumn({ name: "company_product_id" })
    company_product: CompanyProduct;

    @OneToMany(type => CompanyJobFeedback, companyJobFeedback => companyJobFeedback.job)
    company_feedback: CompanyJobFeedback[];

    @OneToMany(type => CompanyJobFeedbackRequest, companyJobFeedbackRequest => companyJobFeedbackRequest.job)
    company_feedback_request: CompanyJobFeedbackRequest[];
}