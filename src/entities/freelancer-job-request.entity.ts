import { Job } from './job.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Freelancer } from './freelancer.entity';

@Entity()
export class FreelancerJobRequest {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    freelancer_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    job_id: number;

    @Column({
        type: 'text',
        nullable: true,
        default: () => null
    })
    notes: string;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    accepted_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    declined_at: Date;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;

    @OneToOne(type => Job, job => job.id)
    @JoinColumn({ name: "job_id" })
    job: Job;
}
