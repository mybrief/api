import { CompanyDemandFeedback } from './company-demand-feedback.entity';
import { CompanyProduct } from './company-product.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Company } from './company.entity';

@Entity()
export class CompanyDemand {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_product_id: number;

    @Column({
        type: 'text',
        nullable: false
    })
    notes: string;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Company, company => company.id)
    @JoinColumn({ name: "company_id" })
    company: Company;

    @OneToOne(type => CompanyDemandFeedback, companyDemandFeedback => companyDemandFeedback.company_demand)
    feedback: CompanyDemandFeedback;

    @OneToOne(type => CompanyProduct, companyProduct => companyProduct.id)
    @JoinColumn({ name: "company_product_id" })
    company_product: CompanyProduct;
}