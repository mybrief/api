import { Entity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Package } from './package.entity';
import { Product } from './product.entity';

@Entity()
export class PackageProduct {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    product_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    package_id: number;

    @Column({
        type: 'int',
        nullable: false,
        default: 1
    })
    quantity: number;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    @ManyToOne(type => Package, _package => _package.id)
    @JoinColumn({ name: "package_id" })
    package: Package;

    @OneToOne(type => Product, product => product.id)
    @JoinColumn({ name: "product_id" })
    product: Product;

}