import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Freelancer } from './freelancer.entity';

@Entity()
export class FreelancerNotification {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    freelancer_id: number;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    viewed: number;

    @Column({
        type: 'text',
        nullable: false
    })
    title: string;

    @Column({
        type: 'text',
        nullable: true,
        default: () => null
    })
    description: string;

    @Column({
        type: 'text',
        nullable: true,
        default: () => null
    })
    go_to: string;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    notified_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;

    /**
     * Relations
     */

    @OneToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;
}
