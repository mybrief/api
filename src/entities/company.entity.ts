import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { CompanyUser } from './company-user.entity';
import { Job } from './job.entity';
import { CompanyInvoice } from './company-invoice.entity';

@Entity()
export class Company {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    name: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    cellphone: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    telephone: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    segment: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    site: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    logo: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
        default: () => null
    })
    localization: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    cnpj: string;

    @Column({
        type: 'varchar',
        length: 16,
        nullable: false,
        unique: true
    })
    slug: string;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: 0
    })
    valid: number;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: 'current_timestamp()'
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => null
    })
    deleted_at: Date;


    /**
     * Relations
     */

    @OneToMany(type => CompanyUser, companyUser => companyUser.company)
    users: CompanyUser[];

    @OneToMany(type => Job, job => job.company)
    job: Job[];

    @OneToMany(type => CompanyInvoice, invoice => invoice.company)
    invoice: CompanyInvoice[];
}