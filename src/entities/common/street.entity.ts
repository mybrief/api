import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class Street {
    @PrimaryColumn()
    zip_code: number;

    @Column({
        type: 'int',
        nullable: false
    })
    neighborhood_id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    name: string;

    @Column({
        type: 'varchar',
        length: 10,
        nullable: false
    })
    type: string;
}