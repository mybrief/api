import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class Neighborhood {
    @PrimaryColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    city_id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    name: string;
}