import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Bank {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    name: string;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false,
        default: () => ''
    })
    document: string;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: () => 0
    })
    deleted: string;
}