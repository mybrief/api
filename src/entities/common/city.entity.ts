import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class City {
    @PrimaryColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    name: string;

    @Column({
        type: 'char',
        length: 2,
        nullable: false
    })
    uf: string;
}