import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Freelancer } from './freelancer.entity';

@Entity()
export class FreelancerConfirmAccount {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    freelancer_id: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: false
    })
    token: string;

    @Column({
        type: 'tinyint',
        nullable: false,
        default: 0
    })
    consumed: number;

    @Column({
        type: 'timestamp',
        nullable: false
    })
    expires_at: Date;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    /**
     * Relations
     */

    @ManyToOne(type => Freelancer, freelancer => freelancer.id)
    @JoinColumn({ name: "freelancer_id" })
    freelancer: Freelancer;
}
