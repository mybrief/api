import { CompanyInvoice } from './company-invoice.entity';
import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { Product } from './product.entity';
import { Plan } from './plan.entity';
import { Package } from './package.entity';

@Entity()
export class CompanyInvoiceItem {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    company_invoice_id: number;

    @Column({
        type: 'enum',
        enum: ['bank_slip', 'credit_card', 'coin'],
        nullable: false
    })
    payment_type: string;

    @Column({
        type: 'int',
        nullable: true,
        default: null
    })
    item_id: number;

    @Column({
        type: 'int',
        nullable: false
    })
    quantity: number;

    @Column({
        type: 'timestamp',
        nullable: false,
        default: () => 'current_timestamp()'
    })
    created_at: Date;

    @Column({
        type: 'enum',
        enum: ['product', 'package', 'plan', 'coin'],
        nullable: false
    })
    item_type: 'product'|'package'|'plan'|'coin';

    /**
     * Relations
     */

    @ManyToOne(type => CompanyInvoice, company => company.id)
    @JoinColumn({ name: "company_invoice_id" })
    invoice: CompanyInvoice;

    @OneToOne(type => Product, product => product.id)
    @JoinColumn({ name: "item_id" })
    product: Product;

    @OneToOne(type => Plan, plan => plan.id)
    @JoinColumn({ name: "item_id" })
    plan: Plan;

    @OneToOne(type => Package, mPackage => mPackage.id)
    @JoinColumn({ name: "item_id" })
    package: Package;
}