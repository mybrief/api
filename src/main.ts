import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';

import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

import * as helmet from 'helmet';

import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
  );

  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.setViewEngine('hbs');

  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    transform: true
  }));

  app.enableCors();

  app.use(helmet());

  const options = new DocumentBuilder()
    .setTitle('My Brief API 1.0')
    .setDescription('API to control MyBrief dashboard, including freelancer area, company area and admin area')
    .setVersion('1.0')
    .addTag('Admin', 'All methods related to admin (any modules)')
    .addTag('Freelancer', 'All methods related to freelancer (any modules)')
    .addTag('Company', 'All methods related to company (any modules)')
    .addTag('Product', 'All methods related to products (including admin, company and freelancer)')
    .addTag('Plan', 'All methods related to plans (including admin, company and freelancer)')
    .addTag('Package', 'All methods related to packages (including admin, company and freelancer)')
    .addTag('Auth', 'All methods related to authentication (including admin, company and freelancer)')
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  await app.listen(8000, '0.0.0.0');
}
bootstrap();
