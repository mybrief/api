import { Injectable, Inject } from '@nestjs/common';
import { Repository, InsertResult, SelectQueryBuilder, Not, In } from 'typeorm';
import {
    Freelancer,
    FreelancerProfile,
    FreelancerConfirmAccount,
    FreelancerTimeAvailable,
    SystemRequest,
    FreelancerBankAccount,
    FreelancerNotification,
    Job,
    JobAttachment,
    JobCommentary,
    FreelancerJobRequest
} from '../../entities';
import {
    CreateFreelancerAccountDto,
    UpdateFreelancerProfileDto,
    UpdateFreelancerTimeAvailableDto,
    CreateFreelancerRequestToChangeTimeAvailableDto,
    UpdateFreelancerBankAccount,
    FilterFreelancerJobsDto
} from '../../dtos';

import { capitalLetter, onlyOneSpaceBetweenWords } from '../../core/utils';

import * as bcrypt from 'bcrypt';

import * as moment from 'moment';

import * as crypto from 'crypto';
import { CommonService } from '../../core/common/common.service';
@Injectable()
export class FreelancerService {
    freelancerRelations: string[] = ['profile', 'bank_account', 'time_available', 'financial'];

    constructor(
        @Inject('FREELANCER_REPOSITORY')
        private readonly freelancerRepository: Repository<Freelancer>,
        @Inject('FREELANCER_PROFILE_REPOSITORY')
        private readonly freelancerProfileRepository: Repository<FreelancerProfile>,
        @Inject('FREELANCER_CONFIRM_ACCOUNT_REPOSITORY')
        private readonly freelancerConfirmAccountRepository: Repository<FreelancerConfirmAccount>,
        @Inject('FREELANCER_TIME_AVAILABLE_REPOSITORY')
        private readonly freelancerTimeAvailableRepository: Repository<FreelancerTimeAvailable>,
        @Inject('FREELANCER_BANK_ACCOUNT_REPOSITORY')
        private readonly freelancerBankAccountRepository: Repository<FreelancerBankAccount>,
        @Inject('FREELANCER_NOTIFICATION_REPOSITORY')
        private readonly freelancerNotificationRepository: Repository<FreelancerNotification>,
        @Inject('FREELANCER_JOB_REQUEST_REPOSITORY')
        private readonly freelancerJobRequestRepository: Repository<FreelancerJobRequest>,
        @Inject('JOB_REPOSITORY')
        private readonly jobRepository: Repository<Job>,
        @Inject('JOB_COMMENTARY_REPOSITORY')
        private readonly jobCommentaryRepository: Repository<JobCommentary>,
        @Inject('JOB_ATTACHMENT_REPOSITORY')
        private readonly jobAttachmentRepository: Repository<JobAttachment>,
        @Inject('SYSTEM_REQUEST_REPOSITORY')
        private readonly systemRequestRepository: Repository<SystemRequest>,
        private readonly commonService: CommonService
    ) {}

    async findAll(): Promise<Freelancer[]> {
        return await this.freelancerRepository.find();
    }

    async findOne(username: string): Promise<Freelancer> {
        return await this.freelancerRepository.findOne({
            where: [
                {
                    email: username
                },
                {
                    username: username
                }
            ]
        })
    }

    async findById(id: number, relations?: string[]): Promise<Freelancer> {
        let rel: string[] = [];
        if(relations != null) {
            for(let i in relations) {
                if(this.freelancerRelations.includes(relations[i])) {
                    rel.push(relations[i]);
                }
            }
        }

        let freelancer: Freelancer = await this.freelancerRepository.findOne(id, {
            relations: rel
        });

        if(freelancer != null) {
            delete freelancer['password'];
        }

        return freelancer;
    }

    async updateAvatar(userId: number, avatar: string) {
        return new Promise<void>(async (resolve, reject) => {
            await this.freelancerProfileRepository.update({
                freelancer_id: userId
            }, { avatar });

            resolve();
        })
    }

    async deleteAvatar(userId: number) {
        return new Promise<void>(async (resolve, reject) => {
            await this.freelancerProfileRepository.update({
                freelancer_id: userId
            }, { avatar: null });

            resolve();
        })
    }

    async createFreelancer(createFreelancerAccountDto: CreateFreelancerAccountDto): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                if(!await this.isUsernameAvailable(createFreelancerAccountDto.username)) {
                    return reject({
                        message: 'auth.ERROR.USERNAME_ALREADY_TAKEN',
                        catch: false,
                        data: {
                            args: {username: createFreelancerAccountDto.username}
                        }
                    });
                }

                if(!await this.isEmailAvailable(createFreelancerAccountDto.email)) {
                    return reject({
                        message: 'auth.ERROR.EMAIL_ALREADY_TAKEN',
                        catch: false,
                        data: {
                            args: {email: createFreelancerAccountDto.email}
                        }
                    });
                }

                let slug: string = [
                    createFreelancerAccountDto.name.trim().toLowerCase(),
                    createFreelancerAccountDto.last_name.trim().toLowerCase().split(' ').join('')
                ].join('');

                let i: number = 1;
                while(!await this.isSlugAvailable(slug)) {
                    slug += i;

                    i++;
                    continue;
                }

                let freelancerPayload = {
                    password: await bcrypt.hash(createFreelancerAccountDto.password, 10),
                    valid: 0,
                    updated_at: null,
                    slug: slug,
                    username: createFreelancerAccountDto.username,
                    email: createFreelancerAccountDto.email
                }

                let insertResult: InsertResult = await this.freelancerRepository.insert(freelancerPayload);

                let freelancerId: number = insertResult.raw.insertId;

                await this.freelancerProfileRepository.insert({
                    name: capitalLetter(createFreelancerAccountDto.name),
                    last_name: capitalLetter(createFreelancerAccountDto.last_name),
                    birthday: createFreelancerAccountDto.birthday,
                    area: createFreelancerAccountDto.area,
                    freelancer_id: freelancerId
                })

                resolve(freelancerId);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async createConfirmAccountToken(freelancerId: number): Promise<string> {
        let token: string = crypto.randomBytes(20).toString('hex');

        await this.freelancerConfirmAccountRepository.insert({
            freelancer_id: freelancerId,
            expires_at: moment().add(7, 'days').toDate(),
            token: token
        });

        return token;
    }

    async confirmAccountBytoken(token: string): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                let checkConfirmAccountToken: FreelancerConfirmAccount = await this.freelancerConfirmAccountRepository.findOne({
                    where: {
                        token: token,
                        consumed: 0
                    }
                });

                if(checkConfirmAccountToken == null) {
                    return reject({
                        catch: false,
                        message: 'auth.ERROR.CONFIRMATION_TOKEN_NOT_FOUND'
                    });
                }

                let now = moment();
                let expiresAt = moment(checkConfirmAccountToken.expires_at);

                if(now.isSameOrAfter(expiresAt)) {
                    return reject({
                        catch: false,
                        message: 'auth.ERROR.CONFIRMATION_TOKEN_EXPIRED'
                    });
                }

                await this.freelancerConfirmAccountRepository.update({
                    id: checkConfirmAccountToken.id
                }, {
                    consumed: 1
                });

                await this.freelancerRepository.update({
                    id: checkConfirmAccountToken.freelancer_id
                }, {
                    valid: 1
                });

                resolve(checkConfirmAccountToken.freelancer_id);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async isSlugAvailable(slug: string): Promise<boolean> {
        return await this.freelancerRepository.count({slug: slug}) == 0
    }

    async isUsernameAvailable(username: string): Promise<boolean> {
        return await this.freelancerRepository.count({username: username}) == 0
    }

    async isEmailAvailable(email: string): Promise<boolean> {
        return await this.freelancerRepository.count({email: email}) == 0
    }

    async changePassword(userId: number, oldPlainPassword: string, newPlainPassword: string): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let hashedPassword: string|null = await this.getUserHashedPassword(userId);

                if(hashedPassword == null) {
                    return reject({
                        message: 'auth.ERROR.USER_NOT_FOUND',
                        catch: false,
                        data: {
                            args: {username: ''}
                        }
                    });
                }

                if(!bcrypt.compareSync(oldPlainPassword, hashedPassword)) {
                    return reject({
                        message: 'freelancer.ERROR.INVALID_PASSWORD',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                // If is the same password resolve as if it was successfully the request to change the password
                if(newPlainPassword == oldPlainPassword || bcrypt.compareSync(newPlainPassword, hashedPassword)) {
                    return resolve();
                }

                let newHashedPassword: string = bcrypt.hashSync(newPlainPassword, 10);

                await this.freelancerRepository.update({
                    id: userId
                }, {
                    password: newHashedPassword
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async getUserHashedPassword(userId: number): Promise<string|null> {
        return new Promise<string|null>(async (resolve, reject) => {
            let user: Freelancer = await this.freelancerRepository.findOne({
                where: {
                    id: userId
                },
                select: ['password']
            });

            resolve(user != null ? user.password : null);
        });
    }

    async updateProfile(userId: number, updateFreelancerProfileDto: UpdateFreelancerProfileDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let email: string|null = updateFreelancerProfileDto.email;

                if(email != null && !await this.isEmailAvailable(email)) {
                    return reject({
                        message: 'auth.ERROR.EMAIL_ALREADY_TAKEN',
                        catch: false,
                        data: {
                            args: {email: email}
                        }
                    });
                }

                let fullName: string|null = updateFreelancerProfileDto.full_name;

                delete updateFreelancerProfileDto['email'];
                delete updateFreelancerProfileDto['full_name'];

                if(fullName != null) {
                    let fullNameSplit: string[] = onlyOneSpaceBetweenWords(fullName).split(' ');
                    updateFreelancerProfileDto['name'] = fullNameSplit[0];

                    fullNameSplit.splice(0, 1);
                    updateFreelancerProfileDto['last_name'] = fullNameSplit.join(' ');
                }

                if(Object.keys(updateFreelancerProfileDto).length > 0) {
                    await this.freelancerProfileRepository.update({
                        freelancer_id: userId
                    }, updateFreelancerProfileDto);
                }

                if(email != null) {
                    await this.freelancerRepository.update({
                        id: userId
                    }, {
                        email: email
                    });
                }

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async updateTimeAvailable(userId: number, updateFreelancerTimeAvailableDto: UpdateFreelancerTimeAvailableDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let canUpdateTimeAvailable: boolean = await this.freelancerTimeAvailableRepository.count({
                    where: {
                        freelancer_id: userId,
                        can_update_time_available: 1
                    }
                }) == 1;

                if(!canUpdateTimeAvailable) {
                    return reject({
                        message: 'freelancer.ERROR.CANT_UPDATE_TIME_AVAILABLE',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                updateFreelancerTimeAvailableDto['can_update_time_available'] = 0;

                await this.freelancerTimeAvailableRepository.update({
                    freelancer_id: userId
                }, updateFreelancerTimeAvailableDto);

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async createRequestToChangeTimeAvailable(userId: number, createFreelancerRequestToChangeTimeAvailableDto: CreateFreelancerRequestToChangeTimeAvailableDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.systemRequestRepository.insert({
                    freelancer_id: userId,
                    description: createFreelancerRequestToChangeTimeAvailableDto.description,
                    request_type_id: 1
                });

                await this.freelancerTimeAvailableRepository.update({
                    freelancer_id: userId
                }, {
                    requested_analysis_at: moment().toDate()
                })

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async updateBankAccount(userId: number, updateFreelancerBankAccount: UpdateFreelancerBankAccount): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.freelancerBankAccountRepository.update({
                    freelancer_id: userId
                }, updateFreelancerBankAccount);

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async getNotifications(userId: number): Promise<FreelancerNotification[]> {
        return new Promise<FreelancerNotification[]>(async (resolve, reject) => {
            try {
                resolve(await this.freelancerNotificationRepository.find({
                    freelancer_id: userId,
                    deleted_at: null
                }));
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async attachToJob(jobId: number, userId: number, url: string, originalName: string, mimeType: string, bytes: number): Promise<JobAttachment> {
        return new Promise<JobAttachment>(async (resolve, reject) => {
            try {
                if(await this.jobRepository.findOne({
                    where: {
                        id: jobId,
                        status: In(['doing', 'to_change'])
                    },
                    select: ['id']
                }) == null) {
                    return reject({
                        message: 'freelancer.ERROR.INVALID_JOB_OR_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                let payload: any = {
                    job_id: jobId,
                    sender_id: userId,
                    sender_type: 'freelancer',
                    name: originalName,
                    url: url,
                    mime_type: mimeType,
                    bytes: bytes
                }

                let insert: InsertResult = await this.jobAttachmentRepository.insert(payload);

                payload['id'] = insert.identifiers[0].id;
                payload['created_at'] = moment().toDate();

                resolve(payload);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        })
    }

    async getLatestJobs(userId: number): Promise<Job[]> {
        return new Promise<Job[]>(async (resolve, reject) => {
            try {
                resolve(await this.jobRepository.find({
                    where: {
                        freelancer_id: userId,
                        paused: 0,
                        deleted_at: null
                    },
                    select: ['id', 'title', 'created_at', 'status'],
                    take: 6,
                    order: {
                        created_at: 'DESC'
                    }
                }))
            } catch (err) {
                reject({
                    catch: true,
                    error: err.message,
                    data: err
                });
            }
        });
    }

    async getJobs(userId: number, filters: FilterFreelancerJobsDto): Promise<Job[]> {
        return new Promise<Job[]>(async (resolve, reject) => {
            try {
                let companiesFilter: string = "";

                if(filters.companies != null && filters.companies.length > 0) {
                    companiesFilter = `company.id IN (${filters.companies.join(',')})`;
                }

                let order: 'DESC'|'ASC' = "DESC";
                let orderBy: string = "job.created_at";

                if(filters.order_by == 'most_recent') {
                    order = "DESC";
                    orderBy = "job.created_at";
                } else if (filters.order_by == 'less_recent') {
                    order = "ASC";
                    orderBy = "job.created_at";
                } else if(filters.order_by == 'company') {
                    order = "ASC";
                    orderBy = "company.name";
                } else if(filters.order_by == 'more_finish_date') {
                    order = "DESC";
                    orderBy = "job.finish_date";
                } else if(filters.order_by == 'less_finish_date') {
                    order = "ASC";
                    orderBy = "job.finish_date";
                }

                let orderObj = {};
                orderObj[orderBy] = order;

                let jobs: SelectQueryBuilder<Job> = this.jobRepository.createQueryBuilder('job')
                    .where({
                        freelancer_id: userId,
                        paused: 0,
                        deleted_at: null,
                    })
                    .innerJoin('job.company', 'company', companiesFilter)
                    .leftJoinAndSelect('job.commentary', 'commentary')
                    .leftJoinAndSelect('job.attachment', 'attachment')
                    .leftJoinAndSelect('commentary.freelancer', 'freelancer')
                    .leftJoinAndSelect('commentary.admin', 'admin')
                    .leftJoinAndSelect('freelancer.profile', 'profile')
                    .orderBy({
                        ...orderObj,
                        'commentary.created_at': 'DESC'
                    })
                    .select([
                        'job.id',
                        'job.notes',
                        'job.title',
                        'job.started_at',
                        'job.start_date',
                        'job.finish_date',
                        'job.finished_at',
                        'job.status',
                        'attachment',
                        'commentary.sender_type',
                        'commentary.commentary',
                        'commentary.sender_id',
                        'commentary.created_at',
                        'freelancer.id',
                        'freelancer.username',
                        'profile.name',
                        'profile.last_name',
                        'profile.avatar',
                        'admin.id',
                        'admin.avatar',
                        'admin.name',
                        'admin.last_name',
                        'company.id',
                        'company.name',
                        'company.logo'
                    ]);

                resolve(await jobs.getMany());
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async changeJobStatus(userId: number, jobId: number, status: 'to_do'|'doing'|'to_approval'): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let job: Job = await this.jobRepository.findOne({
                    where: {
                        freelancer_id: userId,
                        id: jobId,
                        status: Not(In(['to_approval', 'done']))
                    }
                });

                if(job == null) {
                    return reject({
                        message: 'freelancer.ERROR.INVALID_JOB_OR_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                if(job.status == status) {
                    return resolve();
                }

                let updateOpt = {};

                if(status == 'doing' && job.started_at == null) {
                    updateOpt['started_at'] = moment().toDate();
                } else if(status == 'to_approval') {
                    updateOpt['finished_at'] = moment().toDate();
                }

                await this.jobRepository.update({
                    freelancer_id: userId,
                    id: jobId
                }, {
                    status: status,
                    ...updateOpt
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async deleteAttachment(attachmentId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let attachment: JobAttachment = await this.jobAttachmentRepository.findOne({
                    where: {
                        id: attachmentId
                    },
                    select: ['id', 'job_id', 'url']
                });

                if(attachment == null) {
                    return reject({
                        message: 'freelancer.ERROR.ATTACHMENT_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                let canDelete: boolean = await this.jobRepository.count({ id: attachment.job_id, status: In(['doing', 'to_change'])}) > 0;

                if(!canDelete) {
                    return reject({
                        message: 'freelancer.ERROR.INVALID_JOB_OR_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                await this.commonService.deleteFileFromServer(attachment.url);
                await this.jobAttachmentRepository.delete({
                    id: attachmentId
                });
                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    error: err.message,
                    data: err
                })
            }
        });
    }

    async commentInJob(jobId: number, userId: number, commentary: string): Promise<JobCommentary> {
        return new Promise<JobCommentary>(async (resolve, reject) => {
            try {
                if(await this.jobRepository.count({ id: jobId }) == 0) {
                    return reject({
                        message: 'freelancer.ERROR.INVALID_JOB_OR_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                let payload: any = {
                    job_id: jobId,
                    commentary: commentary,
                    sender_type: 'freelancer',
                    sender_id: userId
                }

                let insert: InsertResult = await this.jobCommentaryRepository.insert(payload);

                let freelancer: Freelancer = await this.freelancerRepository.createQueryBuilder('freelancer')
                                                    .where({
                                                        id: userId
                                                    })
                                                    .innerJoinAndSelect('freelancer.profile', 'profile')
                                                    .select([
                                                        'freelancer.id',
                                                        'freelancer.username',
                                                        'profile.name',
                                                        'profile.last_name',
                                                        'profile.avatar',
                                                    ])
                                                    .getOne();

                payload['id'] = insert.identifiers[0].id;
                payload['created_at'] = moment().toDate();
                payload['freelancer'] = freelancer;

                resolve(payload);
            } catch (err) {
                reject({
                    catch: true,
                    error: err.message,
                    data: err
                })
            }
        });
    }

    async getJobRequests(freelancerId: number): Promise<FreelancerJobRequest[]> {
        return new Promise<FreelancerJobRequest[]>(async (resolve, reject) => {
            try {
                const requests = await this.freelancerJobRequestRepository.find({
                    where: {
                        freelancer_id: freelancerId,
                        accepted_at: null,
                        declined_at: null
                    },
                    select: ['id', 'freelancer_id', 'job_id', 'notes', 'created_at']
                });

                resolve(requests);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    async answerJobRequest(freelancerId: number, requestId: number, accepted: boolean): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                const payload = {};

                if(accepted) {
                    payload['accepted_at'] = moment().toDate();
                    payload['declined_at'] = null;
                } else {
                    payload['declined_at'] = moment().toDate();
                    payload['accepted_at'] = null;
                }

                await this.freelancerJobRequestRepository.update({
                    id: requestId,
                    freelancer_id: freelancerId
                }, payload);

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }
}
