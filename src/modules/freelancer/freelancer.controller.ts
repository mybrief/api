import {
    Controller,
    Get,
    UseGuards,
    Req,
    Post,
    Body,
    HttpException,
    HttpStatus,
    Param,
    Query,
    Put,
    UseInterceptors,
    UploadedFile,
    Delete
} from '@nestjs/common';
import { FreelancerService } from './freelancer.service';
import { AuthGuard } from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiUseTags,
    ApiOperation,
    ApiCreatedResponse,
    ApiBadRequestResponse,
    ApiOkResponse
} from '@nestjs/swagger';
import { LogService } from '../../core/log/log.service';
import { Request } from 'express';
import { MailerService } from '@nest-modules/mailer';
import {
    CreateFreelancerAccountDto,
    UpdateFreelancerPasswordDto,
    UpdateFreelancerProfileDto,
    UpdateFreelancerTimeAvailableDto,
    CreateFreelancerRequestToChangeTimeAvailableDto,
    UpdateFreelancerBankAccount,
    FilterFreelancerJobsDto,
    UpdateFreelanerJobStatusDto,
    CreateJobCommentaryDto,
    UpdateFreelancerJobRequestDto
} from '../../dtos';
import { I18nService, I18nLang } from 'nestjs-i18n';

import * as moment from 'moment';
import { onlyOneSpaceBetweenWords } from '../../core/utils';
import { FileInterceptor } from '@nestjs/platform-express';

import { diskStorage } from 'multer';
import * as fs from 'fs';
import { Job, JobAttachment } from '../../entities';
import { ConfigService } from '../../core/config/config.service';

@ApiUseTags('Freelancer')
@Controller('freelancer')
export class FreelancerController {
    constructor(
        private readonly freelancerService: FreelancerService,
        private readonly logService: LogService,
        private readonly mailerService: MailerService,
        private readonly i18n: I18nService,
        private readonly configService: ConfigService
    ) {
    }

    @ApiOperation({
        title: 'Get freelancer job requests',
        description: `
            Method to get all freelancer job requests
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Put('job/request/:requestId/answer')
    async answerJobRequests(
        @Req() req: Request,
        @Param('requestId') requestId: number,
        @Body() updateFreelancerJobRequestDto: UpdateFreelancerJobRequestDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.freelancerService.answerJobRequest(req.user['freelancer_id'], requestId, updateFreelancerJobRequestDto.accepted)
            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Get freelancer job requests',
        description: `
            Method to get all freelancer job requests
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Get('job/request')
    async getJobRequests(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                data: await this.freelancerService.getJobRequests(req.user['freelancer_id'])
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Add a new commentary to job',
        description: `
            Method to add a new commentary to job
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated freelancer and job and added the comment to job`
    })
    @ApiBadRequestResponse({
        description: `This means that the job or freelancer was not found.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Post('job/:jobId/comment')
    async commentInJob(
        @Req() req: Request,
        @Param('jobId') jobId: number,
        @Body() createJobCommentaryDto: CreateJobCommentaryDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                data: await this.freelancerService.commentInJob(jobId, req.user['freelancer_id'], createJobCommentaryDto.commentary)
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            console.log(err.data);

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Delete a attachment from the job',
        description: `
            Method to delet a attachment from the job
        `
    })
    @ApiOkResponse({
        description: `The system successfully validated freelancer, attachment and job and deleted the attachment`
    })
    @ApiBadRequestResponse({
        description: `This means that the job or freelancer or attachment was not found.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Delete('job/attachment/:attachmentId')
    async deleteAttachment(
        @Req() req: Request,
        @Param('attachmentId') attachmentId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.freelancerService.deleteAttachment(attachmentId);

            return {
                error: false,
                message: this.i18n.translate('freelancer.ATTACHMENT_DELETED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'New attachment to the job',
        description: `
            Method to create a nre attachment to the job
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated freelancer and job and created the attachment`
    })
    @ApiBadRequestResponse({
        description: `This means that the job or freelancer was not found.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Post('job/:jobId/attach')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: (req: Request, file: Express.Multer.File, cb) => {
                let dir = process.cwd() +  '/public/upload/freelancers/'+req.user['username']+'/job';

                if (!fs.existsSync(dir)) {
                    fs.mkdir(dir, { recursive: true }, err => cb(err, dir));
                } else {
                    cb(null, dir);
                }
            },
            filename: (req: Request, file: Express.Multer.File, cb) => {
                const randomName = Array(16).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');

                return cb(null, `${randomName}_-_${file.originalname}`)
            }
        })
    }))
    async attachToJob(
        @Req() req: Request,
        @UploadedFile() file: Express.Multer.File,
        @Param('jobId') jobId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            let url: string = '/upload/freelancers/'+ req.user['username'] + '/job/' + file.filename;
            let attachment: JobAttachment = await this.freelancerService.attachToJob(jobId, req.user['freelancer_id'], url, file.originalname, file.mimetype, file.size);

            if(!this.configService.isProduction) {
                attachment.url = req.protocol + '://' + req.get('host') + url;
            } else {
                attachment.url = this.configService.baseApiUrl + url;
            }

            return {
                error: false,
                data: attachment
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Change job status',
        description: `
            Method to change job status
        `
    })
    @ApiOkResponse({
        description: `The system successfully validated freelancer and job and changed the status`
    })
    @ApiBadRequestResponse({
        description: `This means that the job or freelancer was not found or status was invalid.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Put('job/status/:jobId')
    async changeJobStatus(
        @Req() req: Request,
        @Body() updateFreelanerJobStatusDto: UpdateFreelanerJobStatusDto,
        @Param('jobId') jobId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.freelancerService.changeJobStatus(req.user['freelancer_id'], jobId, updateFreelanerJobStatusDto.status)
            return {
                error: false,
                message: this.i18n.translate('freelancer.JOB_STATUS_UPDATED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Get latest jobs',
        description: `
            Method to get latest 6 job to show as a list
        `
    })
    @ApiOkResponse({
        description: `The system successfully validated freelancer and return the list`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Get('jobs/latest')
    async getLatestJobs(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            return {
                error: false,
                data: await this.freelancerService.getLatestJobs(req.user['freelancer_id'])
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Get all freelancer job',
        description: `
            Method to get all freelancers job to show as a kanban
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated freelancer and job and changed the status`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Post('jobs')
    async getJobs(
        @Req() req: Request,
        @Body() filterFreelancerJobsDto: FilterFreelancerJobsDto

    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            let jobs: Job[] = await this.freelancerService.getJobs(req.user['freelancer_id'], filterFreelancerJobsDto);

            jobs.map((job: Job) => {
                job.attachment.map(attachment => {
                    if(!this.configService.isProduction) {
                        attachment.url = req.protocol + '://' + req.get('host') + attachment.url;
                    } else {
                        attachment.url = this.configService.baseApiUrl + attachment.url;
                    }
                });
            });

            return {
                error: false,
                data: jobs
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Get all freelancer notifications',
        description: `
            Method to get all freelancers notifications from the system (not emails)
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Get('notifications')
    async getNotifications(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            return {
                error: false,
                data: await this.freelancerService.getNotifications(req.user['freelancer_id'])
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update freelancer bank account',
        description: `
            Method to change freelancer current bank account
        `
    })
    @ApiOkResponse({
        description: `The system successfully validated freelancer and changed the bank account`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Put('bank-account')
    async updateBankAccount(
        @Req() req: Request,
        @Body() updateFreelancerBankAccount: UpdateFreelancerBankAccount
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.freelancerService.updateBankAccount(req.user['freelancer_id'], updateFreelancerBankAccount);
            return {
                error: false,
                message: this.i18n.translate('freelancer.BANK_ACCOUNT_UPDATED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update freelancer profile',
        description: `
            Method to update freelancer profile (public informations)
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Put('profile')
    async updateProfile(
        @Req() req: Request,
        @Body() updateFreelancerProfileDto: UpdateFreelancerProfileDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.freelancerService.updateProfile(req.user['freelancer_id'], updateFreelancerProfileDto);
            return {
                error: false,
                message: this.i18n.translate('freelancer.PROFILE_UPDATED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update freelancers avatar'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Put('profile/avatar')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: (req: Request, file: Express.Multer.File, cb) => {
                let dir = process.cwd() +  '/public/upload/freelancers/'+req.user['username']+'/profile/avatar';

                if (!fs.existsSync(dir)) {
                    fs.mkdir(dir, { recursive: true }, err => cb(err, dir));
                } else {
                    cb(null, dir);
                }
            },
            filename: (req: Request, file: Express.Multer.File, cb) => {
                const randomName = Array(16).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');

                return cb(null, `${randomName}_-_${file.originalname}`)
            }
        })
    }))
    async updateAvatar(
        @Req() req: Request,
        @UploadedFile() file: Express.Multer.File,
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            let url: string = '/upload/freelancers/'+ req.user['username'] + '/profile/avatar/' + file.filename;
            await this.freelancerService.updateAvatar(req.user['freelancer_id'], url);

            if(!this.configService.isProduction) {
                url = req.protocol + '://' + req.get('host') + url;
            } else {
                url = this.configService.baseApiUrl + url;
            }
            return {
                error: false,
                message: this.i18n.translate('freelancer.PROFILE_UPDATED_SUCCESSFULLY', {
                    lang: language
                }),
                data: {
                    url
                }
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Remove freelancers avatar'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Delete('profile/avatar')
    async deleteAvatar(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.freelancerService.deleteAvatar(req.user['freelancer_id']);
            return {
                error: false,
                message: this.i18n.translate('freelancer.PROFILE_UPDATED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'New request to update available time',
        description: `
            Method to create a new request to update current available time
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Post('request/time-available')
    async newRequestToChangeAvailableTime(
        @Req() req: Request,
        @Body() createFreelancerRequestToChangeTimeAvailableDto: CreateFreelancerRequestToChangeTimeAvailableDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.freelancerService.createRequestToChangeTimeAvailable(req.user['freelancer_id'], createFreelancerRequestToChangeTimeAvailableDto);

            return {
                error: false,
                message: this.i18n.translate('freelancer.REQUEST_TO_CHANGE_TIME_AVAILABLE_CREATED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update available time',
        description: `
            Method to update current available time
        `
    })
    @ApiBadRequestResponse({
        description: 'That means that the freelancer cannot update his current available time'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Put('time-available')
    async updateTimeAvailable(
        @Req() req: Request,
        @Body() updateFreelancerTimeAvailableDto: UpdateFreelancerTimeAvailableDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.freelancerService.updateTimeAvailable(req.user['freelancer_id'], updateFreelancerTimeAvailableDto);

            return {
                error: false,
                message: this.i18n.translate('freelancer.TIME_AVAILABLE_UPDATED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update freelancer password',
        description: `
            Method to update current password
        `
    })
    @ApiBadRequestResponse({
        description: 'That means that the current password is not correct'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Put('password')
    async updatePassword(
        @Req() req: Request,
        @Body() updateFreelancerPasswordDto: UpdateFreelancerPasswordDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.freelancerService.changePassword(req.user['freelancer_id'], updateFreelancerPasswordDto.old_password, updateFreelancerPasswordDto.new_password)
            return {
                error: false,
                message: this.i18n.translate('freelancer.PASSWORD_CHANGED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Get freelancer profile',
        description: `
            Method to get current profile
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Get()
    async getProfile(
        @Req() req: Request,
        @Query('scope') query: string
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            const data = await this.freelancerService.findById(req.user['freelancer_id'], onlyOneSpaceBetweenWords(query).split(' '));
            
            if(data && data.profile && data.profile.avatar) {
                if(!this.configService.isProduction) {
                    data.profile.avatar = req.protocol + '://' + req.get('host') + data.profile.avatar;
                } else {
                    data.profile.avatar = this.configService.baseApiUrl + data.profile.avatar;
                }
            }

            return {
                error: false,
                data
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Create Freelancer',
        description: `
            Method to create a new freelancer.
            It will create a freelancer authentication information, profile and a token to confirm his account.
            With the token, will send a email with the confirmation URL
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully created freelancer's account and send a confirmation email to his email.`
    })
    @ApiBadRequestResponse({
        description: `This means that either the email or username is already in use.`
    })
    @Post()
    async createFreelancer(
        @Body() createFreelancerAccountDto: CreateFreelancerAccountDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let id =  await this.freelancerService.createFreelancer(createFreelancerAccountDto);

            await this.logService.createFreelancerLog({
                freelancerId: id,
                userAgent: req.headers['user-agent'],
                ip: req.ip,
                title: 'log.ACCOUNT_CREATED',
                description: 'log.ACCOUNT_CREATED_DESCRIPTION',
                createdAt: moment().toDate(),
                actionType: 'log.ACTION_TYPE.SIGN_UP'
            });

            let token: string = await this.freelancerService.createConfirmAccountToken(id);

            await this.logService.createFreelancerLog({
                freelancerId: id,
                userAgent: req.headers['user-agent'],
                ip: req.ip,
                title: 'log.CONFIRM_ACCOUNT_CREATED',
                description: 'log.CONFIRM_ACCOUNT_CREATED_DESCRIPTION',
                createdAt: moment().toDate(),
                actionType: 'log.ACTION_TYPE.CONFIRM_ACCOUNT'
            });

            await this.mailerService.sendMail({
                to: createFreelancerAccountDto.email,
                from: 'noreply@mybrief.com.br',
                subject: this.i18n.translate('auth.CONFIRM_YOUR_ACCOUNT', {lang: language}),
                html: this.i18n.translate('auth.CONFIRM_YOUR_ACCOUNT_DESCRIPTION', {
                    lang: language,
                    args: {
                        name: createFreelancerAccountDto.name + ' ' + createFreelancerAccountDto.last_name,
                        token: token
                    }
                })
            });

            return {
                error: false,
                message: this.i18n.translate('auth.FREELANCER_SUCCESSFULLY_REGISTERED', {
                    lang: language
                }),
                data: {
                    id: id
                }
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: `Confirm freelancer's account by token`,
        description: `
            Method to confirm a freelancer account by token.
            It will check the given token, set the token as consumed and confirm the account, allowing the freelancer to login.
        `
    })
    @ApiBadRequestResponse({
        description: `This means that either the token was not found (or already used) or the token was expired.`
    })
    @ApiOkResponse({
        description: `The account is confirmed and the freelancer can now login`
    })
    @Get(':token')
    async confirmAccountByToken(
        @Param('token') token: string,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let id: number = await this.freelancerService.confirmAccountBytoken(token);

            await this.logService.createFreelancerLog({
                freelancerId: id,
                userAgent: req.headers['user-agent'],
                ip: req.ip,
                title: 'log.ACCOUNT_CONFIMED',
                description: 'log.ACCOUNT_CONFIMED_DESCRIPTION',
                createdAt: moment().toDate(),
                actionType: 'log.ACTION_TYPE.ACCOUNT_CONFIMED'
            });

            return {
                error: false,
                message: this.i18n.translate('auth.ACCOUNT_CONFIRMED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
