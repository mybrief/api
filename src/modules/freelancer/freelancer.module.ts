import { Module } from '@nestjs/common';
import { FreelancerService } from './freelancer.service';
import { FreelancerController } from './freelancer.controller';
import {
  freelancerProviders,
  systemRequestProviders,
  jobProviders
} from '../../core/providers';
import { DatabaseModule } from '../../core/database/database.module';
import { LogModule } from '../../core/log/log.module';
import { CommonModule } from '../../core/common/common.module';

@Module({
  imports: [
    DatabaseModule,
    CommonModule,
    LogModule
  ],
  controllers: [FreelancerController],
  providers: [
    ...freelancerProviders,
    ...systemRequestProviders,
    ...jobProviders,
    FreelancerService
  ],
  exports: [
    FreelancerService
  ]
})
export class FreelancerModule {}
