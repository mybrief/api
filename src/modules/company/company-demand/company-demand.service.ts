import { Repository } from 'typeorm';
import { Injectable, Inject } from '@nestjs/common';

import { CompanyDemand, CompanyDemandAnnex, CompanyDemandFeedback } from '../../../entities';
import { CreateCompanyDemandDto } from './../../../dtos';

@Injectable()
export class CompanyDemandService {
    constructor(
        @Inject('COMPANY_DEMAND_REPOSITORY')
        private readonly demandRepository: Repository<CompanyDemand>,
        @Inject('COMPANY_DEMAND_ANNEX_REPOSITORY')
        private readonly demandAnnexRepository: Repository<CompanyDemandAnnex>,
        @Inject('COMPANY_DEMAND_FEEDBACK_REPOSITORY')
        private readonly demandFeedbackRepository: Repository<CompanyDemandFeedback>
    ) {}

    async getDemands(companyId: number): Promise<CompanyDemand[]> {
        return new Promise<CompanyDemand[]>(async (resolve, reject) => {
            try {
                const demands = await this.demandRepository.find({
                    where: {
                        company_id: companyId,
                        deleted_at: null
                    },
                    order: {
                        created_at: 'DESC'
                    },
                    relations: ['company_product', 'company_product.product', 'feedback']
                });

                resolve(demands);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    async createDemand(companyId: number, createDemandDto: CreateCompanyDemandDto): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                const { company_product_id, notes, annex } = createDemandDto;

                const insertResult = await this.demandRepository.insert({
                    company_id: companyId,
                    company_product_id,
                    notes
                });

                const demandId = insertResult.raw.insertId;

                annex.map(demandAnnex => {
                    demandAnnex.company_demand_id = demandId;
                });

                await this.demandAnnexRepository.insert(annex);

                resolve(demandId);

            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }
}
