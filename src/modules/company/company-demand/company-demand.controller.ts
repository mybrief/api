import { Controller, UseGuards, Post, Get, HttpStatus, HttpException, Req, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { I18nService } from 'nestjs-i18n';
import { ApiUseTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { CompanyDemandService } from './company-demand.service';
import { CreateCompanyDemandDto } from './../../../dtos';

@ApiUseTags('Company')
@Controller('company/demand')
export class CompanyDemandController {

    constructor(
        private readonly companyDemandService: CompanyDemandService,
        private readonly i18n: I18nService
    ) {

    }

    @ApiOperation({
        title: 'Get company demands',
        description: `
            Method to get list of company demands
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Demand')
    @UseGuards(AuthGuard('jwt:company'))
    @Get()
    async getDemands(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];
            return {
                error: false,
                data: await this.companyDemandService.getDemands(companyId)
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Create company demand',
        description: `
            Method to create a new company demand
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Demand')
    @UseGuards(AuthGuard('jwt:company'))
    @Post()
    async createDemands(
        @Req() req: Request,
        @Body() createCompanyDemandDto: CreateCompanyDemandDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];
            return {
                error: false,
                data: {
                    id: await this.companyDemandService.createDemand(companyId, createCompanyDemandDto)
                }
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
