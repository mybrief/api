import { Injectable, Inject } from '@nestjs/common';
import { Repository, Like } from 'typeorm';
import { Package } from './../../../entities';
import { FilterCompanyPackagesDto } from './../../../dtos';

import { isStringEmpty } from '../../../core/utils';

@Injectable()
export class CompanyPackageService {
    constructor(
        @Inject('PACKAGE_REPOSITORY')
        private readonly packageRepository: Repository<Package>
    ) {}

    async getPackages(filters: FilterCompanyPackagesDto): Promise<Package[]> {
        return new Promise<Package[]>(async (resolve, reject) => {
            try {
                const { name } = filters;

                const optionalWhere = {};

                if(!isStringEmpty(name)) {
                    optionalWhere['name'] = Like(`%${name}%`);
                }

                const packages = await this.packageRepository.find({
                    where: {
                        ...optionalWhere,
                        deleted_at: null,
                    },
                    order: {
                        created_at: 'DESC'
                    },
                    select: ['id', 'name', 'description', 'image', 'price_coin', 'price', 'allowed_payment_type'],
                    relations: ['package_product', 'package_product.product']
                });

                resolve(packages);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
