import { Injectable, Inject } from '@nestjs/common';
import { Repository, Like, Raw } from 'typeorm';
import { Product } from './../../../entities';
import { FilterCompanyProductsDto } from './../../../dtos';

import { isStringEmpty } from '../../../core/utils';

@Injectable()
export class CompanyProductService {
    constructor(
        @Inject('PRODUCT_REPOSITORY')
        private readonly productRepository: Repository<Product>
    ) {}

    async getProducts(filters: FilterCompanyProductsDto): Promise<Product[]> {
        return new Promise<Product[]>(async (resolve, reject) => {
            try {
                const { category, name } = filters;

                const optionalWhere = {};

                if(!isStringEmpty(name)) {
                    optionalWhere['name'] = Raw(alias => `LOWER(${alias}) Like '%${name.toLowerCase()}%'`);
                }

                const products = await this.productRepository.find({
                    where: {
                        category: category,
                        deleted_at: null,
                        ...optionalWhere
                    },
                    order: {
                        created_at: 'DESC'
                    },
                    select: ['id', 'name', 'description', 'image', 'price_coin', 'price', 'allowed_payment_type']
                });

                resolve(products);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
