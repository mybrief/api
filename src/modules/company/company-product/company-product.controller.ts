import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { Controller, UseGuards, Post, Req, Body, HttpStatus, HttpException } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { ApiUseTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { FilterCompanyProductsDto } from './../../../dtos/filter-company-product.dto';

import { CompanyProductService } from './company-product.service';

@ApiUseTags('Company')
@Controller('company/product')
export class CompanyProductController {
    constructor(
        private readonly companyProductService: CompanyProductService,
        private readonly i18n: I18nService
    ) {}

    @ApiOperation({
        title: 'Method to filter and get products to company'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:company'))
    @Post()
    async getProducts(
        @Req() req: Request,
        @Body() filters: FilterCompanyProductsDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            const data = await this.companyProductService.getProducts(filters);

            return {
                error: false,
                data
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
