import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CompanyUser } from '../../../entities';
import { CreateCompanyUserDto, UpdateCompanyUserDto } from './../../../dtos';

import { isEmpty } from '../../../core/utils';

const bcrypt = require('bcrypt');

@Injectable()
export class CompanyUserService {

    constructor(
        @Inject('COMPANY_USER_REPOSITORY')
        private readonly companyUserRepository: Repository<CompanyUser>,
    ) {}

    async deleteUser(userId: number, companyId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.companyUserRepository.delete({
                    id: userId,
                    company_id: companyId
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async getUsers(): Promise<CompanyUser[]> {
        return new Promise<CompanyUser[]>(async (resolve, reject) => {
            try {
                const users = await this.companyUserRepository.find({
                    order: {
                        created_at: 'DESC'
                    },
                    where: {
                        deleted_at: null
                    }
                });

                resolve(users);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async findOne(username: string): Promise<CompanyUser> {
        return await this.companyUserRepository.findOne({
            email: username
        })
    }

    async createUser(companyId: number, createCompanyUserDto: CreateCompanyUserDto): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                const { name, last_name, email, password, permission } = createCompanyUserDto;

                let insertResult = await this.companyUserRepository.insert({
                    company_id: companyId,
                    name,
                    last_name,
                    email,
                    permission,
                    password: await bcrypt.hash(password, 10)
                });

                let userId: number = insertResult.raw.insertId;

                resolve(userId);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async updateUser(userId: number, updateCompanyUserDto: UpdateCompanyUserDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                const { name, last_name, email, password, permission, owner } = updateCompanyUserDto;

                let payload = {};

                if(!isEmpty(name)) {
                    payload = { ...payload, name };
                }

                if(!isEmpty(last_name)) {
                    payload = { ...payload, last_name };
                }

                if(!isEmpty(email)) {
                    payload = { ...payload, email };
                }

                if(!isEmpty(password)) {
                    payload = { ...payload, password: await bcrypt.hash(password, 10) };
                }

                if(!isEmpty(permission)) {
                    payload = { ...payload, permission };
                }

                if(!isEmpty(owner)) {
                    payload = { ...payload, owner };
                }

                if(isEmpty(payload)) {
                    return resolve();
                }

                await this.companyUserRepository.update({
                    id: userId,
                }, payload);

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
