import { Controller, UseGuards, Get, HttpStatus, HttpException, Req, Post, Body, Delete, Param, Put } from '@nestjs/common';
import { Request } from 'express';
import { CompanyUserService } from './company-user.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { CreateCompanyUserDto, UpdateCompanyUserDto } from './../../../dtos';

@ApiUseTags('Company')
@Controller('company/user')
export class CompanyUserController {
    constructor(
        private readonly companyUserService: CompanyUserService,
        private readonly i18n: I18nService
    ) {}

    @ApiOperation({
        title: 'Method to delete a company user'
    })
    @ApiBearerAuth()
    @ApiUseTags('CompanyUser')
    @UseGuards(AuthGuard('jwt:company'))
    @Delete(':userId')
    async deleteUser(
        @Req() req: Request,
        @Param() userId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];

            await this.companyUserService.deleteUser(userId, companyId);
            
            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Method to get company user lit'
    })
    @ApiBearerAuth()
    @ApiUseTags('CompanyUser')
    @UseGuards(AuthGuard('jwt:company'))
    @Get()
    async list(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            const users = await this.companyUserService.getUsers();
            
            return {
                error: false,
                data: users
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Method to create a new company user'
    })
    @ApiBearerAuth()
    @ApiUseTags('CompanyUser')
    @UseGuards(AuthGuard('jwt:company'))
    @Post()
    async createUser(
        @Req() req: Request,
        @Body() createCompanyUserDto: CreateCompanyUserDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];

            const id = await this.companyUserService.createUser(companyId, createCompanyUserDto);
            
            return {
                error: false,
                data: {
                    id
                }
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Method to create a new company user'
    })
    @ApiBearerAuth()
    @ApiUseTags('CompanyUser')
    @UseGuards(AuthGuard('jwt:company'))
    @Put(':userId')
    async updateUser(
        @Req() req: Request,
        @Param() userId: number,
        @Body() updateCompanyUserDto: UpdateCompanyUserDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.companyUserService.updateUser(userId, updateCompanyUserDto);
            
            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
