import { Module } from '@nestjs/common';
import { CompanyUserService } from './company-user.service';
import { companyUserProviders } from '../../../core/providers/company-user.providers';
import { DatabaseModule } from '../../../core/database/database.module';
import { CompanyUserController } from './company-user.controller';

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [CompanyUserController],
  providers: [
    ...companyUserProviders,
    CompanyUserService
  ],
  exports: [
    CompanyUserService
  ]
})
export class CompanyUserModule {}
