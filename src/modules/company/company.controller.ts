import { Controller, Post, Body, Req, HttpStatus, HttpException } from '@nestjs/common';
import { ApiOperation, ApiCreatedResponse, ApiBadRequestResponse, ApiUseTags } from '@nestjs/swagger';
import { CreateCompanyAccountDto } from '../../dtos';
import { Request } from 'express';
import { CompanyService } from './company.service';
import { LogService } from '../../core/log/log.service';
import { MailerService } from '@nest-modules/mailer';
import { I18nService } from 'nestjs-i18n';
import * as moment from 'moment';
import { CreateCompanyResponse } from '../../interfaces/responses';

@ApiUseTags('Company')
@Controller('company')
export class CompanyController {

    constructor(
        private companyService: CompanyService,
        private readonly logService: LogService,
        private readonly mailerService: MailerService,
        private readonly i18n: I18nService
    ) {}

    @ApiOperation({
        title: 'Create Company',
        description: `
            Method to create a new company.
            It will create a company account and the first administrator as owner with all permissions
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully created company's account and send a welcome email to his email.`
    })
    @ApiBadRequestResponse({
        description: `This means that either the CNPJ was invalid or taken.`
    })
    @Post()
    async createCompany(
        @Body() createCompanyAccountDto: CreateCompanyAccountDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let response: CreateCompanyResponse =  await this.companyService.createCompany(createCompanyAccountDto);

            await this.logService.createCompanyUserLog({
                companyUserId: response.company_user_id,
                userAgent: req.headers['user-agent'],
                ip: req.ip,
                title: 'log.COMPANY_ACCOUNT_CREATED',
                description: 'log.COMPANY_ACCOUNT_CREATED_DESCRIPTION',
                createdAt: moment().toDate(),
                actionType: 'log.ACTION_TYPE.SIGN_UP'
            });

            await this.mailerService.sendMail({
                to: createCompanyAccountDto.admin_email,
                from: 'noreply@mybrief.com.br',
                subject: this.i18n.translate('auth.AWAIT_COMPANY_VALIDATION', {lang: language}),
                html: this.i18n.translate('auth.AWAIT_COMPANY_VALIDATION_DESCRIPTION', {
                    lang: language,
                    args: {
                        companyName: createCompanyAccountDto.name,
                        adminName: createCompanyAccountDto.admin_name + ' ' + createCompanyAccountDto.admin_last_name,
                    }
                })
            });

            return {
                error: false,
                message: this.i18n.translate('auth.COMPANY_SUCCESSFULLY_REGISTERED', {
                    lang: language
                }),
                data: {
                    id: response.company_id
                }
            };

        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
