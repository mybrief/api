import { Module } from '@nestjs/common';
import { CompanyService } from './company.service';
import { CompanyController } from './company.controller';
import { DatabaseModule } from '../../core/database/database.module';
import { LogModule } from '../../core/log/log.module';
import { companyProviders, jobProviders, productProviders, packageProviders, planProviders } from '../../core/providers';
import { CompanyJobController } from './company-job/company-job.controller';
import { CompanyJobService } from './company-job/company-job.service';
import { CompanyRequestController } from './company-request/company-request.controller';
import { CompanyRequestService } from './company-request/company-request.service';
import { CompanyDemandController } from './company-demand/company-demand.controller';
import { CompanyDemandService } from './company-demand/company-demand.service';
import { CompanyInvoiceController } from './company-invoice/company-invoice.controller';
import { CompanyInvoiceService } from './company-invoice/company-invoice.service';
import { CompanyProductController } from './company-product/company-product.controller';
import { CompanyPackageController } from './company-package/company-package.controller';
import { CompanyPlanController } from './company-plan/company-plan.controller';
import { CompanyProductService } from './company-product/company-product.service';
import { CompanyPackageService } from './company-package/company-package.service';
import { CompanyPlanService } from './company-plan/company-plan.service';

@Module({
  imports: [
    DatabaseModule,
    LogModule,
  ],
  providers: [
    ...companyProviders,
    ...productProviders,
    ...packageProviders,
    ...planProviders,
    ...jobProviders,
    CompanyService,
    CompanyJobService,
    CompanyRequestService,
    CompanyDemandService,
    CompanyInvoiceService,
    CompanyProductService,
    CompanyPackageService,
    CompanyPlanService
  ],
  controllers: [CompanyController, CompanyJobController, CompanyRequestController, CompanyDemandController, CompanyInvoiceController, CompanyProductController, CompanyPackageController, CompanyPlanController]
})
export class CompanyModule {}
