import { Injectable, Inject } from '@nestjs/common';
import { Repository, Like } from 'typeorm';
import { Plan } from './../../../entities';
import { FilterCompanyPlansDto } from './../../../dtos';

import { isStringEmpty } from '../../../core/utils';

@Injectable()
export class CompanyPlanService {
    constructor(
        @Inject('PLAN_REPOSITORY')
        private readonly planRepository: Repository<Plan>
    ) {}

    async getPlans(filters: FilterCompanyPlansDto): Promise<Plan[]> {
        return new Promise<Plan[]>(async (resolve, reject) => {
            try {
                const { name } = filters;

                const optionalWhere = {};

                if(!isStringEmpty(name)) {
                    optionalWhere['name'] = Like(`%${name}%`);
                }

                const plans = await this.planRepository.find({
                    where: {
                      ...optionalWhere,
                      deleted_at: null,
                    },
                    order: {
                        created_at: 'DESC'
                    },
                    select: ['id', 'name', 'description', 'image', 'price', 'allowed_payment_type'],
                    relations: ['plan_product', 'plan_product.package', 'plan_product.product']
                });

                plans.forEach(plan => {
                    plan.plan_product.forEach((planProduct) => {
                        if(planProduct.item_type === 'package') {
                            delete planProduct.product;
                        } else {
                            delete planProduct.package;
                        }
                    });
                });

                resolve(plans);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
