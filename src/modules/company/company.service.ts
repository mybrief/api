import { Injectable, Inject } from '@nestjs/common';
import { Repository, InsertResult } from 'typeorm';
import { Company, CompanyUser } from '../../entities';
import { CreateCompanyAccountDto } from '../../dtos';
import { capitalLetter, isValidCNPJ, onlyNumbers, replaceAccents } from '../../core/utils';
import { CreateCompanyResponse } from '../../interfaces/responses';

const bcrypt = require('bcrypt');

@Injectable()
export class CompanyService {
    constructor(
        @Inject('COMPANY_REPOSITORY')
        private readonly companyRepository: Repository<Company>,
        @Inject('COMPANY_USER_REPOSITORY')
        private readonly companyUserRepository: Repository<CompanyUser>
    ) {}

    async createCompany(createCompanyAccountDto: CreateCompanyAccountDto): Promise<CreateCompanyResponse> {
        return new Promise<CreateCompanyResponse>(async (resolve, reject) => {
            try {
                if(!isValidCNPJ(createCompanyAccountDto.cnpj)) {
                    return reject({
                        message: 'auth.ERROR.COMPANY.INVALID_CNPJ',
                        catch: false,
                        data: {
                            args: {cnpj: createCompanyAccountDto.cnpj}
                        }
                    });
                }

                createCompanyAccountDto.cnpj = onlyNumbers(createCompanyAccountDto.cnpj);

                if(!await this.isCnpjAvailable(createCompanyAccountDto.cnpj)) {
                    return reject({
                        message: 'auth.ERROR.COMPANY.CNPJ_ALREADY_TAKEN',
                        catch: false,
                        data: {
                            args: {cnpj: createCompanyAccountDto.cnpj}
                        }
                    });
                }

                if(!await this.isAdminEmailvailable(createCompanyAccountDto.admin_email)) {
                    return reject({
                        message: 'auth.ERROR.COMPANY.COMPANY_ADMIN_EMAIL_ALREADY_TAKEN',
                        catch: false,
                        data: {
                            args: {email: createCompanyAccountDto.admin_email}
                        }
                    });
                }

                let slug: string = replaceAccents(createCompanyAccountDto.name.trim().toLowerCase().split(' ').join(''));

                let i: number = 1;
                while(!await this.isSlugAvailable(slug)) {
                    slug += i;

                    i++;
                    continue;
                }

                let companyPayload = {
                    name: createCompanyAccountDto.name,
                    cellphone: createCompanyAccountDto.cellphone,
                    telephone: createCompanyAccountDto.telephone,
                    segment: createCompanyAccountDto.segment,
                    site: createCompanyAccountDto.site,
                    localization: createCompanyAccountDto.localization,
                    cnpj: createCompanyAccountDto.cnpj,
                    slug: slug,
                    valid: 0,
                    updated_at: null
                }

                let insertResult: InsertResult = await this.companyRepository.insert(companyPayload);

                let companyId: number = insertResult.raw.insertId;

                let userInsertResult: InsertResult = await this.companyUserRepository.insert({
                    name: capitalLetter(createCompanyAccountDto.admin_name),
                    last_name: capitalLetter(createCompanyAccountDto.admin_last_name),
                    owner: 1,
                    company_id: companyId,
                    password: await bcrypt.hash(createCompanyAccountDto.admin_password, 10),
                    permission: 'purchase,request_job,approve_delivery,reprove_delivery',
                    email: createCompanyAccountDto.admin_email,
                    updated_at: null
                });

                let userId: number = userInsertResult.raw.insertId;

                resolve({
                    company_id: companyId,
                    company_user_id: userId
                });
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async companyExists(id: number): Promise<boolean> {
        return await this.companyRepository.count({id: id}) == 1
    }


    async isCnpjAvailable(cnpj: string): Promise<boolean> {
        return await this.companyRepository.count({cnpj: cnpj}) == 0
    }

    async isSlugAvailable(slug: string): Promise<boolean> {
        return await this.companyRepository.count({slug: slug}) == 0
    }

    async isAdminEmailvailable(email: string): Promise<boolean> {
        return await this.companyUserRepository.count({email: email}) == 0
    }
}
