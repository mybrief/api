import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';

import { isEmpty } from '../../../core/utils';

import {
    Job,
    JobFeedback,
    CompanyJobFeedback,
    CompanyJobFeedbackRequest,
} from '../../../entities';
import { UpdateCompanyJobStatusDto } from '../../../dtos';

import { CompanyService } from '../company.service';

@Injectable()
export class CompanyJobService {

    constructor(
        @Inject('JOB_REPOSITORY')
        private readonly jobRepository: Repository<Job>,
        @Inject('COMPANY_JOB_FEEDBACK_REPOSITORY')
        private readonly companyJobFeedbackRepository: Repository<CompanyJobFeedback>,
        @Inject('COMPANY_JOB_FEEDBACK_REQUEST_REPOSITORY')
        private readonly companyJobFeedbackRequestRepository: Repository<CompanyJobFeedbackRequest>,

        private readonly companyService: CompanyService
    ) {}

    /**
     * ### Method to list all company jobs
     *
     * Will return all jobs from company with the product name and category
     * @param companyId id of the company to get the jobs
     */
    async getCompanyJobs(companyId: number): Promise<Job[]> {
        return new Promise<Job[]>(async (resolve, reject) => {
            try {
                let job = await this.jobRepository.createQueryBuilder('job')
                                    // .innerJoin('job.product', 'product', 'product.category = "social_media"')
                                    .where({company_id: companyId})
                                    .innerJoin('job.product', 'product')
                                    .select(['job.id', 'job.status', 'product.name', 'product.category'])
                                    .getMany();

                resolve(job);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Method to list all company jobs filtered by category
     *
     * Will return all jobs from company with the product name and category filtering by category
     * @param companyId id of the company to get the jobs
     * @param category category name to filter the jobs
     */
    async getCompanyJobsByCategory(companyId: number, category: "social_media"|"strategy"|"development"): Promise<Job[]> {
        return new Promise<Job[]>(async (resolve, reject) => {
            try {
                const jobApprovedOrWaitingQuery = "job.status not in ('to_do', 'doing', 'to_change')";

                const jobs = await this.jobRepository.createQueryBuilder('job')
                                    .where({company_id: companyId})
                                    .innerJoinAndSelect('job.company_product', 'company_product')
                                    .innerJoinAndSelect('company_product.product', 'product', `product.category = "${category}"`)
                                    .leftJoinAndSelect('job.attachment', 'attachment', jobApprovedOrWaitingQuery)
                                    .leftJoinAndSelect('job.company_feedback_request', 'company_feedback_request', jobApprovedOrWaitingQuery)
                                    .leftJoinAndSelect('company_feedback_request.feedback', 'feedback')
                                    .getMany();

                // Loop to show actual job status from company view.
                // To company is only "to change" if there are already a disapproved feedback
                // And is only "to approval" if there are a request but not a feedback
                // And is only "done" if there are already a approved feedback
                // It's changed on loop to not change the DB structure
                jobs.forEach(job => {
                    if(!isEmpty(job.company_feedback_request)) {
                        const approved = job.company_feedback_request.find(request => !isEmpty(request.feedback) && request.feedback.status === 'approval');
                        const notApproved = job.company_feedback_request.find(request => !isEmpty(request.feedback) && request.feedback.status === 'disapproval');
                        const waitToApproval = job.company_feedback_request.find(request => isEmpty(request.feedback));

                        if(approved) {
                            job.status = 'done';
                            job['company_feedback_request'] = [approved];
                        } else if (notApproved && !waitToApproval) {
                            job.status = 'to_change';
                            job['company_feedback_request'] = [notApproved];
                        } else {
                            job.status = 'to_approval';
                            job['company_feedback_request'] = [waitToApproval]
                        }
                    } else if (['to_approval', 'to_change', 'done'].includes(job.status)) {
                        job.status = 'doing';
                    }
                });

                resolve(jobs);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Method to change company job status
     *
     * Change company job status and creates the feedback
     * @param companyId company id that owns the job
     * @param updateCompanyJobStatusDto informations to create the feedback
     */
    async feedbackJob(companyUserId: number, updateCompanyJobStatusDto: UpdateCompanyJobStatusDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                if(updateCompanyJobStatusDto.status != 'approval' && updateCompanyJobStatusDto.status != 'disapproval') {
                    return reject({
                        message: 'company.JOB.ERROR.INVALID_STATUS_NAME',
                        catch: false,
                        data: null
                    });
                }

                let feedback = {
                    feedback: updateCompanyJobStatusDto.feedback,
                    job_id: updateCompanyJobStatusDto.job_id,
                    company_job_feedback_request_id: updateCompanyJobStatusDto.company_job_feedback_request_id,
                    status: updateCompanyJobStatusDto.status,
                    company_user_id: companyUserId
                };

                await this.companyJobFeedbackRepository.insert(feedback);

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async getCompanyJobFeedbackRequests(companyId: number): Promise<CompanyJobFeedbackRequest[]> {
        return new Promise<CompanyJobFeedbackRequest[]>(async (resolve, reject) => {
            try {
                const requests = await this.companyJobFeedbackRequestRepository
                                            .createQueryBuilder('company_job_request_feedback')
                                            .where({
                                                company_id: companyId
                                            })
                                            .where('NOT EXISTS (SELECT * FROM company_job_feedback as cjf where cjf.job_id = company_job_request_feedback.job_id)')
                                            .getMany();
                resolve(requests);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    async companyJobExists(companyId: number, jobId: number): Promise<boolean> {
        return await this.jobRepository.count({id: jobId, company_id: companyId}) === 1
    }
}
