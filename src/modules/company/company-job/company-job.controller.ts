import { Controller, Post, Body, Req, HttpStatus, HttpException, Get, UseGuards, Param, Put } from '@nestjs/common';
import { ApiOperation, ApiCreatedResponse, ApiBadRequestResponse, ApiUseTags, ApiBearerAuth, ApiOkResponse } from '@nestjs/swagger';
import { CreateCompanyAccountDto, UpdateCompanyJobStatusDto } from '../../../dtos';
import { Request } from 'express';

import { AuthGuard } from '@nestjs/passport';

import { LogService } from '../../../core/log/log.service';
import { MailerService } from '@nest-modules/mailer';
import { I18nService } from 'nestjs-i18n';

import { CompanyJobService } from './company-job.service';

@ApiUseTags('Company')
@Controller('company/job')
export class CompanyJobController {

    constructor(
        private companyJobService: CompanyJobService,
        private readonly logService: LogService,
        private readonly mailerService: MailerService,
        private readonly i18n: I18nService
    ) {}

    @ApiOperation({
        title: 'List Company Jobs feedback requests',
        description: `
            Method to list company job list feedback requests.
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Job')
    @UseGuards(AuthGuard('jwt:company'))
    @Get('feedback/request')
    async listCompanyJobFeedbackRequests(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];

            return {
                error: false,
                data: await this.companyJobService.getCompanyJobFeedbackRequests(companyId)
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'List Company Jobs',
        description: `
            Method to list company job list.
        `
    })
    @ApiOkResponse({
        description: `The system successfully validated company authentication and return the job list`
    })
    @ApiBearerAuth()
    @ApiUseTags('Job')
    @UseGuards(AuthGuard('jwt:company'))
    @Get()
    async listCompanyJobs(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];
            let userId: number = req.user['user_id'];

            return {
                error: false,
                message: this.i18n.translate('company.JOB.COMPANY_JOB_LIST', {
                    lang: language
                }),
                data: await this.companyJobService.getCompanyJobs(companyId)
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'List Company Jobs filtered by category',
        description: `
            Method to list company job list filtered by category.
        `
    })
    @ApiOkResponse({
        description: `The system successfully validated company authentication and return the job list filtered by category`
    })
    @ApiBearerAuth()
    @ApiUseTags('Job')
    @UseGuards(AuthGuard('jwt:company'))
    @Get(':category')
    async listCompanyJobsByCategory(
        @Req() req: Request,
        @Param('category') category: "strategy"|"social_media"|"development"
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];
            let userId: number = req.user['user_id'];
            let categories = ["strategy","social_media","development"];

            if(!categories.includes(category)) {
                category = "social_media";
            }

            return {
                error: false,
                message: this.i18n.translate('company.JOB.COMPANY_JOB_LIST', {
                    lang: language
                }),
                data: await this.companyJobService.getCompanyJobsByCategory(companyId, category)
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Change Company Job Status and send a feedback',
        description: `
            Method to change company job status to set as approved or disapproved and send a feedback.
        `
    })
    @ApiOkResponse({
        description: `The system successfully changed the job status`
    })
    @ApiBadRequestResponse({
        description: `This means that the company job was not found or status name was invalid`
    })
    @ApiBearerAuth()
    @ApiUseTags('Job')
    @UseGuards(AuthGuard('jwt:company'))
    @Post('feedback')
    async changeJobStatusAndFeedback(
        @Req() req: Request,
        @Body() updateCompanyJobStatusDto: UpdateCompanyJobStatusDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let userId: number = req.user['user_id'];
            let companyId: number = req.user['company_id'];
            await this.companyJobService.feedbackJob(userId, updateCompanyJobStatusDto);
            return {
                error: false,
                message: this.i18n.translate('company.JOB.STATUS_CHANGED', {
                    lang: language
                }),
                data: {
                    userId
                }
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
