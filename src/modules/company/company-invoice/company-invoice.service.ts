import { Repository } from 'typeorm';
import { Injectable, Inject } from '@nestjs/common';

import { PagarmeService } from './../../../pagarme/pagarme.service';

import {
  CompanyInvoice,
  CompanyInvoiceItem
} from '../../../entities';
import { CreateCompanyInvoiceDto } from '../../../dtos';

@Injectable()
export class CompanyInvoiceService {
    constructor(
        @Inject('COMPANY_INVOICE_REPOSITORY')
        private readonly companyInvoiceRepository: Repository<CompanyInvoice>,
        @Inject('COMPANY_INVOICE_ITEM_REPOSITORY')
        private readonly companyInvoiceItemRepository: Repository<CompanyInvoiceItem>,
        private readonly pagarmeService: PagarmeService
    ) { }

    async createCompanyInvoice(companyId: number, createCompanyInvoiceDto: CreateCompanyInvoiceDto): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                const { price, pagarme_info, invoice_item } = createCompanyInvoiceDto;

                const insertResult = await this.companyInvoiceRepository.insert({ 
                    company_id: companyId,
                    price,
                    discount: 0,
                    payment_status: 'processing'
                });

                let invoiceId: number = insertResult.raw.insertId;

                invoice_item.map(item => item.company_invoice_id = invoiceId);

                await this.companyInvoiceItemRepository.insert(invoice_item);

                // TODO: Create correct params variables and fix pagarme service
                // await this.pagarmeService.createTransaction(companyId, pagarme_info);

                resolve(invoiceId);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
