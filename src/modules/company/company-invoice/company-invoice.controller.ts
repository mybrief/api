import { AuthGuard } from '@nestjs/passport';
import { I18nService } from 'nestjs-i18n';
import { Request } from 'express';
import { ApiUseTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { Controller, UseGuards, Post, Req, HttpStatus, HttpException, Body } from '@nestjs/common';
import { CompanyInvoiceService } from './company-invoice.service';
import { CreateCompanyInvoiceDto } from './../../../dtos';

@ApiUseTags('Company')
@Controller('company/invoice')
export class CompanyInvoiceController {
    constructor(
        private readonly companyInvoiceService: CompanyInvoiceService,
        private readonly i18n: I18nService
    ) { }

    @ApiOperation({
        title: 'Method to create a new invoice',
        description: `
            Creates a new invoice connecting to mybrief payment service
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Invoice')
    @UseGuards(AuthGuard('jwt:company'))
    @Post()
    async createInvoice(
        @Req() req: Request,
        @Body() createCompanyInvoiceDto: CreateCompanyInvoiceDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];
            const status = await this.companyInvoiceService.createCompanyInvoice(companyId, createCompanyInvoiceDto);
            return {
                error: false,
                data: {
                    status
                }
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
