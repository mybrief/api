import { Controller, Req, HttpStatus, HttpException, Get, UseGuards, Post, Param } from '@nestjs/common';
import { ApiOperation, ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { Request } from 'express';

import { AuthGuard } from '@nestjs/passport';

import { I18nService } from 'nestjs-i18n';

import { CompanyRequestService } from './company-request.service';

@ApiUseTags('Company')
@Controller('company/request')
export class CompanyRequestController {

    constructor(
        private companyRequestService: CompanyRequestService,
        private readonly i18n: I18nService
    ) {}

    @ApiOperation({
        title: 'List products by category',
        description: `
            Method to list products by category name
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Request', 'Product')
    @UseGuards(AuthGuard('jwt:company'))
    @Post('product/:category/list')
    async getProductsByCategory(
        @Req() req: Request,
        @Param('category') category: 'development'|'strategy'|'social_media'
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                data: await this.companyRequestService.getProductsByCategory(category)
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'List packages',
        description: `
            Method to list packages
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Request', 'Package')
    @UseGuards(AuthGuard('jwt:company'))
    @Post('package/list')
    async getPackages(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                data: await this.companyRequestService.getPackages()
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'List plans',
        description: `
            Method to list plans
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Request', 'Plan')
    @UseGuards(AuthGuard('jwt:company'))
    @Post('plan/list')
    async getPlans(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                data: await this.companyRequestService.getPlans()
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'List Company requests informations',
        description: `
            Method to company requests informations (total and percentage)
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Job')
    @UseGuards(AuthGuard('jwt:company'))
    @Get('job/percentage')
    async listCompanyRequestsPercentages(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];

            return {
                error: false,
                data: await this.companyRequestService.getRequestsPercentage(companyId)
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'List Company requests consumption',
        description: `
            Method to company requests consumption (by product category)
        `
    })
    @ApiBearerAuth()
    @ApiUseTags('Job')
    @UseGuards(AuthGuard('jwt:company'))
    @Get('job/consumption')
    async listCompanyRequestsConsumption(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let companyId: number = req.user['company_id'];

            return {
                error: false,
                data: await this.companyRequestService.getAvailableCoinsAndConsumptionData(companyId)
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
