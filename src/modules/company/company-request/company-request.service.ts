import { Injectable, Inject } from '@nestjs/common';
import { Repository, In, MoreThan } from 'typeorm';
import { Job, CompanyInvoice, Product, Plan, Package } from '../../../entities';
import { GetCompanyRequestsPercentage, GetCompanyAvailableCoinsAndConsumptionData } from '../../../interfaces/responses';

@Injectable()
export class CompanyRequestService {

    constructor(
        @Inject('JOB_REPOSITORY')
        private readonly jobRepository: Repository<Job>,
        @Inject('PRODUCT_REPOSITORY')
        private readonly productRepository: Repository<Product>,
        @Inject('PACKAGE_REPOSITORY')
        private readonly packageRepository: Repository<Package>,
        @Inject('PLAN_REPOSITORY')
        private readonly planRepository: Repository<Plan>,
        @Inject('COMPANY_INVOICE_REPOSITORY')
        private readonly companyInvoiceRepository: Repository<CompanyInvoice>
    ) {}

    async getProductsByCategory(category: 'development'|'strategy'|'social_media'): Promise<Product[]> {
        return new Promise<Product[]>(async (resolve, reject) => {
            try {
                const products = await this.productRepository.find({
                    where: {
                        category: category,
                        deleted_at: null
                    },
                    order: {
                        created_at: 'DESC'
                    }
                });

                resolve(products);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    async getPackages(): Promise<Package[]> {
        return new Promise<Package[]>(async (resolve, reject) => {
            try {
                const packages = await this.packageRepository.find({
                    where: {
                        deleted_at: null
                    },
                    order: {
                        created_at: 'DESC'
                    },
                    relations: ['package_product', 'package_product.product']
                });

                resolve(packages);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    async getPlans(): Promise<Plan[]> {
        return new Promise<Plan[]>(async (resolve, reject) => {
            try {
                const plans = await this.planRepository.find({
                    where: {
                        deleted_at: null
                    },
                    order: {
                        created_at: 'DESC'
                    },
                    relations: ['plan_product', 'plan_product.product']
                });

                resolve(plans);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    /**
     * ### Method to return company requests percentages
     *
     * Return company request percentage based on job status
     * @param companyId id of the company
     */
    async getRequestsPercentage(companyId: number): Promise<GetCompanyRequestsPercentage> {
        return new Promise<GetCompanyRequestsPercentage>(async (resolve, reject) => {
            try {
                let totalJobCount: number = await this.jobRepository.count({ company_id: companyId, deleted_at: null });

                let doingJobCount: number = await this.jobRepository.count({ company_id: companyId, deleted_at: null, status: 'doing' });

                let doneJobCount: number = await this.jobRepository.count({ company_id: companyId, deleted_at: null, status: 'done' });

                let toApprovalJobCount: number = await this.jobRepository.count({ company_id: companyId, deleted_at: null, status: 'to_approval' });

                let toChangeJobCount: number = await this.jobRepository.count({ company_id: companyId, deleted_at: null, status: 'to_change' });

                let toDoJobCount: number = await this.jobRepository.count({ company_id: companyId, deleted_at: null, status: 'to_do' });

                let doingJobPercentage = (doingJobCount / totalJobCount) * 100;
                let doneJobPercentage = (doneJobCount / totalJobCount) * 100;
                let toApprovalJobPercentage = (toApprovalJobCount / totalJobCount) * 100;
                let toChangeJobPercentage = (toChangeJobCount / totalJobCount) * 100;
                let toDoJobPercentage = (toDoJobCount / totalJobCount) * 100;

                resolve({
                    doing: doingJobCount,
                    done: doneJobCount,
                    to_approval: toApprovalJobCount,
                    to_change: toChangeJobCount,
                    to_do: toDoJobCount,
                    doing_percentage: parseFloat(doingJobPercentage.toFixed(1)),
                    done_percentage: parseFloat(doneJobPercentage.toFixed(1)),
                    to_approval_percentage: parseFloat(toApprovalJobPercentage.toFixed(1)),
                    to_change_percentage: parseFloat(toChangeJobPercentage.toFixed(1)),
                    to_do_percentage: parseFloat(toDoJobPercentage.toFixed(1)),
                    total: totalJobCount
                });
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    /**
     * ### Method to get available coins and all company comsumptions
     *
     * @param companyId id of the company
     */
    async getAvailableCoinsAndConsumptionData(companyId: number): Promise<GetCompanyAvailableCoinsAndConsumptionData> {
        return new Promise<GetCompanyAvailableCoinsAndConsumptionData>(async (resolve, reject) => {
            try {
                let totalNotCoinConsumptionCount = await this.companyInvoiceRepository.createQueryBuilder('company_invoice')
                                                            .where({
                                                                item_type: In(['product', 'package']),
                                                                deleted_at: null,
                                                                payment_status: In(['processing','authorized','paid']),
                                                                quantity: MoreThan(0)
                                                            })
                                                            .innerJoin('company_invoice.invoice_item', 'invoice_item', 'item_type in ("product", "package")')
                                                            .getCount();

                let packageConsumptionCount = await this.companyInvoiceRepository.createQueryBuilder('company_invoice')
                                                        .where({
                                                            item_type: 'package',
                                                            deleted_at: null,
                                                            payment_status: In(['processing','authorized','paid']),
                                                            quantity: MoreThan(0)
                                                        })
                                                        .innerJoin('company_invoice.invoice_item', 'invoice_item', 'item_type = "package"')
                                                        .getCount();

                let socialMediaConsumptionCount = await this.getCompanyConsumptionByCategory(companyId, 'social_media');
                let developmentConsumptionCount = await this.getCompanyConsumptionByCategory(companyId, 'development');
                let strategyConsumptionCount = await this.getCompanyConsumptionByCategory(companyId, 'strategy');

                let developmentPercentage = (developmentConsumptionCount / totalNotCoinConsumptionCount) * 100;
                let socialMediaPercentage = (socialMediaConsumptionCount / totalNotCoinConsumptionCount) * 100;
                let strategyPercentage = (strategyConsumptionCount / totalNotCoinConsumptionCount) * 100;
                let packagePercentage = (packageConsumptionCount / totalNotCoinConsumptionCount) * 100;

                resolve({
                    development: developmentConsumptionCount,
                    package: packageConsumptionCount,
                    development_percentage: parseFloat(developmentPercentage.toFixed(1)),
                    social_media: socialMediaConsumptionCount,
                    social_media_percentage: parseFloat(socialMediaPercentage.toFixed(1)),
                    strategy: strategyConsumptionCount,
                    strategy_percentage: parseFloat(strategyPercentage.toFixed(1)),
                    package_percentage: parseFloat(packagePercentage.toFixed(1)),
                    available_coins: await this.getCompanyAvailableCoins(companyId)
                })
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    /**
     * ### Method to return number of purchases by product category
     *
     * @param companyId id of the company
     * @param category product category
     */
    async getCompanyConsumptionByCategory(companyId: number, category: 'development'|'social_media'|'strategy'): Promise<number>{
        return new Promise<number>(async (resolve, reject) => {
            try {
                const consumption = await this.companyInvoiceRepository.createQueryBuilder('company_invoice')
                                                .where({
                                                    company_id: companyId,
                                                    deleted_at: null,
                                                    payment_status: In(['processing','authorized','paid'])
                                                })
                                                .innerJoin('company_invoice.invoice_item', 'invoice_item', 'invoice_item.item_type = "product"')
                                                .innerJoin('invoice_item.product', 'product', 'product.category = "'+category+'"')
                                                .getCount();
                resolve(consumption);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }

    /**
     * ### Method to return number of available coins
     *
     * @param companyId company id
     */
    async getCompanyAvailableCoins(companyId: number): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                let consumption = await this.companyInvoiceRepository.createQueryBuilder('company_invoice')
                                            .where({
                                                company_id: companyId,
                                                deleted_at: null,
                                                payment_status: In(['authorized','paid']),
                                            })
                                            .innerJoinAndSelect('company_invoice.invoice_item', 'invoice_item', 'invoice_item.item_type = "coin"')
                                            .getMany();

                let amount: number = 0;

                for(let invoice of consumption) {
                    for(let item of invoice.invoice_item) {
                        amount += item.quantity;
                    }
                }

                resolve(amount)
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        });
    }
}
