import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { FreelancerService } from '../freelancer/freelancer.service';
import { JwtService } from '@nestjs/jwt';
import { CompanyUserService } from '../company/company-user/company-user.service';
import { Freelancer, Admin, CompanyUser } from '../../entities';
import { AdminService } from '../admin/admin.service';

const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
    constructor(
        private readonly freelancerService: FreelancerService,
        private readonly companyUserService: CompanyUserService,
        private readonly adminService: AdminService,
        private readonly jwtService: JwtService
    ) {}

    async validateFreelancer(username: string, password: string): Promise<any> {
        const freelancer = await this.freelancerService.findOne(username);

        if (freelancer && await bcrypt.compare(password, freelancer.password)) {
            const { password, ...result } = freelancer;
            return result;
        }
        return null;
    }

    async validateCompanyUser(username: string, password: string): Promise<any> {
        const companyUser = await this.companyUserService.findOne(username);

        if (companyUser && await bcrypt.compare(password, companyUser.password)) {
            const { password, ...result } = companyUser;
            return result;
        }
        return null;
    }

    async validateAdmin(username: string, password: string): Promise<any> {
        const admin = await this.adminService.findOne(username);

        if (admin && await bcrypt.compare(password, admin.password)) {
            const { password, ...result } = admin;
            return result;
        }
        return null;
    }

    async loginCompany(companyUser: any) {
        return new Promise<any>(async (resolve, reject) => {
            try {
                const payload = {
                    username: companyUser.email,
                    company_id: companyUser.company_id,
                    sub: companyUser.id
                };

                let checkCompany: CompanyUser = await this.companyUserService.findOne(companyUser.email);

                if(checkCompany == null) {
                    return reject({
                        catch: false,
                        message: 'auth.ERROR.USER_NOT_FOUND',
                        data: {
                            args: {username: companyUser.email}
                        }
                    })
                }

                resolve({
                    access_token: this.jwtService.sign(payload),
                    company_user: checkCompany
                });
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async loginAdmin(admin: any) {
        return new Promise<any>(async (resolve, reject) => {
            try {
                const payload = {
                    username: admin.username,
                    sub: admin.id
                };

                let checkAdmin: Admin = await this.adminService.findById(admin.id);

                if(checkAdmin == null) {
                    return reject({
                        catch: false,
                        message: 'auth.ERROR.USER_NOT_FOUND',
                        data: {
                            args: {username: admin.username}
                        }
                    })
                }

                resolve({
                    access_token: this.jwtService.sign(payload),
                    admin: checkAdmin
                });
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async login(freelancer: any): Promise<{ access_token: string, freelancer: Freelancer}> {
        return new Promise<any>(async (resolve, reject) => {
            try {
                const payload = {
                    username: freelancer.username,
                    sub: freelancer.id
                };

                let checkFreelancer: Freelancer = await this.freelancerService.findById(freelancer.id, ["profile"]);

                if(checkFreelancer == null) {
                    return reject({
                        catch: false,
                        message: 'auth.ERROR.USER_NOT_FOUND',
                        data: {
                            args: {username: freelancer.username}
                        }
                    })
                }

                if(checkFreelancer.valid == 0) {
                    return reject({
                        catch: false,
                        message: 'auth.ERROR.USER_NOT_CONFIRMED',
                        data: {
                            args: {username: freelancer.username}
                        }
                    })
                }

                resolve({
                    access_token: this.jwtService.sign(payload),
                    freelancer: checkFreelancer
                });
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
