import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { FreelancerModule } from '../freelancer/freelancer.module';

import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from '../../core/strategies/local-freelancer.strategy';

import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from '../../core/strategies/jwt-freelancer.strategy';
import { AuthController } from './auth.controller';
import { CompanyUserModule } from '../company/company-user/company-user.module';
import { JwtStrategyCompany } from '../../core/strategies/jwt-company.strategy';
import { LocalStrategyCompany } from '../../core/strategies/local-company.strategy';
import { LocalStrategyAdmin } from '../../core/strategies/local-admin.strategy';
import { AdminModule } from '../admin/admin.module';
import { JwtStrategyAdmin } from '../../core/strategies/jwt-admin.strategy';

@Module({
  imports: [
    FreelancerModule,
    CompanyUserModule,
    AdminModule,
    PassportModule.register({ defaultStrategy: 'jwt:freelancer' }),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '10800s' },
    }),
  ],
  providers: [
    AuthService,
    LocalStrategy,
    LocalStrategyAdmin,
    LocalStrategyCompany,
    JwtStrategy,
    JwtStrategyCompany,
    JwtStrategyAdmin
  ],
  exports: [
    AuthService
  ],
  controllers: [AuthController]
})
export class AuthModule {}
