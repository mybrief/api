import { ConfigService } from './../../core/config/config.service';
import { Controller, Post, UseGuards, Request, Body, HttpException, HttpStatus } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { ApiUseTags } from '@nestjs/swagger';
import { LoginDto } from '../../dtos';
import { I18nService } from 'nestjs-i18n';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly i18n: I18nService,
        private readonly configService: ConfigService
      ) {}

    @ApiUseTags('Auth', 'Freelancer')
    @UseGuards(AuthGuard('local:freelancer'))
    @Post('login/freelancer')
    async login(@Request() req, @Body() _loginDto: LoginDto) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {

            const { freelancer, access_token } = await this.authService.login(req.user);

            if(freelancer.profile && freelancer.profile.avatar) {
                if(!this.configService.isProduction) {
                    freelancer.profile.avatar = req.protocol + '://' + req.get('host') + freelancer.profile.avatar;
                } else {
                    freelancer.profile.avatar = this.configService.baseApiUrl + freelancer.profile.avatar;
                }
            }

            return {
                error: false,
                message: this.i18n.translate('auth.USER_SUCCESSFULLY_AUTHENTICATED', {
                    lang: language
                }),
                data: {
                    freelancer,
                    access_token
                }
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiUseTags('Auth', 'Admin')
    @UseGuards(AuthGuard('local:admin'))
    @Post('login/admin')
    async loginAdmin(@Request() req, @Body() _loginDto: LoginDto) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            return {
                error: false,
                message: this.i18n.translate('auth.ADMIN_SUCCESSFULLY_AUTHENTICATED', {
                    lang: language
                }),
                data: await this.authService.loginAdmin(req.user)
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiUseTags('Auth', 'Company')
    @UseGuards(AuthGuard('local:company'))
    @Post('login/company')
    async loginCompany(@Request() req, @Body() _loginDto: LoginDto) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                message: this.i18n.translate('auth.USER_SUCCESSFULLY_AUTHENTICATED', {
                    lang: language
                }),
                data: await this.authService.loginCompany(req.user)
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
