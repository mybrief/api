import { Module } from '@nestjs/common';
import { LogModule } from '../../core/log/log.module';
import { DatabaseModule } from '../../core/database/database.module';
import { AdminService } from './admin.service';

import { adminProviders } from '../../core/providers';

import { AdminCompanyModule } from './controller/company/company.module';
import { AdminPackageModule } from './controller/package/package.module';
import { AdminPlanModule } from './controller/plan/plan.module';
import { AdminProductModule } from './controller/product/product.module';
import { AdminJobModule } from './controller/job/job.module';
import { AdminFreelancerModule } from './controller/freelancer/freelancer.module';
import { AdminController } from './admin.controller';

@Module({
  imports: [
    DatabaseModule,
    LogModule,
    AdminCompanyModule,
    AdminPackageModule,
    AdminPlanModule,
    AdminProductModule,
    AdminJobModule,
    AdminFreelancerModule
  ],
  controllers: [AdminController],
  providers: [
    ...adminProviders,
    AdminService,
  ],
  exports: [
    AdminService
  ]
})
export class AdminModule {}
