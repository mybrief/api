import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Admin } from '../../entities';

@Injectable()
export class AdminService {
    constructor(
        @Inject('ADMIN_REPOSITORY')
        private readonly adminRepository: Repository<Admin>,
    ) {}

    async findOne(username: string): Promise<Admin> {
        return await this.adminRepository.findOne({
            email: username
        })
    }

    async findById(id: number): Promise<Admin> {
        let admin: Admin = await this.adminRepository.findOne(id);

        if(admin != null) {
            delete admin['password'];
        }

        return admin;
    }

    async getOverview(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {

            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
