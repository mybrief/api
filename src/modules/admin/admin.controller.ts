import { Controller, HttpStatus, HttpException, UseGuards, Post, Req, Body, Get } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { AdminService } from './admin.service';
import { I18nService } from 'nestjs-i18n';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

@ApiUseTags('Admin')
@Controller('admin')
export class AdminController {
    constructor(
        private readonly adminService: AdminService,
        private readonly i18n: I18nService,
    ) {}

    @ApiOperation({
        title: `Get admin overview`,
        description: `
            Method to get admin overview
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Get()
    async getCompanies(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                // data: await this.adminService.getCompanies()
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
