import { Module } from '@nestjs/common';
import { PackageService } from './package.service';
import { PackageController } from './package.controller';
import { DatabaseModule } from '../../../../core/database/database.module';
import { LogModule } from '../../../../core/log/log.module';
import { packageProviders } from '../../../../core/providers';

@Module({
    imports: [
        DatabaseModule,
        LogModule
    ],
    controllers: [
        PackageController
    ],
    providers: [
        ...packageProviders,
        PackageService
    ],
    exports: [
        PackageService
    ]
})
export class AdminPackageModule {}
