import { Injectable, Inject } from '@nestjs/common';
import { Package, PackageProduct } from '../../../../entities';
import { Repository, InsertResult, Like } from 'typeorm';
import { CreatePackageDto, CreatePackageProductDto, FilterPackagesDto, UpdatePackageDto } from '../../../../dtos';
import { isStringEmpty, isEmpty } from '../../../../core/utils';
import * as moment from 'moment';

@Injectable()
export class PackageService {
    constructor(
        @Inject('PACKAGE_REPOSITORY')
        private readonly packageRepository: Repository<Package>,
        @Inject('PACKAGE_PRODUCT_REPOSITORY')
        private readonly packageProductRepository: Repository<PackageProduct>
    ) {}

    async updateAvatar(packageId: number, image: string) {
        return new Promise<void>(async (resolve, reject) => {
            await this.packageRepository.update({
                id: packageId
            }, { image });

            resolve();
        })
    }

    async deleteAvatar(packageId: number) {
        return new Promise<void>(async (resolve, reject) => {
            await this.packageRepository.update({
                id: packageId
            }, { image: null });

            resolve();
        })
    }

    /**
     * ### Validate package name
     *
     * Method to validate if package name is available to be used
     * @param name Package name to validate if is available
     * @returns `Promise`: `resolve` name is available & `reject` name is not available
     */
    async isNameAvailable(name: string): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                let count: number = await this.packageRepository.count({name: name});

                if(count == 0) {
                    return resolve(true);
                } else {
                    return reject({
                        message: 'package.ERROR.NAME_ALREADY_TAKEN',
                        catch: false,
                        data: {
                            args: {name: name}
                        }
                    })
                }
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Create a package
     *
     * Method to create a new package with the given informations
     * @param createPackageDto Package informations
     * @returns `Promise`: `resolve` package created and returns the `package id` & `reject` error while creating the package
     */
    async createPackage(createPackageDto: CreatePackageDto): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                await this.isNameAvailable(createPackageDto.name);

                let payload = {
                    name: createPackageDto.name,
                    description: createPackageDto.description,
                    price_coin: createPackageDto.price_coin,
                    price: createPackageDto.price
                }

                let insertResult: InsertResult = await this.packageRepository.insert(payload);
                let packageId: number = insertResult.raw.insertId;

                if(createPackageDto.products != null && createPackageDto.products.length) {
                    await this.createPackageProduct(createPackageDto.products, packageId);
                }

                resolve(packageId);
            } catch (err) {
                if(err.catch != null && !err.catch) {
                    return reject(err);
                } else {
                    return reject({
                        catch: true,
                        message: err.message,
                        data: err
                    });
                }
            }
        });
    }

    /**
     * ### Create package products
     *
     * Method to create a package product with the given informations
     * @param createPackageProductDto Package products informations
     * @returns `Promise`: `resolve` products created & `reject` error while creating the package product
     */
    async createPackageProduct(createPackageProductDto: CreatePackageProductDto[], packageId: number = null): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                if(packageId != null) {
                    createPackageProductDto.map((product) => {
                        product.package_id = packageId;
                    });
                }

                await this.packageProductRepository.insert(createPackageProductDto);

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Check if package exists
     *
     * Check if package exists with the given ID
     * @param id id of the package to be checked
     */
    async packageExists(id: number): Promise<boolean> {
        return await this.packageRepository.count({id: id}) == 1
    }

    /**
     * ### Get list of packages
     *
     * Method to get list of packages not soft deleted
     * @param filterPacakagesDto optional params to filter the list
     * @returns Promise with the package array
     */
    async getPackages(filterPacakagesDto: FilterPackagesDto): Promise<Package[]> {
        return new Promise<Package[]>(async (resolve, reject) => {
            try {
                const {name} = filterPacakagesDto;

                let findOpt = {};

                if(!isStringEmpty(name)) {
                    findOpt['name'] = Like(`%${name}%`);
                }

                let packages: Package[] = await this.packageRepository.find({
                    where: {
                        deleted_at: null,
                        ...findOpt,
                    },
                    order: {
                        created_at: 'DESC'
                    },
                    relations: ['package_product', 'package_product.product']
                });

                resolve(packages);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Soft delete a package
     *
     * Method to soft deleted a package updating deleted_at field as current date
     * @param packageId id to be soft deleted
     */
    async softDeletePackage(packageId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.packageRepository.update({
                    id: packageId
                }, {
                    deleted_at: moment().toDate()
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Edit package
     *
     * Method to edit package informations if it's not soft deleted
     * @param packageId id of the package to be edited
     * @param editPackageDto params to be changed
     */
    async editPackage(packageId: number, editPackageDto: UpdatePackageDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                if(!isEmpty(editPackageDto)) {
                    await this.packageRepository.update({
                        id: packageId,
                        deleted_at: null
                    }, editPackageDto);
                }

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
