import { ConfigService } from './../../../../core/config/config.service';
import { diskStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express';
import { Controller, HttpStatus, HttpException, UseGuards, Get, Req, Param, Body, Post, Delete, Put, UseInterceptors, UploadedFile } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiOperation, ApiCreatedResponse, ApiBadRequestResponse } from '@nestjs/swagger';
import { PackageService } from './package.service';
import { I18nService } from 'nestjs-i18n';

import * as moment from 'moment';
import { AuthGuard } from '@nestjs/passport';
import { BaseResponse } from '../../../../interfaces/responses';
import { CreatePackageDto, FilterPackagesDto, UpdatePackageDto } from '../../../../dtos';
import { Request } from 'express';
import { LogService } from '../../../../core/log/log.service';
import { Package } from '../../../../entities';
import * as fs from 'fs';

@ApiUseTags('Admin', 'Package')
@Controller('admin/package')
export class PackageController {
    constructor(
        private packageService: PackageService,
        private readonly i18n: I18nService,
        private readonly logService: LogService,
        private readonly configService: ConfigService
    ) {}

    @ApiOperation({
        title: 'Update package image'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put('image/:packageId')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: (req: Request, file: Express.Multer.File, cb) => {
                let dir = process.cwd() +  '/public/upload/packages';

                if (!fs.existsSync(dir)) {
                    fs.mkdir(dir, { recursive: true }, err => cb(err, dir));
                } else {
                    cb(null, dir);
                }
            },
            filename: (req: Request, file: Express.Multer.File, cb) => {
                const randomName = Array(16).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');

                return cb(null, `${randomName}_-_${file.originalname}`)
            }
        })
    }))
    async updateImage(
        @Req() req: Request,
        @UploadedFile() file: Express.Multer.File,
        @Param('packageId') packageId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            let url: string = '/upload/packages/' + file.filename;
            await this.packageService.updateAvatar(packageId, url);

            if(!this.configService.isProduction) {
                url = req.protocol + '://' + req.get('host') + url;
            } else {
                url = this.configService.baseApiUrl + url;
            }
            return {
                error: false,
                data: {
                    url
                }
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Remove package image'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Delete('image/:packageId')
    async deleteImage(
        @Req() req: Request,
        @Param('packageId') packageId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.packageService.deleteAvatar(packageId);
            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Check if package name is available',
        description: `
            Method to validate package name and return if name is available for using
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated package name and is available to be used`
    })
    @ApiBadRequestResponse({
        description: `This means that the name is unavailable to be used.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Get(':name')
    async isNameAvailable(
        @Req() req,
        @Param('name') name: string
    ): Promise<BaseResponse> {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.packageService.isNameAvailable(name);
            return {
                error: false,
                message: this.i18n.translate('package.NAME_AVAILABLE', {
                    lang: language,
                    args: {
                        name: name
                    }
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Creates a new package',
        description: `
            Method to create a new package
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated package name and the package is created`
    })
    @ApiBadRequestResponse({
        description: `This means that the name is unavailable to be used and the package is not created.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post()
    async createPackage(
        @Body() createPackageDto: CreatePackageDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            // Get admin from express request
            let admin: any = req.user;

            // Call service to create the package
            // If name is unavailable the method will end in catch exception
            // If is available and there are no internal errors, it will return the new package ID
            let packageId: number = await this.packageService.createPackage(createPackageDto);

            // Creates the log to admin
            await this.logService.createAdminLog({
                adminId: admin.admin_id,
                userAgent: req.headers['user-agent'],
                ip: req.ip,
                title: 'Novo pacote criado',
                description: `Novo pacote <b>${createPackageDto.name}</b> criado em ${moment().format('DD/MM/YYYY [às] HH:mm')}`,
                createdAt: moment().toDate(),
                actionType: 'insert'
            });

            return {
                error: false,
                message: this.i18n.translate('package.PACKAGE_SUCCESSFULLY_CREATED', {
                    lang: language
                }),
                data: {
                    id: packageId
                }
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Get package list',
        description: `
            Method to get all packages not soft deleted
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post('list')
    async getPackages(
        @Body() filterPackagesDto: FilterPackagesDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let packages: Package[] = await this.packageService.getPackages(filterPackagesDto);

            packages.map(pack => {
                if(pack.image) {
                    if(!this.configService.isProduction) {
                        pack.image = req.protocol + '://' + req.get('host') + pack.image;
                    } else {
                        pack.image = this.configService.baseApiUrl + pack.image;
                    }
                }
            })

            return {
                error: false,
                data: packages
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Delete a package',
        description: `
            Method to soft delete a package
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Delete(':packageId')
    async softDeletePackage(
        @Param('packageId') packageId: number,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.packageService.softDeletePackage(packageId);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update a package',
        description: `
            Method to update a package
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put(':packageId')
    async editPackage(
        @Param('packageId') packageId: number,
        @Body() updatePackageDto: UpdatePackageDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.packageService.editPackage(packageId, updatePackageDto);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
