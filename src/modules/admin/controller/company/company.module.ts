import { Module } from '@nestjs/common';
import { CompanyController } from './company.controller';
import { CompanyService } from './company.service';
import { companyProviders } from '../../../../core/providers';
import { LogModule } from '../../../../core/log/log.module';
import { DatabaseModule } from '../../../../core/database/database.module';
import { CompanyDemandController } from './company-demand/company-demand.controller';
import { CompanyDemandService } from './company-demand/company-demand.service';

@Module({
    imports: [
        DatabaseModule,
        LogModule
    ],
    controllers: [
        CompanyController,
        CompanyDemandController
    ],
    providers: [
        ...companyProviders,
        CompanyService,
        CompanyDemandService
    ],
    exports: [
        CompanyService
    ]
})
export class AdminCompanyModule {}
