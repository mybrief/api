import { Injectable, Inject } from '@nestjs/common';
import { Repository, Like } from 'typeorm';
import { CompanyUser, Company } from '../../../../entities';
import { FilterCompaniesDto, UpdateCompanyDto } from '../../../../dtos';
import { isStringEmpty } from '../../../../core/utils';
import * as moment from 'moment';

@Injectable()
export class CompanyService {
    constructor(
        @Inject('COMPANY_REPOSITORY')
        private readonly companyRepository: Repository<Company>,
        @Inject('COMPANY_USER_REPOSITORY')
        private readonly companyUserRepository: Repository<CompanyUser>
    ) {}

    async validateCompany(companyId: number): Promise<CompanyUser> {
        return new Promise<CompanyUser>(async (resolve, reject) => {
            try {
                let admin: CompanyUser = await this.companyUserRepository.findOne({
                    where: {
                        company_id: companyId,
                        owner: 1
                    }
                });

                await this.companyRepository.update({ id: companyId}, {
                    valid: 1
                });

                resolve(admin);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async updateCompany(companyId: number, updateCompanyDto: UpdateCompanyDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.companyRepository.update({id: companyId}, updateCompanyDto);

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async deleteCompany(companyId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.companyRepository.update({id: companyId}, {
                    deleted_at: moment().toDate()
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async getCompanies({name}: FilterCompaniesDto): Promise<Company[]> {
        return new Promise<Company[]>(async (resolve, reject) => {
            try {
                let whereOpt = {};

                if(!isStringEmpty(name)) {
                    whereOpt['name'] = Like(`%${name}%`);
                }

                let companies: Company[] = await this.companyRepository.find({
                    where: {
                        deleted_at: null,
                        ...whereOpt
                    },
                    relations: ['users']
                });

                resolve(companies);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Check if company exists
     *
     * Check if company exists with the given ID
     * @param id id of the company to be checked
     */
    async companyExists(id: number): Promise<boolean> {
        return await this.companyRepository.count({id: id}) == 1
    }
}
