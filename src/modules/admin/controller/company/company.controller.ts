import { Controller, UseGuards, Get, Req, HttpStatus, HttpException, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { CompanyService } from './company.service';
import { I18nService } from 'nestjs-i18n';
import { MailerService } from '@nest-modules/mailer';
import { CompanyUser } from '../../../../entities';
import { Request } from 'express';
import { FilterCompaniesDto, UpdateCompanyDto } from '../../../../dtos';

@ApiUseTags('Admin', 'Company')
@Controller('admin/company')
export class CompanyController {
    constructor(
        private readonly companyService: CompanyService,
        private readonly i18n: I18nService,
        private readonly mailerService: MailerService,
    ) {}

    @ApiOperation({
        title: `Get all companies`,
        description: `
            Method to get all companies in the system that are not soft deleted
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post()
    async getCompanies(
        @Req() req: Request,
        @Body() filterCompaniesDto: FilterCompaniesDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                data: await this.companyService.getCompanies(filterCompaniesDto)
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: `Validate a company`,
        description: `
            Method to validate a company
        `
    })
    @ApiOkResponse({
        description: 'The company is valid and can now start to use the platform.'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Get(':companyId')
    async validateCompany(
        @Req() req,
        @Param('companyId') companyId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let user: CompanyUser = await this.companyService.validateCompany(companyId);

            await this.mailerService.sendMail({
                to: user.email,
                from: 'noreply@mybrief.com.br',
                subject: this.i18n.translate('auth.COMPANY_SUCCESSFULLY_VALIDATED', {lang: language}),
                html: this.i18n.translate('auth.COMPANY_SUCCESSFULLY_VALIDATED_DESCRIPTION', {
                    lang: language,
                    args: {
                        name: user.name + ' ' + user.last_name
                    }
                })
            });

            return {
                error: false,
                message: this.i18n.translate('company.ADMIN.COMPANY_SUCCESSFULLY_VALIDATED', {
                    lang: language
                }),
                data: null
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: `Update the company`,
        description: `
            Method to update company informations
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put(':companyId')
    async updateCompany(
        @Req() req: Request,
        @Param('companyId') companyId: number,
        @Body() updateCompanyDto: UpdateCompanyDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.companyService.updateCompany(companyId, updateCompanyDto);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: `Soft delete a company`,
        description: `
            Method to soft delete a company (setting deleted at as current timestamp)
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Delete(':companyId')
    async deleteCompany(
        @Req() req: Request,
        @Param('companyId') companyId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.companyService.deleteCompany(companyId);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
