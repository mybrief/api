import { AuthGuard } from '@nestjs/passport';
import { I18nService } from 'nestjs-i18n';
import { Request } from 'express';
import { ApiUseTags, ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { Controller, HttpStatus, HttpException, Req, UseGuards, Get } from '@nestjs/common';
import { CompanyDemandService } from './company-demand.service';

@ApiUseTags('Company')
@Controller('admin/company/demand')
export class CompanyDemandController {
    constructor(
        private readonly companyDemandService: CompanyDemandService,
        private readonly i18n: I18nService
    ) {}

    @ApiOperation({
        title: `Get all companies demands`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Get()
    async getDemands(
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                data: await this.companyDemandService.getDemands()
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
