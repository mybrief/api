import { Repository } from 'typeorm';
import { Injectable, Inject } from '@nestjs/common';
import { CompanyDemand, CompanyDemandFeedback } from './../../../../../entities';
import { CreateCompanyDemandFeedbackDto } from './../../../../../dtos';

@Injectable()
export class CompanyDemandService {
    constructor(
        @Inject('COMPANY_DEMAND_REPOSITORY')
        private readonly companyDemandRepository: Repository<CompanyDemand>,
        @Inject('COMPANY_DEMAND_FEEDBACK_REPOSITORY')
        private readonly companyDemandFeedbackRepository: Repository<CompanyDemandFeedback>
    ) {}

    async getDemands(): Promise<CompanyDemand[]> {
        return new Promise<CompanyDemand[]>(async (resolve, reject) => {
            try {
                const demands = await this.companyDemandRepository
                                            .createQueryBuilder('company_demand')
                                            .where({
                                                deleted_at: null
                                            })
                                            .where('NOT EXISTS (SELECT * FROM company_demand_feedback as cdf where cdf.company_demand_id = company_demand.id)')
                                            .leftJoinAndSelect('company_demand.annex', 'annex')
                                            .orderBy('created_at', 'DESC')
                                            .getMany();

                resolve(demands);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async createDemandFeedback(companyDemandId: number, createCompanyDemandFeedbackDto: CreateCompanyDemandFeedbackDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                const { feedback, status } = createCompanyDemandFeedbackDto;

                await this.companyDemandFeedbackRepository.insert({
                    company_demand_id: companyDemandId,
                    feedback,
                    status
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
