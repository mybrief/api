import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../../../core/database/database.module';
import { LogModule } from '../../../../core/log/log.module';
import { ProductController } from './product.controller';
import { productProviders } from '../../../../core/providers';
import { ProductService } from './product.service';

@Module({
    imports: [
        DatabaseModule,
        LogModule
    ],
    controllers: [
        ProductController
    ],
    providers: [
        ...productProviders,
        ProductService
    ],
    exports: [
        ProductService
    ]
})
export class AdminProductModule {}
