import { ConfigService } from './../../../../core/config/config.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Controller, Get, Param, UseGuards, Req, HttpStatus, HttpException, Post, Body, Delete, Put, UploadedFile, UseInterceptors } from '@nestjs/common';
import { ProductService } from './product.service';
import { I18nService } from 'nestjs-i18n';
import { ApiUseTags, ApiBearerAuth, ApiOperation, ApiCreatedResponse, ApiBadRequestResponse, ApiOkResponse } from '@nestjs/swagger';
import { BaseResponse } from '../../../../interfaces/responses';
import { AuthGuard } from '@nestjs/passport';
import { CreateProductDto, FilterProductsDto, UpdateProductDto } from '../../../../dtos';
import { Request } from 'express';
import { LogService } from '../../../../core/log/log.service';
import * as moment from 'moment';
import { Product } from '../../../../entities';
import * as fs from 'fs';

@ApiUseTags('Admin', 'Product')
@Controller('admin/product')
export class ProductController {
    constructor(
        private productService: ProductService,
        private readonly i18n: I18nService,
        private readonly logService: LogService,
        private readonly configService: ConfigService
    ) {}

    @ApiOperation({
        title: 'Check if product name is available',
        description: `
            Method to validate product name and return if name is available for using
        `
    })
    @ApiOkResponse({
        description: `The system successfully validated product name and is available to be used`
    })
    @ApiBadRequestResponse({
        description: `This means that the name is unavailable to be used.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Get(':name')
    async isNameAvailable(
        @Req() req,
        @Param('name') name: string
    ): Promise<BaseResponse> {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.productService.isNameAvailable(name);
            return {
                error: false,
                message: this.i18n.translate('product.NAME_AVAILABLE', {
                    lang: language,
                    args: {
                        name: name
                    }
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update product image'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put('image/:productId')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: (req: Request, file: Express.Multer.File, cb) => {
                let dir = process.cwd() +  '/public/upload/products';

                if (!fs.existsSync(dir)) {
                    fs.mkdir(dir, { recursive: true }, err => cb(err, dir));
                } else {
                    cb(null, dir);
                }
            },
            filename: (req: Request, file: Express.Multer.File, cb) => {
                const randomName = Array(16).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');

                return cb(null, `${randomName}_-_${file.originalname}`)
            }
        })
    }))
    async updateImage(
        @Req() req: Request,
        @UploadedFile() file: Express.Multer.File,
        @Param('productId') productId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            let url: string = '/upload/products/' + file.filename;
            await this.productService.updateAvatar(productId, url);

            if(!this.configService.isProduction) {
                url = req.protocol + '://' + req.get('host') + url;
            } else {
                url = this.configService.baseApiUrl + url;
            }
            return {
                error: false,
                data: {
                    url
                }
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Remove product image'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Delete('image/:productId')
    async deleteImage(
        @Req() req: Request,
        @Param('productId') productId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.productService.deleteAvatar(productId);
            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Creates a new product',
        description: `
            Method to create a new product
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated product name and the product is created`
    })
    @ApiBadRequestResponse({
        description: `This means that the name is unavailable to be used and the product is not created.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post()
    async createProduct(
        @Body() createProductDto: CreateProductDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            // Get admin from express request
            let admin: any = req.user;

            // Call service to create the product
            // If name is unavailable the method will end in catch exception
            // If is available and there are no internal errors, it will return the new product ID
            let productId: number = await this.productService.createProduct(createProductDto);

            // Creates the log to admin
            await this.logService.createAdminLog({
                adminId: admin.admin_id,
                userAgent: req.headers['user-agent'],
                ip: req.ip,
                title: 'Novo produto criado',
                description: `Novo produto <b>${createProductDto.name}</b> criado em ${moment().format('DD/MM/YYYY [às] HH:mm')}`,
                createdAt: moment().toDate(),
                actionType: 'insert'
            });

            return {
                error: false,
                message: this.i18n.translate('product.PRODUCT_SUCCESSFULLY_CREATED', {
                    lang: language
                }),
                data: {
                    id: productId
                }
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Get product list',
        description: `
            Method to get all products not soft deleted
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post('list')
    async getProducts(
        @Body() filterProductsDto: FilterProductsDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let products: Product[] = await this.productService.getProducts(filterProductsDto);

            products.map(product => {
                if(product.image) {
                    if(!this.configService.isProduction) {
                        product.image = req.protocol + '://' + req.get('host') + product.image;
                    } else {
                        product.image = this.configService.baseApiUrl + product.image;
                    }
                }
            })

            return {
                error: false,
                data: products
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Delete a product',
        description: `
            Method to soft delete a product
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Delete(':productId')
    async softDeleteProduct(
        @Param('productId') productId: number,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.productService.softDeleteProduct(productId);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update a product',
        description: `
            Method to update a product
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put(':productId')
    async editProduct(
        @Param('productId') productId: number,
        @Body() updateProductDto: UpdateProductDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.productService.editProduct(productId, updateProductDto);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
