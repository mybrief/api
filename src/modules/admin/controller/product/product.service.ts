import { Injectable, Inject } from '@nestjs/common';
import { Product } from '../../../../entities';
import { Repository, InsertResult, Like } from 'typeorm';
import { CreateProductDto, FilterProductsDto, UpdateProductDto } from '../../../../dtos';
import { isStringEmpty, isEmpty } from '../../../../core/utils';
import * as moment from 'moment';

@Injectable()
export class ProductService {
    constructor(
        @Inject('PRODUCT_REPOSITORY')
        private readonly productRepository: Repository<Product>
    ) {}

    async updateAvatar(productId: number, image: string) {
        return new Promise<void>(async (resolve, reject) => {
            await this.productRepository.update({
                id: productId
            }, { image });

            resolve();
        })
    }

    async deleteAvatar(productId: number) {
        return new Promise<void>(async (resolve, reject) => {
            await this.productRepository.update({
                id: productId
            }, { image: null });

            resolve();
        })
    }

    /**
     * ### Get product list
     *
     * Method to get all products not soft deleted
     * @param filterProductsDto optional params to filter products
     * @returns Promise with the product array
     */
    async getProducts(filterProductsDto: FilterProductsDto): Promise<Product[]> {
        return new Promise<Product[]>(async (resolve, reject) => {
            try {
                const {name, category} = filterProductsDto;

                let findOpt = {};

                if(!isStringEmpty(name)) {
                    findOpt['name'] = Like(`%${name}%`);
                }

                if(!isStringEmpty(category)) {
                    findOpt['category'] = category.toLowerCase();
                }

                let products: Product[] = await this.productRepository.find({
                    where: {
                        deleted_at: null,
                        ...findOpt,
                    },
                    order: {
                        created_at: 'DESC'
                    }
                });

                resolve(products);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Edit product
     *
     * Method to edit product informations if it's not soft deleted
     * @param productId id of the product to be edited
     * @param editProductDto params to be changed
     */
    async editProduct(productId: number, editProductDto: UpdateProductDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                if(!isEmpty(editProductDto)) {
                    await this.productRepository.update({
                        id: productId,
                        deleted_at: null
                    }, editProductDto);
                }

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Soft delete a product
     *
     * Method to soft deleted a product updating deleted_at field as current date
     * @param productId id to be soft deleted
     */
    async softDeleteProduct(productId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.productRepository.update({
                    id: productId
                }, {
                    deleted_at: moment().toDate()
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Validate product name
     *
     * Method to validate if product name is available to be used
     * @param name Package name to validate if is available
     * @returns `Promise`: `resolve` name is available & `reject` name is not available
     */
    async isNameAvailable(name: string): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                let count: number = await this.productRepository.count({name: name});

                if(count == 0) {
                    return resolve(true);
                } else {
                    return reject({
                        message: 'product.ERROR.NAME_ALREADY_TAKEN',
                        catch: false,
                        data: {
                            args: {name: name}
                        }
                    })
                }
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Create a product
     *
     * Method to create a new product with the given informations
     * @param createProductDto Package informations
     * @returns `Promise`: `resolve` product created and returns the `product id` & `reject` error while creating the product
     */
    async createProduct(createProductDto: CreateProductDto): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                await this.isNameAvailable(createProductDto.name);

                let insertResult: InsertResult = await this.productRepository.insert(createProductDto);

                let productId: number = insertResult.raw.insertId;

                resolve(productId);
            } catch (err) {
                if(err.catch != null && !err.catch) {
                    return reject(err);
                } else {
                    return reject({
                        catch: true,
                        message: err.message,
                        data: err
                    });
                }
            }
        });
    }

    /**
     * ### Check if product exists
     *
     * Check if product exists with the given ID
     * @param id id of the product to be checked
     */
    async productExists(id: number): Promise<boolean> {
        return await this.productRepository.count({id: id}) == 1
    }
}
