import { Injectable, Inject } from '@nestjs/common';
import { Plan, PlanProduct } from '../../../../entities';
import { Repository, InsertResult, Like } from 'typeorm';
import { CreatePlanDto, CreatePlanProductDto, FilterPlansDto, UpdatePlanDto } from '../../../../dtos';
import { ProductService } from '../product/product.service';
import { PackageService } from '../package/package.service';
import { isStringEmpty, isEmpty } from '../../../../core/utils';
import * as moment from 'moment';

@Injectable()
export class PlanService {
    constructor(
        @Inject('PLAN_REPOSITORY')
        private readonly planRepository: Repository<Plan>,
        @Inject('PLAN_PRODUCT_REPOSITORY')
        private readonly planProductRepository: Repository<PlanProduct>,
        private readonly productService: ProductService,
        private readonly packageService: PackageService,
    ) {}

    async updateAvatar(planId: number, image: string) {
        return new Promise<void>(async (resolve, reject) => {
            await this.planRepository.update({
                id: planId
            }, { image });

            resolve();
        })
    }

    async deleteAvatar(planId: number) {
        return new Promise<void>(async (resolve, reject) => {
            await this.planRepository.update({
                id: planId
            }, { image: null });

            resolve();
        })
    }

    /**
     * ### Validate plan name
     *
     * Method to validate if plan name is available to be used
     * @param name Plan name to validate if is available
     * @returns `Promise`: `resolve` name is available & `reject` name is not available
     */
    async isNameAvailable(name: string): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                let count: number = await this.planRepository.count({name: name});

                if(count == 0) {
                    return resolve(true);
                } else {
                    return reject({
                        message: 'plan.ERROR.NAME_ALREADY_TAKEN',
                        catch: false,
                        data: {
                            args: {name: name}
                        }
                    })
                }
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Create a plan
     *
     * Method to create a new plan with the given informations
     * @param createPlanDto Plan informations
     * @returns `Promise`: `resolve` plan created and returns the `plan id` & `reject` error while creating the plan
     */
    async createPlan(createPlanDto: CreatePlanDto): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                await this.isNameAvailable(createPlanDto.name);

                let payload = {
                    name: createPlanDto.name,
                    description: createPlanDto.description,
                    price: createPlanDto.price,
                    allowed_payment_type: createPlanDto.allowed_payment_type,
                    pagarme_plan_id: createPlanDto.pagarme_plan_id,
                    charges: createPlanDto.charges
                }

                let insertResult: InsertResult = await this.planRepository.insert(payload);
                let planId: number = insertResult.raw.insertId;

                if(createPlanDto.products != null && createPlanDto.products.length) {
                    await this.createPlanProduct(createPlanDto.products, planId);
                }

                resolve(planId);
            } catch (err) {
                if(err.catch != null && !err.catch) {
                    return reject(err);
                } else {
                    return reject({
                        catch: true,
                        message: err.message,
                        data: err
                    });
                }
            }
        });
    }

    /**
     * ### Create plan products
     *
     * Method to create a plan product with the given informations
     * @param createPlanProductDto Plan products informations
     * @returns `Promise`: `resolve` products created & `reject` error while creating the plan product
     */
    async createPlanProduct(createPlanProductDto: CreatePlanProductDto[], planId: number = null): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                if(planId != null) {
                    createPlanProductDto.map((product) => {
                        product.plan_id = planId;
                    });
                }

                let payload: CreatePlanProductDto[] = [];

                for(let product of createPlanProductDto) {
                    if(product.item_type === 'product' && !await this.productService.productExists(product.item_id)) {
                        continue;
                    } else if(product.item_type === 'package' && !await this.packageService.packageExists(product.item_id)) {
                        continue;
                    } else {
                        payload.push(product);
                    }
                }

                await this.planProductRepository.insert(payload);

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Get list of plans
     *
     * Method to get list of plans not soft deleted
     * @param filterPlansDto optional params to filter the list
     * @returns Promise with the plan array
     */
    async getPlans(filterPlansDto: FilterPlansDto): Promise<Plan[]> {
        return new Promise<Plan[]>(async (resolve, reject) => {
            try {
                const {name, payment_type} = filterPlansDto;

                let findOpt = {};

                if(!isStringEmpty(name)) {
                    findOpt['name'] = Like(`%${name}%`);
                }

                if(!isStringEmpty(payment_type)) {
                    findOpt['payment_type'] = payment_type.toLowerCase();
                }

                let plans: Plan[] = await this.planRepository.find({
                    where: {
                        deleted_at: null,
                        ...findOpt,
                    },
                    order: {
                        created_at: 'DESC'
                    },
                    relations: ['plan_product', 'plan_product.product', 'plan_product.package']
                });

                resolve(plans);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Soft delete a plan
     *
     * Method to soft deleted a plan updating deleted_at field as current date
     * @param planId id to be soft deleted
     */
    async softDeletePlan(planId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.planRepository.update({
                    id: planId
                }, {
                    deleted_at: moment().toDate()
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Edit plan
     *
     * Method to edit plan informations if it's not soft deleted
     * @param planId id of the plan to be edited
     * @param editPlanDto params to be changed
     */
    async editPlan(planId: number, editPlanDto: UpdatePlanDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                if(!isEmpty(editPlanDto)) {
                    await this.planRepository.update({
                        id: planId,
                        deleted_at: null
                    }, editPlanDto);
                }

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
