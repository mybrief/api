import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../../../core/database/database.module';
import { LogModule } from '../../../../core/log/log.module';
import { PlanController } from './plan.controller';
import { planProviders, productProviders, packageProviders } from '../../../../core/providers';
import { PlanService } from './plan.service';
import { AdminPackageModule } from '../package/package.module';
import { AdminProductModule } from '../product/product.module';

@Module({
    imports: [
        DatabaseModule,
        LogModule,
        AdminPackageModule,
        AdminProductModule
    ],
    controllers: [
        PlanController
    ],
    providers: [
        ...planProviders,
        ...productProviders,
        ...packageProviders,
        PlanService
    ],
    exports: [
        PlanService
    ]
})
export class AdminPlanModule {}
