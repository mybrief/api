import { ConfigService } from './../../../../core/config/config.service';
import { diskStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express';
import { Controller, HttpStatus, HttpException, Param, Req, Get, UseGuards, Post, Body, Delete, Put, UseInterceptors, UploadedFile } from '@nestjs/common';
import { PlanService } from './plan.service';
import { I18nService } from 'nestjs-i18n';
import { ApiUseTags, ApiBearerAuth, ApiBadRequestResponse, ApiCreatedResponse, ApiOperation } from '@nestjs/swagger';
import { LogService } from '../../../../core/log/log.service';
import { BaseResponse } from '../../../../interfaces/responses';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { CreatePlanDto, FilterPlansDto, UpdatePlanDto } from '../../../../dtos';
import * as moment from 'moment';
import { Plan } from '../../../../entities';
import * as fs from 'fs';

@ApiUseTags('Admin', 'Plan')
@Controller('admin/plan')
export class PlanController {
    constructor(
        private planService: PlanService,
        private readonly i18n: I18nService,
        private readonly logService: LogService,
        private readonly configService: ConfigService
    ) {}

    @ApiOperation({
        title: 'Update plan image'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put('image/:planId')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: (req: Request, file: Express.Multer.File, cb) => {
                let dir = process.cwd() +  '/public/upload/plans';

                if (!fs.existsSync(dir)) {
                    fs.mkdir(dir, { recursive: true }, err => cb(err, dir));
                } else {
                    cb(null, dir);
                }
            },
            filename: (req: Request, file: Express.Multer.File, cb) => {
                const randomName = Array(16).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');

                return cb(null, `${randomName}_-_${file.originalname}`)
            }
        })
    }))
    async updateImage(
        @Req() req: Request,
        @UploadedFile() file: Express.Multer.File,
        @Param('planId') planId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            let url: string = '/upload/plans/' + file.filename;
            await this.planService.updateAvatar(planId, url);

            if(!this.configService.isProduction) {
                url = req.protocol + '://' + req.get('host') + url;
            } else {
                url = this.configService.baseApiUrl + url;
            }
            return {
                error: false,
                data: {
                    url
                }
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Remove plan image'
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:freelancer'))
    @Delete('image/:planId')
    async deleteImage(
        @Req() req: Request,
        @Param('planId') planId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.planService.deleteAvatar(planId);
            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Check if plan name is available',
        description: `
            Method to validate plan name and return if name is available for using
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated plan name and is available to be used`
    })
    @ApiBadRequestResponse({
        description: `This means that the name is unavailable to be used.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Get(':name')
    async isNameAvailable(
        @Req() req: Request,
        @Param('name') name: string
    ): Promise<BaseResponse> {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.planService.isNameAvailable(name);
            return {
                error: false,
                message: this.i18n.translate('plan.NAME_AVAILABLE', {
                    lang: language,
                    args: {
                        name: name
                    }
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Creates a new plan',
        description: `
            Method to create a new plan
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated plan name and the plan is created`
    })
    @ApiBadRequestResponse({
        description: `This means that the name is unavailable to be used and the plan is not created.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post()
    async createPlan(
        @Body() createPlanDto: CreatePlanDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            // Get admin from express request
            let admin: any = req.user;

            // Call service to create the plan
            // If name is unavailable the method will end in catch exception
            // If is available and there are no internal errors, it will return the new plan ID
            let planId: number = await this.planService.createPlan(createPlanDto);

            // Creates the log to admin
            await this.logService.createAdminLog({
                adminId: admin.admin_id,
                userAgent: req.headers['user-agent'],
                ip: req.ip,
                title: 'Novo plano criado',
                description: `Novo plano <b>${createPlanDto.name}</b> criado em ${moment().format('DD/MM/YYYY [às] HH:mm')}`,
                createdAt: moment().toDate(),
                actionType: 'insert'
            });

            return {
                error: false,
                message: this.i18n.translate('plan.PLAN_SUCCESSFULLY_CREATED', {
                    lang: language
                }),
                data: {
                    id: planId
                }
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Get plan list',
        description: `
            Method to get all plans not soft deleted
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post('list')
    async getPlans(
        @Body() filterPlansDto: FilterPlansDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let plans: Plan[] = await this.planService.getPlans(filterPlansDto);

            plans.map(plan => {
                if(plan.image) {
                    if(!this.configService.isProduction) {
                        plan.image = req.protocol + '://' + req.get('host') + plan.image;
                    } else {
                        plan.image = this.configService.baseApiUrl + plan.image;
                    }
                }
            })

            return {
                error: false,
                data: plans
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Delete a plan',
        description: `
            Method to soft delete a plan
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Delete(':planId')
    async softDeletePlan(
        @Param('planId') planId: number,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.planService.softDeletePlan(planId);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update a plan',
        description: `
            Method to update a plan
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put(':planId')
    async editPlan(
        @Param('planId') planId: number,
        @Body() updatePlanDto: UpdatePlanDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.planService.editPlan(planId, updatePlanDto);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
