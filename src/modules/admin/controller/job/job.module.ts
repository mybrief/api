import { Module } from '@nestjs/common';
import { JobController } from './job.controller';
import { JobService } from './job.service';
import { LogModule } from '../../../../core/log/log.module';
import { DatabaseModule } from '../../../../core/database/database.module';
import { jobProviders, adminProviders } from '../../../../core/providers';
import { FreelancerModule } from '../../../freelancer/freelancer.module';
import { AdminCompanyModule } from '../company/company.module';
import { AdminProductModule } from '../product/product.module';
import { CommonModule } from '../../../../core/common/common.module';

@Module({
    imports: [
        DatabaseModule,
        LogModule,
        FreelancerModule,
        AdminCompanyModule,
        AdminProductModule,
        CommonModule
    ],
    controllers: [
        JobController
    ],
    providers: [
        ...jobProviders,
        ...adminProviders,
        JobService
    ],
    exports: [
        JobService
    ]
})
export class AdminJobModule {}
