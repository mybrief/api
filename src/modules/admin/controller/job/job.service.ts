import { Injectable, Inject } from '@nestjs/common';
import { Repository, InsertResult, SelectQueryBuilder, In } from 'typeorm';
import {
    CreateJobDto,
    FilterAdminJobsDto,
    UpdateJobDeadlineDto
} from '../../../../dtos';
import { Job, JobAttachment, JobCommentary, Admin } from '../../../../entities';
import { FreelancerService } from '../freelancer/freelancer.service';
import { CompanyService } from '../company/company.service';
import { ProductService } from '../product/product.service';

import * as moment from 'moment';
import { CommonService } from '../../../../core/common/common.service';
import { rejects } from 'assert';

@Injectable()
export class JobService {
    constructor(
        @Inject('JOB_REPOSITORY')
        private readonly jobRepository: Repository<Job>,
        @Inject('JOB_ATTACHMENT_REPOSITORY')
        private readonly jobAttachmentRepository: Repository<JobAttachment>,
        @Inject('JOB_COMMENTARY_REPOSITORY')
        private readonly jobCommentaryRepository: Repository<JobCommentary>,
        @Inject('ADMIN_REPOSITORY')
        private readonly adminRepository: Repository<Admin>,
        private readonly freelancerService: FreelancerService,
        private readonly companyService: CompanyService,
        private readonly productService: ProductService,
        private readonly commonService: CommonService
    ) {}

    async commentInJob(jobId: number, adminId: number, commentary: string): Promise<JobCommentary> {
        return new Promise<JobCommentary>(async (resolve, reject) => {
            try {
                if(await this.jobRepository.count({ id: jobId }) == 0) {
                    return reject({
                        message: 'admin.ERROR.INVALID_JOB_OR_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                let payload: any = {
                    job_id: jobId,
                    commentary: commentary,
                    sender_type: 'admin',
                    sender_id: adminId
                }

                let insert: InsertResult = await this.jobCommentaryRepository.insert(payload);

                let admin: Admin = await this.adminRepository.findOne({
                    where: {
                        id: adminId
                    },
                    select: ['id', 'avatar', 'name', 'last_name']
                });

                payload['id'] = insert.identifiers[0].id;
                payload['created_at'] = moment().toDate();
                payload['admin'] = admin;

                resolve(payload);
            } catch (err) {
                reject({
                    catch: true,
                    error: err.message,
                    data: err
                })
            }
        });
    }

    async deleteAttachment(attachmentId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let attachment: JobAttachment = await this.jobAttachmentRepository.findOne({
                    where: {
                        id: attachmentId
                    },
                    select: ['id', 'job_id', 'url']
                });

                if(attachment == null) {
                    return reject({
                        message: 'admin.ERROR.ATTACHMENT_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                await this.commonService.deleteFileFromServer(attachment.url);
                await this.jobAttachmentRepository.delete({
                    id: attachmentId
                });
                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    error: err.message,
                    data: err
                })
            }
        });
    }

    async changeJobStatus(jobId: number, status: 'to_do'|'doing'|'to_approval'|'to_change'|'done'): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let job: Job = await this.jobRepository.findOne({
                    where: {
                        id: jobId
                    }
                });

                if(job == null) {
                    return reject({
                        message: 'freelancer.ERROR.INVALID_JOB_OR_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                if(job.status == status) {
                    return resolve();
                }

                let updateOpt = {};

                if(status == 'doing' && job.started_at == null) {
                    updateOpt['started_at'] = moment().toDate();
                } else if(status == 'to_approval') {
                    updateOpt['finished_at'] = moment().toDate();
                }

                await this.jobRepository.update({
                    id: jobId
                }, {
                    status: status,
                    ...updateOpt
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async attachToJob(jobId: number, adminId: number, url: string, originalName: string, mimeType: string, bytes: number): Promise<JobAttachment> {
        return new Promise<JobAttachment>(async (resolve, reject) => {
            try {
                if(await this.jobRepository.findOne({
                    where: {
                        id: jobId
                    },
                    select: ['id']
                }) == null) {
                    return reject({
                        message: 'admin.ERROR.INVALID_JOB_OR_NOT_FOUND',
                        catch: false,
                        data: {
                            args: null
                        }
                    });
                }

                let payload: any = {
                    job_id: jobId,
                    sender_id: adminId,
                    sender_type: 'admin',
                    name: originalName,
                    url: url,
                    mime_type: mimeType,
                    bytes: bytes
                }

                let insert: InsertResult = await this.jobAttachmentRepository.insert(payload);

                payload['id'] = insert.identifiers[0].id;
                payload['created_at'] = moment().toDate();

                resolve(payload);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                })
            }
        })
    }

    async createJob(createJobDto: CreateJobDto): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                // Verify if product is already a job
                if(await this.productAlreadyExistsAsJob(createJobDto.company_product_id)) {
                    return reject({
                        message: 'job.ERROR.PRODUCT_ALREADY_EXISTS_AS_JOB',
                        catch: false,
                        data: null
                    });
                }

                // Verify product id
                if(!await this.productService.productExists(createJobDto.company_product_id)) {
                    return reject({
                        message: 'job.ERROR.PRODUCT_NOT_FOUND',
                        catch: false,
                        data: null
                    });
                }

                // If freelancer is beeing assigned on creation, verify is freelancer exists
                if(createJobDto.freelancer_id != null && !await this.freelancerService.freelancerExists(createJobDto.freelancer_id)) {
                    return reject({
                        message: 'job.ERROR.FREELANCER_NOT_FOUND',
                        catch: false,
                        data: null
                    });
                }

                // Verify company id
                if(!await this.companyService.companyExists(createJobDto.company_id)) {
                    return reject({
                        message: 'job.ERROR.COMPANY_NOT_FOUND',
                        catch: false,
                        data: null
                    });
                }

                let insertResult: InsertResult = await this.jobRepository.insert(createJobDto);
                let jobId: number = insertResult.raw.insertId;

                resolve(jobId);
            } catch(err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async getJobs(filters: FilterAdminJobsDto): Promise<Job[]> {
        return new Promise<Job[]>(async (resolve, reject) => {
            try {
                let companiesFilter: string = "";
                let freelancersFilter: string = "";

                if(filters.companies != null && filters.companies.length > 0) {
                    companiesFilter = `company.id IN (${filters.companies.join(',')})`;
                }

                if(filters.freelancers != null && filters.freelancers.length > 0) {
                    freelancersFilter = `freelancer.deleted_at is null and freelancer.id IN (${filters.freelancers.join(',')})`;
                } else {
                    freelancersFilter = 'freelancer.deleted_at is null';
                }

                let order: 'DESC'|'ASC' = "DESC";
                let orderBy: string = "job.created_at";

                if(filters.order_by == 'most_recent') {
                    order = "DESC";
                    orderBy = "job.created_at";
                } else if (filters.order_by == 'less_recent') {
                    order = "ASC";
                    orderBy = "job.created_at";
                } else if(filters.order_by == 'company') {
                    order = "ASC";
                    orderBy = "company.name";
                } else if(filters.order_by == 'freelancer') {
                    order = "ASC";
                    orderBy = "profile.name";
                } else if(filters.order_by == 'more_finish_date') {
                    order = "DESC";
                    orderBy = "job.finish_date";
                } else if(filters.order_by == 'less_finish_date') {
                    order = "ASC";
                    orderBy = "job.finish_date";
                }

                let orderObj = {};
                orderObj[orderBy] = order;

                let jobs: SelectQueryBuilder<Job> = this.jobRepository.createQueryBuilder('job')
                    .where({
                        paused: 0,
                        deleted_at: null
                    })
                    .innerJoin('job.company', 'company', companiesFilter)
                    .leftJoinAndSelect('job.commentary', 'commentary')
                    .leftJoinAndSelect('job.attachment', 'attachment')
                    .leftJoinAndSelect('commentary.freelancer', 'freela')
                    .leftJoinAndSelect('freela.profile', 'freela_profile')
                    .leftJoinAndSelect('commentary.admin', 'admin')
                    .innerJoinAndSelect('job.freelancer', 'freelancer', freelancersFilter)
                    .leftJoinAndSelect('freelancer.profile', 'profile')
                    .orderBy({
                        ...orderObj,
                        'commentary.created_at': 'DESC'
                    })
                    .select([
                        'job.id',
                        'job.notes',
                        'job.freelancer_id',
                        'job.title',
                        'job.started_at',
                        'job.start_date',
                        'job.finish_date',
                        'job.finished_at',
                        'job.status',
                        'attachment',
                        'commentary.sender_type',
                        'commentary.commentary',
                        'commentary.sender_id',
                        'commentary.created_at',
                        'freelancer.id',
                        'freelancer.username',
                        'freela.id',
                        'freela.username',
                        'freela_profile.name',
                        'freela_profile.last_name',
                        'freela_profile.avatar',
                        'profile.name',
                        'profile.last_name',
                        'profile.avatar',
                        'admin.id',
                        'admin.avatar',
                        'admin.name',
                        'admin.last_name',
                        'company.id',
                        'company.name',
                        'company.logo'
                    ]);

                resolve(await jobs.getMany());
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async updateJobDeadline(jobId: number, updateJobDeadlineDto: UpdateJobDeadlineDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.jobRepository.update({
                    id: jobId
                }, {
                    start_date: moment(updateJobDeadlineDto.start_date).toDate(),
                    finish_date: moment(updateJobDeadlineDto.finish_date).toDate()
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        })
    }

    async updateJobTitle(jobId: number, title: string): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.jobRepository.update({
                    id: jobId
                }, {
                    title: title
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    async updateJobDescription(jobId: number, description: string): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.jobRepository.update({
                    id: jobId
                }, {
                    notes: description
                });

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    private async productAlreadyExistsAsJob(id: number): Promise<boolean> {
        return await this.jobRepository.count({
            company_product_id: id,
            deleted_at: null
        }) == 1
    }
}
