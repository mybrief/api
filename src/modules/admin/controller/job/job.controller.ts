import { Controller, HttpStatus, HttpException, Param, Req, UseGuards, Post, Body, UseInterceptors, UploadedFile, Put, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiBadRequestResponse, ApiCreatedResponse, ApiOperation } from '@nestjs/swagger';
import { JobService } from './job.service';
import { I18nService } from 'nestjs-i18n';
import { MailerService } from '@nest-modules/mailer';
import { CreateJobDto, FilterAdminJobsDto, UpdateAdminJobStatusDto, CreateJobCommentaryDto, UpdateJobDeadlineDto, UpdateJobTitleDto, UpdateJobDescriptionDto } from '../../../../dtos';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { LogService } from '../../../../core/log/log.service';

import * as moment from 'moment';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import * as fs from 'fs';
import { JobAttachment, Job } from '../../../../entities';
import { ConfigService } from '../../../../core/config/config.service';

@ApiUseTags('Admin', 'Job')
@Controller('admin/job')
export class JobController {
    constructor(
        private jobService: JobService,
        private readonly i18n: I18nService,
        private readonly mailerService: MailerService,
        private readonly logService: LogService,
        private readonly configService: ConfigService
    ) {}

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Delete('attachment/:attachmentId')
    async deleteAttachment(
        @Req() req: Request,
        @Param('attachmentId') attachmentId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.jobService.deleteAttachment(attachmentId);

            return {
                error: false,
                message: this.i18n.translate('admin.ATTACHMENT_DELETED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post(':jobId/comment')
    async commentInJob(
        @Req() req: Request,
        @Param('jobId') jobId: number,
        @Body() createJobCommentaryDto: CreateJobCommentaryDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            return {
                error: false,
                data: await this.jobService.commentInJob(jobId, req.user['admin_id'], createJobCommentaryDto.commentary)
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put('description/:jobId')
    async changeJobDescription(
        @Req() req: Request,
        @Body() updateJobDescriptionDto: UpdateJobDescriptionDto,
        @Param('jobId') jobId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.jobService.updateJobDescription(jobId, updateJobDescriptionDto.description);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put('title/:jobId')
    async changeJobTitle(
        @Req() req: Request,
        @Body() updateJobTitleDto: UpdateJobTitleDto,
        @Param('jobId') jobId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.jobService.updateJobTitle(jobId, updateJobTitleDto.title);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put('deadline/:jobId')
    async changeJobDeadline(
        @Req() req: Request,
        @Body() changeJobDeadline: UpdateJobDeadlineDto,
        @Param('jobId') jobId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.jobService.updateJobDeadline(jobId, changeJobDeadline);

            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put('status/:jobId')
    async changeJobStatus(
        @Req() req: Request,
        @Body() updateAdminJobStatusDto: UpdateAdminJobStatusDto,
        @Param('jobId') jobId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            await this.jobService.changeJobStatus(jobId, updateAdminJobStatusDto.status)
            return {
                error: false,
                message: this.i18n.translate('admin.JOB_STATUS_UPDATED_SUCCESSFULLY', {
                    lang: language
                })
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post(':jobId/attach')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: (req: Request, file: Express.Multer.File, cb) => {
                let dir = process.cwd() +  '/public/upload/admins/job';

                if (!fs.existsSync(dir)) {
                    fs.mkdir(dir, { recursive: true }, err => cb(err, dir));
                } else {
                    cb(null, dir);
                }
            },
            filename: (req: Request, file: Express.Multer.File, cb) => {
                const randomName = Array(16).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');

                return cb(null, `${randomName}_-_${file.originalname}`)
            }
        })
    }))
    async attachToJob(
        @Req() req: Request,
        @UploadedFile() file: Express.Multer.File,
        @Param('jobId') jobId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';

        try {
            let url: string = '/upload/admins/job/' + file.filename;
            let attachment: JobAttachment = await this.jobService.attachToJob(jobId, req.user['admin_id'], url, file.originalname, file.mimetype, file.size);

            if(!this.configService.isProduction) {
                attachment.url = req.protocol + '://' + req.get('host') + url;
            } else {
                attachment.url = this.configService.baseApiUrl + url;
            }

            return {
                error: false,
                data: attachment
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post('list')
    async getJobs(
        @Body() filterAdminJobsDto: FilterAdminJobsDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            let jobs: Job[] = await this.jobService.getJobs(filterAdminJobsDto);

            jobs.map((job: Job) => {
                job.attachment.map(attachment => {
                    if(!this.configService.isProduction) {
                        attachment.url = req.protocol + '://' + req.get('host') + attachment.url;
                    } else {
                        attachment.url = this.configService.baseApiUrl + attachment.url;
                    }
                });
            });
            return {
                error: false,
                data: jobs
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Creates a job',
        description: `
            Method to create a new job of a specific product
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated job informations and created it`
    })
    @ApiBadRequestResponse({
        description: `This means that the product, company or freelancer doestn't exists or the product is already a job.`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post()
    async createJob(
        @Body() createJobDto: CreateJobDto,
        @Req() req: Request
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            // Get admin from express request
            let admin: any = req.user;

            // Call service to create the job
            let jobId: number = await this.jobService.createJob(createJobDto);

            // Creates the log to admin
            await this.logService.createAdminLog({
                adminId: admin.admin_id,
                userAgent: req.headers['user-agent'],
                ip: req.ip,
                title: 'Nova atividade criada',
                description: `Nova atividade criada em ${moment().format('DD/MM/YYYY [às] HH:mm')}`,
                createdAt: moment().toDate(),
                actionType: 'insert'
            });

            return {
                error: false,
                message: this.i18n.translate('job.JOB_SUCCESSFULLY_CREATED', {
                    lang: language
                }),
                data: {
                    id: jobId
                }
            };
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
