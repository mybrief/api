import { Injectable, Inject } from '@nestjs/common';
import { Freelancer, FreelancerProfile, Job, FreelancerFinancial } from '../../../../entities';
import { Repository } from 'typeorm';
import { FilterFreelancersDto, UpdateFreelancerDto } from '../../../../dtos';
import * as moment from 'moment';
import { isEmpty } from '../../../../core/utils';

@Injectable()
export class FreelancerService {
    constructor(
        @Inject('JOB_REPOSITORY')
        private readonly jobRepository: Repository<Job>,
        @Inject('FREELANCER_REPOSITORY')
        private readonly freelancerRepository: Repository<Freelancer>,
        @Inject('FREELANCER_FINANCIAL_REPOSITORY')
        private readonly freelancerFinancialRepository: Repository<FreelancerFinancial>,
        @Inject('FREELANCER_PROFILE_REPOSITORY')
        private readonly freelancerProfileRepository: Repository<FreelancerProfile>
    ) {}

    /**
     * ### Check if freelancer exists
     *
     * Check if freelancer exists with the given ID
     * @param id id of the freelancer to be checked
     */
    async freelancerExists(id: number): Promise<boolean> {
        return await this.freelancerRepository.count({id: id}) == 1
    }

    /**
     * ### Check if freelancer email is already in use
     *
     * Check if freelancer exists with the given email
     * @param email email of the freelancer to be checked
     */
    async freelancerEmailAlreadyInUse(email: string): Promise<boolean> {
        return await this.freelancerRepository.count({email: email}) == 1
    }

    /**
     * ### Get all freelancers and their relations
     *
     * Soft deleted frelancers will not show in the list
     * @param filter optional filters to get freelancers
     */
    async getFreelancers(filter: FilterFreelancersDto): Promise<Freelancer[]> {
        return new Promise<Freelancer[]>(async (resolve, reject) => {
            try {
                let profileParam: string = "";

                if(filter.name != null) {
                    profileParam = `(profile.name LIKE ("%${filter.name}%"))`;
                }

                if(filter.area != null) {
                    if(profileParam != '') {
                        profileParam += ' AND ';
                    }

                    profileParam += `(profile.area LIKE ("%${filter.area}%"))`;
                }

                let freelancers: Freelancer[] = [];
                freelancers = await this.freelancerRepository.createQueryBuilder('freelancer')
                                        .innerJoinAndSelect('freelancer.profile', 'profile', profileParam)
                                        .leftJoinAndSelect('freelancer.bank_account', 'bank_account')
                                        .leftJoinAndSelect('freelancer.financial', 'financial')
                                        .leftJoinAndSelect('freelancer.time_available', 'time_available')
                                        .where({deleted_at: null})
                                        .getMany();

                for(let freelancer of freelancers) {
                    freelancer['job_count'] = await this.jobRepository.count({freelancer_id: freelancer.id})
                }

                resolve(freelancers);
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Method to soft delete a freelancer
     *
     * The method set deleted_at as current time stamp
     */
    async softDeleteFreelancer(id: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.freelancerRepository.update({id: id}, {deleted_at: moment().toDate()});

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }

    /**
     * ### Method to update freelancers informations
     *
     * @param updateFreelancerDto parameters to update the freelancer
     */
    async updateFreelancer(freelancerId: number, updateFreelancerDto: UpdateFreelancerDto): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let profileUpdate = {};
                let financialUpdate = {};

                if(updateFreelancerDto.hour_value_progress != null) {
                    financialUpdate['hour_value_progress'] = updateFreelancerDto.hour_value_progress;
                }

                if(updateFreelancerDto.current_hourly_value != null) {
                    financialUpdate['current_hourly_value'] = updateFreelancerDto.current_hourly_value;
                }

                if(updateFreelancerDto.month_earning != null) {
                    financialUpdate['month_earning'] = updateFreelancerDto.month_earning;
                }

                if(updateFreelancerDto.name != null) {
                    profileUpdate['name'] = updateFreelancerDto.name;
                }

                if(updateFreelancerDto.last_name != null) {
                    profileUpdate['last_name'] = updateFreelancerDto.last_name;
                }

                if(updateFreelancerDto.birthday != null) {
                    profileUpdate['birthday'] = updateFreelancerDto.birthday;
                }

                if(updateFreelancerDto.job != null) {
                    profileUpdate['job'] = updateFreelancerDto.job;
                }

                if(updateFreelancerDto.area != null) {
                    profileUpdate['area'] = updateFreelancerDto.area;
                }

                if(updateFreelancerDto.cellphone != null) {
                    profileUpdate['cellphone'] = updateFreelancerDto.cellphone;
                }

                if(updateFreelancerDto.location != null) {
                    profileUpdate['location'] = updateFreelancerDto.location;
                }

                if(updateFreelancerDto.area != null) {
                    profileUpdate['area'] = updateFreelancerDto.area;
                }

                if(!isEmpty(profileUpdate)) {
                    await this.freelancerProfileRepository.update({ freelancer_id: freelancerId }, profileUpdate);
                }

                if(!isEmpty(financialUpdate)) {
                    await this.freelancerFinancialRepository.update({ freelancer_id: freelancerId }, financialUpdate);
                }

                if(!isEmpty(updateFreelancerDto.email) && !await this.freelancerEmailAlreadyInUse(updateFreelancerDto.email)) {
                    await this.freelancerRepository.update({id: freelancerId}, {
                        email: updateFreelancerDto.email
                    });
                }

                resolve();
            } catch (err) {
                reject({
                    catch: true,
                    message: err.message,
                    data: err
                });
            }
        });
    }
}
