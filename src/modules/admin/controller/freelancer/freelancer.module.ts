import { Module } from '@nestjs/common';
import { FreelancerService } from './freelancer.service';
import { FreelancerController } from './freelancer.controller';
import { freelancerProviders, jobProviders } from '../../../../core/providers';
import { LogModule } from '../../../../core/log/log.module';
import { DatabaseModule } from '../../../../core/database/database.module';
import { AdminPlanModule } from '../plan/plan.module';

@Module({
  imports: [
    DatabaseModule,
    LogModule,
    AdminPlanModule
  ],
  providers: [
    ...freelancerProviders,
    ...jobProviders,
    FreelancerService
  ],
  controllers: [
    FreelancerController
  ],
  exports: [
    FreelancerService
  ]
})
export class AdminFreelancerModule {}
