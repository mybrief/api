import { ConfigService } from './../../../../core/config/config.service';
import { Controller, Get, UseGuards, HttpStatus, HttpException, Req, Body, Post, Param, Delete, Put } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiCreatedResponse, ApiBadRequestResponse, ApiBearerAuth, ApiOkResponse } from '@nestjs/swagger';
import { PlanService } from '../plan/plan.service';
import { I18nService } from 'nestjs-i18n';
import { LogService } from '../../../../core/log/log.service';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { FreelancerService } from './freelancer.service';
import { FilterFreelancersDto, UpdateFreelancerDto } from '../../../../dtos';

@ApiUseTags('Admin', 'Freelancer')
@Controller('admin/freelancer')
export class FreelancerController {
    constructor(
        private planService: PlanService,
        private readonly i18n: I18nService,
        private readonly logService: LogService,
        private readonly freelancerService: FreelancerService,
        private readonly configService: ConfigService
    ) {}

    @ApiOperation({
        title: 'Get freelancer list',
        description: `
            Method to get freelancer list
        `
    })
    @ApiCreatedResponse({
        description: `The system successfully validated admin token and returned the list`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Post()
    async getList(
        @Req() req: Request,
        @Body() filterFreelancersDto: FilterFreelancersDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            const freelancers = await this.freelancerService.getFreelancers(filterFreelancersDto);

            freelancers.map(freelancer => {
                if(freelancer.profile) {
                    if(!this.configService.isProduction) {
                        freelancer.profile.avatar = req.protocol + '://' + req.get('host') + freelancer.profile.avatar;
                    } else {
                        freelancer.profile.avatar = this.configService.baseApiUrl + freelancer.profile.avatar;
                    }
                }
            })
            
            return {
                error: false,
                data: freelancers
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Soft delete a freelancer',
        description: `
            Method to soft delete freelancer by id 
        `
    })
    @ApiOkResponse({
        description: `The system successfully soft deleted the freelancer`
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Delete(':freelancerId')
    async softDelete(
        @Req() req: Request,
        @Param('freelancerId') freelancerId: number
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.freelancerService.softDeleteFreelancer(freelancerId);
            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }

    @ApiOperation({
        title: 'Update freelancer information',
        description: `
            Method to update freelancer informations (financial and profile)
        `
    })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt:admin'))
    @Put(':freelancerId')
    async updateFreelancer(
        @Req() req: Request,
        @Param('freelancerId') freelancerId: number,
        @Body() updateFreelancerDto: UpdateFreelancerDto
    ) {
        let language: any = req.acceptsLanguages('fr', 'en', 'pt') || 'pt';
        try {
            await this.freelancerService.updateFreelancer(freelancerId, updateFreelancerDto);
            return {
                error: false
            }
        } catch (err) {
            let message: string = "";
            let error: number;
            if(err.catch != null && !err.catch) {
                message = this.i18n.translate(err.message, {
                    lang: language,
                    args: err.data.args
                });

                error = HttpStatus.BAD_REQUEST;
            } else {
                message = err.message;
                error = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            throw new HttpException(message, error);
        }
    }
}
