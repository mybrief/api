import { IsNotEmpty, IsString, IsOptional, IsArray, ValidateNested, ValidateIf } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { CreateCompanyInvoiceCustomerDto } from './create-company-invoice-customer.dto';
import { CreateCompanyInvoiceCreditCardDto } from './create-company-invoice-credit-card.dto';
import { CreateCompanyInvoiceBankSlipDto } from './create-company-invoice-bank-slip.dto';
import { CreateCompanyInvoiceBillingDto } from './create-company-invoice-billing.dto';

export class CreateCompanyInvoicePagarmeInfoDto {
    @ValidateNested()
    @IsNotEmpty()
    @ApiModelProperty()
    customer: CreateCompanyInvoiceCustomerDto;

    @ValidateNested()
    @IsNotEmpty()
    @ApiModelProperty()
    billing: CreateCompanyInvoiceBillingDto;

    @ValidateIf(dto => dto.card_id == null && dto.payment_method === 'credit_card')
    @ValidateNested()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    card?: CreateCompanyInvoiceCreditCardDto;

    @ValidateIf(dto => dto.card == null && dto.payment_method === 'credit_card')
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    card_id?: string

    @ValidateIf(dto => dto.card == null && dto.card_id == null)
    @ValidateNested()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    bank_slip?: CreateCompanyInvoiceBankSlipDto;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    installments: '1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'|'10'|'11'|'12';

    @ValidateIf(dto => dto.payment_method === 'credit_card')
    @ApiModelProperty()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    soft_descriptor?: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    payment_method: 'boleto'|'credit_card';

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    postback_url: string;
}