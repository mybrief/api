import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, ValidateIf, IsArray } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class FilterFreelancerJobsDto {
    @IsArray()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    companies?: number[];

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    order_by?: 'most_recent'|'less_recent'|'company'|'more_finish_date'|'less_finish_date' = 'most_recent';
}