import { IsNotEmpty, IsString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateFreelanerJobStatusDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    status: 'to_do'|'doing'|'to_approval';
}