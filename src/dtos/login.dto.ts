import { IsNotEmpty, IsString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class LoginDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    username: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    password: string;
}