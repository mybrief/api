import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, ValidateNested, ValidateIf, IsArray } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { CreatePackageProductDto } from './create-package-product.dto';

export class CreatePackageDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    description: string;

    @ValidateIf(dto => dto.price == null)
    @IsInt()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    price_coin: number;

    @ValidateIf(dto => dto.price_coin == null)
    @IsDecimal()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    price: number;

    @IsArray()
    @IsOptional()
    @ApiModelPropertyOptional()
    products: CreatePackageProductDto[];
}