import { IsNotEmpty, IsString, IsOptional, IsInt, IsDecimal } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateProductDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    description?: string;

    @IsInt()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    price_coin?: number;

    @IsDecimal()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    price?: number;
}