import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, Min } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreatePackageProductDto {
    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    product_id: number;

    @IsOptional()
    @IsInt()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    package_id: number;

    @IsInt()
    @Min(1)
    @IsNotEmpty()
    @ApiModelProperty()
    quantity: number;
}