import { IsNotEmpty, IsString, IsOptional, IsArray } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateCompanyInvoiceCustomerDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    type: 'individual'|'corporation';

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    country: string = 'br';

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    email: string;

    @IsArray()
    @IsNotEmpty()
    @ApiModelProperty()
    documents: [
      {
        'type': string,
        'number': string
      }
    ];

    @IsArray()
    @IsNotEmpty()
    @ApiModelProperty()
    phone_numbers: string[];

    @IsArray()
    @IsNotEmpty()
    @ApiModelProperty()
    birthday: string;
}