import { IsNotEmpty, IsString, IsOptional, IsIn } from 'class-validator';

import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class FilterCompanyProductsDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;

    @IsString()
    @IsIn(['social_media','strategy','development'])
    @IsNotEmpty()
    @ApiModelProperty()
    category: 'social_media'|'strategy'|'development';
}