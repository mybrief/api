import { IsNotEmpty, IsString, IsOptional, IsIn } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class FilterPlansDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;

    @IsString()
    @IsIn(['bank_slip','credit_card','all'])
    @IsOptional()
    @ApiModelPropertyOptional()
    payment_type?: 'bank_slip'|'credit_card'|'all';
}