import { IsNotEmpty, IsString, IsOptional } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class FilterCompanyPackagesDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;
}