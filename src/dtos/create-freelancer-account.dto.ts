import { IsNotEmpty, IsString, IsEmail, MinLength, MaxLength, IsDateString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class CreateFreelancerAccountDto {
    @IsString()
    @IsNotEmpty()
    @MinLength(3)
    @MaxLength(32)
    @ApiModelProperty()
    username: string;

    @IsEmail()
    @IsNotEmpty()
    @ApiModelProperty()
    email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(3)
    @ApiModelProperty()
    password: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    last_name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    area: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    birthday: string;
}