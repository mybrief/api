import { IsNotEmpty, IsString, IsOptional, IsInt } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateCompanyUserDto {
    @IsInt()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    company_id: number;

    @IsInt()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: 0
    })
    owner: number = 0;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: 'purchase,request_job,approve_delivery,reprove_delivery'
    })
    permission: string = 'purchase,request_job,approve_delivery,reprove_delivery';

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    last_name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    password: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    email: string;
}