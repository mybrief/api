import { IsNotEmpty, IsString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateAdminJobStatusDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    status: 'to_do'|'doing'|'to_approval'|'to_change'|'done';
}