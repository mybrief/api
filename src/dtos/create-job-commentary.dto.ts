import { IsNotEmpty, IsString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class CreateJobCommentaryDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    commentary: string;
}