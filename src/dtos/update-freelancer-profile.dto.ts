import { IsNotEmpty, IsString, MinLength, IsOptional, IsEmail } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateFreelancerProfileDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    full_name: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    cellphone: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    birthday: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    location: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    job: string;

    @IsEmail()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    email: string;
}