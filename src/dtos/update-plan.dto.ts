import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, IsArray } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { CreatePlanProductDto } from './create-plan-product.dto';

export class UpdatePlanDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    description?: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    allowed_payment_type?: string;

    @IsArray()
    @IsOptional()
    @ApiModelPropertyOptional()
    products?: CreatePlanProductDto[];
}