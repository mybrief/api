import { IsNotEmpty, IsString, IsOptional } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateCompanyAccountDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    admin_name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    admin_last_name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    admin_password: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    admin_email: string;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    cellphone: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    telephone: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    segment: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    site: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    localization: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    cnpj: string;
}