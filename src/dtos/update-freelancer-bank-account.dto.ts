import { IsNotEmpty, IsString, MinLength, IsOptional, IsEmail, IsIn } from 'class-validator';

import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class UpdateFreelancerBankAccount {
    @IsNotEmpty()
    @IsString()
    @ApiModelProperty()
    bank: string;

    @IsNotEmpty()
    @IsString()
    @ApiModelProperty()
    agency: string;

    @IsNotEmpty()
    @IsString()
    @ApiModelProperty()
    account: string;

    @IsNotEmpty()
    @IsString()
    @ApiModelProperty()
    full_name: string;

    @IsNotEmpty()
    @IsString()
    @ApiModelProperty()
    cpf: string;

    @IsNotEmpty()
    @IsString()
    @IsIn(['savings','checking'])
    @ApiModelProperty()
    account_type: string;
}