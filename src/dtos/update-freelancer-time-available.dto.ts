import { IsNotEmpty, IsInt, Max, Min } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateFreelancerTimeAvailableDto {
    @IsNotEmpty()
    @IsInt()
    @Max(10)
    @Min(0)
    @ApiModelProperty()
    monday: number;

    @IsNotEmpty()
    @IsInt()
    @Max(10)
    @Min(0)
    @ApiModelProperty()
    tuesday: number;

    @IsNotEmpty()
    @IsInt()
    @Max(10)
    @Min(0)
    @ApiModelProperty()
    wednesday: number;

    @IsNotEmpty()
    @IsInt()
    @Max(10)
    @Min(0)
    @ApiModelProperty()
    thursday: number;

    @IsNotEmpty()
    @IsInt()
    @Max(10)
    @Min(0)
    @ApiModelProperty()
    friday: number;

    @IsNotEmpty()
    @IsInt()
    @Max(10)
    @Min(0)
    @ApiModelProperty()
    saturday: number;

    @IsNotEmpty()
    @IsInt()
    @Max(10)
    @Min(0)
    @ApiModelProperty()
    sunday: number;
}