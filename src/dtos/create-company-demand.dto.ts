import { IsNotEmpty, IsString, IsInt, IsOptional, IsArray } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

import { CreateCompanyDemandAnnexDto } from './create-company-demand-annex.dto';

export class CreateCompanyDemandDto {
    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    company_product_id: number;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    notes: string;

    @IsArray()
    @IsOptional()
    @ApiModelPropertyOptional()
    annex: CreateCompanyDemandAnnexDto[];
}