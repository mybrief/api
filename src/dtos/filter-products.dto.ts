import { IsNotEmpty, IsString, IsOptional, IsIn } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class FilterProductsDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;

    @IsString()
    @IsIn(['social_media','strategy','development'])
    @IsOptional()
    @ApiModelPropertyOptional()
    category?: 'social_media'|'strategy'|'development';
}