import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateFreelancerRequestToChangeTimeAvailableDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    description: string;
}