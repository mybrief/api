import { IsNotEmpty, IsBoolean } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateFreelancerJobRequestDto {
    @IsBoolean()
    @IsNotEmpty()
    @ApiModelProperty()
    accepted: boolean;
}