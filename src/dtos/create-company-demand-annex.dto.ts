import { IsNotEmpty, IsString, IsInt, IsOptional } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateCompanyDemandAnnexDto {
    @IsInt()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    company_demand_id?: number;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    annex_type: 'file'|'link';

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    path: 'file'|'link';
}