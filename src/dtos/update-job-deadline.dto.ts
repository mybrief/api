import { IsNotEmpty, IsDate, IsString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateJobDeadlineDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    start_date: Date;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    finish_date: Date;
}