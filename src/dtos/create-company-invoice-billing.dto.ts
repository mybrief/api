import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateCompanyInvoiceAddressDto } from './create-company-invoice-address.dto';

export class CreateCompanyInvoiceBillingDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string

    @ValidateNested()
    @IsNotEmpty()
    @ApiModelProperty()
    address: CreateCompanyInvoiceAddressDto
}