import { IsNotEmpty, IsString, IsOptional, ValidateNested } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateCompanyInvoiceAddressDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string;
    
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    street: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    street_number: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    zipcode: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    country: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    state: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    city: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    neighborhood: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    complementary?: string;
}