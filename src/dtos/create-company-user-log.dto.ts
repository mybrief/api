import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateCompanyUserLogDto {
    @IsNumber()
    @IsNotEmpty()
    @ApiModelProperty()
    companyUserId: number;

    @IsOptional()
    @IsDate()
    @ApiModelPropertyOptional()
    createdAt: Date;

    @IsOptional()
    @IsString()
    @ApiModelPropertyOptional()
    actionType: string;

    @IsOptional()
    @IsString()
    @ApiModelPropertyOptional()
    userAgent: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    title: string;

    @IsOptional()
    @IsString()
    @ApiModelPropertyOptional()
    description: string;

    @IsOptional()
    @IsString()
    @ApiModelPropertyOptional()
    ip: string;
}