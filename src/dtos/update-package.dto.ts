import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, ValidateNested, ValidateIf, IsArray } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { CreatePackageProductDto } from './create-package-product.dto';

export class UpdatePackageDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    description: string;

    @IsInt()
    @IsOptional()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    price_coin?: number;

    @IsDecimal()
    @IsOptional()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    price?: number;

    @IsArray()
    @IsOptional()
    @ApiModelPropertyOptional()
    products?: CreatePackageProductDto[];
}