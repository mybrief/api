import { IsNotEmpty, IsString, IsOptional, IsInt } from 'class-validator';

import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class CreateCompanyJobFeedbackRequestDto {
    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    job_id: number;

    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    company_id: number;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    notes: string;
}