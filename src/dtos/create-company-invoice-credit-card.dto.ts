import { IsNotEmpty, IsString, MinLength, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateCompanyInvoiceCustomerDto } from './create-company-invoice-customer.dto';

export class CreateCompanyInvoiceCreditCardDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    card_number?: string

    @IsString()
    @MinLength(3)
    @MaxLength(4)
    @IsNotEmpty()
    @ApiModelProperty()
    card_cvv?: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    card_expiration_date?: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    card_holder_name?: string
}