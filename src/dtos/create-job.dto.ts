import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, ValidateIf, Min, Max, IsEnum, IsIn } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateJobDto {
    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    company_id: number;

    @IsInt()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: null
    })
    freelancer_id: number;

    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    company_product_id: number;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: null
    })
    notes: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    title: string;

    @IsString()
    @IsIn(['to_do','doing','to_approval','to_change','done'])
    @IsNotEmpty()
    @ApiModelProperty()
    status: string;

    @IsInt()
    @Min(0)
    @Max(1)
    @IsOptional()
    @ApiModelPropertyOptional({
        default: 0
    })
    paused: number;

    @IsDate()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: null
    })
    start_date: Date;

    @IsDate()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: null
    })
    finish_date: Date;

    @IsDate()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: null
    })
    started_at: Date;

    @IsDate()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: null
    })
    finished_at: Date;
}