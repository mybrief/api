import { IsNotEmpty, IsString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateJobDescriptionDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    description: string;
}