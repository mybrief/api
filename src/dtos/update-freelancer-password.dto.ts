import { IsNotEmpty, IsString, MinLength } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateFreelancerPasswordDto {
    @IsString()
    @IsNotEmpty()
    @MinLength(3)
    @ApiModelProperty()
    old_password: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(3)
    @ApiModelProperty()
    new_password: string;
}