import { IsNotEmpty, IsString, IsOptional, IsArray, IsNumber, ValidateNested } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

import { CreateCompanyInvoiceItemDto } from './create-company-invoice-item.dto';
import { CreateCompanyInvoicePagarmeInfoDto } from './create-company-invoice-pagarme-info.dto';

export class CreateCompanyInvoiceDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    payment_status: 'processing'|'authorized'|'paid'|'refunded'|'waiting_payment'|'pending_refund'|'refused';

    @IsArray()
    @IsNotEmpty()
    @ApiModelProperty()
    invoice_item: CreateCompanyInvoiceItemDto[];

    @IsNumber()
    @IsNotEmpty()
    @ApiModelProperty()
    price: number;

    @IsNumber()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    discount: number;

    @IsNotEmpty()
    @ApiModelProperty()
    pagarme_info: CreateCompanyInvoicePagarmeInfoDto;
}