import { IsNotEmpty, IsString, MinLength, IsOptional, IsEmail, IsIn, IsNumber, Min } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateFreelancerDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    last_name: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    cellphone: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    birthday: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    location: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    job: string;

    @IsString()
    @IsIn(['designer', 'developer', 'editor'])
    @IsOptional()
    @ApiModelPropertyOptional()
    area: 'designer'|'developer'|'editor';

    @IsEmail()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    email: string;

    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    @IsOptional()
    @ApiModelPropertyOptional()
    month_earning: string;

    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    @IsOptional()
    @ApiModelPropertyOptional()
    hour_value_progress: string;

    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    @IsOptional()
    @ApiModelPropertyOptional()
    current_hourly_value: string;
}