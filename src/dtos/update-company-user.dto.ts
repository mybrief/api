import { IsNotEmpty, IsString, IsOptional, IsInt } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateCompanyUserDto {
    @IsInt()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: 0
    })
    owner?: number = 0;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional({
        default: 'purchase,request_job,approve_delivery,reprove_delivery'
    })
    permission?: string = 'purchase,request_job,approve_delivery,reprove_delivery';

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    last_name?: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    password?: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    email?: string;
}