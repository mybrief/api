import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, Min } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreatePlanProductDto {
    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    item_id: number;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    item_type: 'product'|'package';

    @IsOptional()
    @IsInt()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    plan_id: number;

    @IsInt()
    @Min(1)
    @IsNotEmpty()
    @ApiModelProperty()
    quantity: number;
}