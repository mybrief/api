import { IsNotEmpty, IsString, IsInt } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateCompanyJobStatusDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    status: string;

    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    job_id: number;

    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    company_job_feedback_request_id: number;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    feedback: string;
}