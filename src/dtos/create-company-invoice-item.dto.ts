import { IsNotEmpty, IsString, IsInt, IsOptional, Min } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateCompanyInvoiceItemDto {
  @IsInt()
  @IsNotEmpty()
  @IsOptional()
  @ApiModelPropertyOptional()
  company_invoice_id: number;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  item_type: 'product'|'package'|'plan';

  @IsInt()
  @IsNotEmpty()
  @ApiModelProperty()
  item_id: number;

  @IsInt()
  @Min(1)
  @IsNotEmpty()
  @ApiModelProperty()
  quantity: number;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  payment_type: 'bank_slip'|'credit_card'|'coin';
}