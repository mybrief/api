import { IsNotEmpty, IsString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateJobTitleDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    title: string;
}