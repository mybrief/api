import { IsNotEmpty, IsString, IsOptional, IsInt } from 'class-validator';

import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class CreateFreelancerJobRequestDto {
    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    freelancer_id: number;

    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    job_id: number;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    notes: string;
}