import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, ValidateIf } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateProductDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    description: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    category: string;

    @ValidateIf(dto => dto.price == null)
    @IsInt()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    price_coin: number;

    @ValidateIf(dto => dto.price_coin == null)
    @IsDecimal()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    price: number;
}