import { IsNotEmpty, IsString, IsOptional, IsInt, IsDecimal, IsArray } from 'class-validator';

import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { CreatePlanProductDto } from './create-plan-product.dto';

export class CreatePlanDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    description: string;

    @IsDecimal()
    @IsNotEmpty()
    @ApiModelProperty()
    price: number;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    allowed_payment_type: string;

    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    pagarme_plan_id: number;

    @IsInt()
    @IsNotEmpty()
    @ApiModelProperty()
    charges: number;

    @IsArray()
    @IsNotEmpty()
    @ApiModelProperty()
    products: CreatePlanProductDto[];
}