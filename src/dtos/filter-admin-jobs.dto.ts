import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, ValidateIf, IsArray } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class FilterAdminJobsDto {
    @IsArray()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    companies?: number[];

    @IsArray()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    freelancers?: number[];

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    order_by?: 'most_recent'|'less_recent'|'company'|'freelancer'|'more_finish_date'|'less_finish_date' = 'most_recent';
}