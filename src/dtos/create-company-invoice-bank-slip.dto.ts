import { IsNotEmpty, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateCompanyInvoiceBankSlipDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    boleto_expiration_date: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    boleto_instructions: string
}