import { IsNotEmpty, IsString, IsDate, IsNumber, IsOptional, IsInt, IsDecimal, ValidateIf, IsArray, IsIn } from 'class-validator';

import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class FilterFreelancersDto {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;

    @IsString()
    @IsIn(['designer', 'developer', 'editor'])
    @IsOptional()
    @ApiModelPropertyOptional()
    area?: 'designer'|'developer'|'editor';
}