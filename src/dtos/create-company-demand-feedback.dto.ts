import { IsNotEmpty, IsString } from 'class-validator';

import { ApiModelProperty } from '@nestjs/swagger';

export class CreateCompanyDemandFeedbackDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    status: 'approval'|'disapproval';

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    feedback: string;
}