import { Module, Global } from '@nestjs/common';
import { PagarmeService } from './pagarme.service';

@Global()
@Module({
  providers: [PagarmeService],
  exports: [PagarmeService]
})
export class PagarmeModule {}
