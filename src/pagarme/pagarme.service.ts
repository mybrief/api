import { Injectable } from '@nestjs/common';

import { ConfigService } from './../core/config/config.service';

import { TransactionInterface } from './../interfaces/pagarme/transaction.interface';

import axios from 'axios';

@Injectable()
export class PagarmeService {
    constructor(
        private readonly configService: ConfigService
    ) {}

    async createTransaction(companyId: number, payload: TransactionInterface): Promise<string> {
        const { data } = await axios.post(`${this.configService.pagarmeBaseApiUrl}/transaction/${companyId}`, payload);

        return '';
    }
}
